# Testing procedure to determine the cause of the contraction

1) Make a new folder for the test
2) Create the start.slurm file as follow, changing the names appropriately:

```
#!/bin/bash

#SBATCH --job-name=TucSeed1
#SBATCH --ntasks=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=darwin.roduit@epfl.ch

#~ Load modules
module load GCC/11.2.0
module load OpenMPI/4.1.1
module load FFmpeg/4.3.2

#Runs the simulation
srun --ntasks=1 /hpcstorage/roduit/Simulations/swiftsim/swift --external-gravity --self-gravity --stars --threads=${SLURM_CPUS_PER_TASK} params.yml

#Then corrects the potential of swift with pNbody potcenter (takes A LOT of time)
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/correct_snapshot_potential.py --folder="snap/" --out_folder="snap_corrected" --method="histocenter"

#Finally analyzes the data
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/data_analysis.py "snap_corrected/" "data_analysis" --no_DM_half_light_radius_computations
```

3) Create the restart directory. Inside, create the stop file. `mkdir restart && touch stop &&  mv stop restart`
4) Create the params.yml file with the help of the following template:

```
#Define the system of units to use internally.
InternalUnitSystem:
  UnitMass_in_cgs:     1.988e+43 # 10^10 Solar masses
  UnitLength_in_cgs:   3.086e+21 # kpc
  UnitVelocity_in_cgs: 1e5       # km / s
  UnitCurrent_in_cgs:  1         # Amperes
  UnitTemp_in_cgs:     1         # Kelvin

# Parameters governing the time integration
TimeIntegration:
  time_begin:          0.      # The starting time of the simulation (in internal units).
  time_end:            6.5     # The end time of the simulation (in internal units).
  dt_min:              1e-11   # The minimal time-step size of the simulation (in internal units).
  dt_max:              1e-3    # The maximal time-step size of the simulation (in internal units).

# Parameters for the self-gravity scheme
Gravity:
  eta:                           0.005      # Constant dimensionless multiplier for time integration.
  MAC:                           adaptive  # Choice of mulitpole acceptance criterion: 'adaptive' OR 'geometric'.
  epsilon_fmm:                   0.001      # Tolerance parameter for the adaptive multipole acceptance criterion.
  theta_cr:                      0.7        # Opening angle for the purely gemoetric criterion.
  max_physical_DM_softening:     0.0125      # Physical softening length (in internal units).
  max_physical_baryon_softening: 0.0125      # Physical softening length (in internal units).

# Parameters governing the snapshots
Snapshots:
  subdir:              snap
  basename:            snapshot  # Common part of the name of output files
  time_first:          0.        # Time of the first output (in internal units)
  delta_time:          .1300000000000000000      # Time difference between consecutive outputs (in internal units)

# Parameters governing the conserved quantities statistics
Statistics:
  delta_time:          2e-1    # Time between statistics output

# Parameters related to the initial conditions
InitialConditions:
  file_name:          Tucana_2_launched.hdf5 # The file to read
  shift:              [891.51110839843750000000,891.51110839843750000000,891.51110839843750000000]
  periodic:           0

Restarts:
  delta_hours:        2.5
  stop_steps: 		  7000

MWPotential2014Potential:
  useabspos:       0        # 0 -> positions based on centre, 1 -> absolute positions
  position:        [0.,0.,0.]    #Centre of the potential with respect to centre of the box
  timestep_mult:   0.005     # Dimensionless pre-factor for the time-step condition
  epsilon:         0.001      # Softening size (internal units)
  #All other parameters of the potential take their default values
```
---

5) Make the correct ICs and change the name appropriately in the params.yml. For the different seeds, generate them and then launch the dwarf with : `python3 /hpcstorage/roduit/Scripts/launch_on_orbits.py nfw+plummer_seed_2.hdf5 Tucana_s2_launched.hdf5 324.636129457555284716 253.513231376055841793 -56.922922619296571156 --position_coord="carthesian" --vel_orbit 7.108823299407958984 -14.807355880737304688 -25.378175735473632812 --velocity_coord="carthesian"`
6) Run the simulation `sbatch -p p5 -N 1 -c 32 start.slurm`

7) Make density profiles plot :
`pSphericalProfile -o density --xmax 10 --nr 64 --log xy --select halo  snap_corrected/snapshot_0000.hdf5 snap_corrected/snapshot_0010.hdf5`
and
`pSphericalProfile -o density --xmax 10 --nr 64 --log xy --select stars  snap_corrected/snapshot_0000.hdf5 snap_corrected/snapshot_0010.hdf5`

