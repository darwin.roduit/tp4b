###########################################################################################
#  package:   pNbody
#  file:      swift.py
#  brief:     SWIFT file format
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>, Loic Hausammann <loic_hausammann@hotmail.com>
#
# This file is part of pNbody.
###########################################################################################


##########################################################################
#
# GEAR HDF5 CLASS
#
##########################################################################

import numpy as np
import h5py

import pNbody
from pNbody import mpi, error, units

try:				# all this is useful to read files
    from mpi4py import MPI
except BaseException:
    MPI = None



class Nbody_gh5:
  
  
    def get_default_arrays_props(self):
      '''
      get the default properties of arrays considered
      
      "h5name"  :     name in the hdf5 file
      "dtype"   :     numpy dtype                       
      "dim"     :     dimention 
      "ptypes"  :     type of particles that store this property (useless for the moment)
      "read"    :     read it by default or not
      "write"   :     write it by default or not
      "default" :     default values of the components
      "loaded"  :     is the array currently loaded    
      
      # position must always be first, this is a convention
      '''
      
      all_ptypes = list(range(self.get_mxntpe()))
      
      aprops = {}
      
      aprops["pos"] =  {
                        "h5name"  :     "Coordinates", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     3, 
                        "ptypes"  :     all_ptypes,
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["vel"] =  {
                        "h5name"  :     "Velocities", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     3,
                        "ptypes"  :     all_ptypes, 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }
                      
      aprops["mass"] =  {
                        "h5name"  :     "Masses", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     all_ptypes, 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     1,
                        "loaded"  :     False
                      }

      aprops["num"] =  {
                        "h5name"  :     "ParticleIDs", 
                        "dtype"   :     np.uint32,                        
                        "dim"     :     1,
                        "ptypes"  :     all_ptypes, 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }
                          
      aprops["pot"] =  {
                        "h5name"  :     "Potentials", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     all_ptypes, 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["acc"] =  {
                        "h5name"  :     "Acceleration", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     3,
                        "ptypes"  :     all_ptypes, 
                        "read"    :     False,
                        "write"   :     False, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["rsp"] =  {
                        "h5name"  :     "SmoothingLengths", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["u"] =  {
                        "h5name"  :     "InternalEnergies", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      aprops["rho"] =  {
                        "h5name"  :     "Densities", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }
                      


      aprops["metals"] =  {
                        "h5name"  :     "MetalMassFractions", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0,4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      aprops["snii_thermal_time"] =  {
                        "h5name"  :     "SNII_ThermalTime", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["snia_thermal_time"] =  {
                        "h5name"  :     "SNIa_ThermalTime", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [0], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      aprops["softening"] =  {
                        "h5name"  :     "Softenings", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [1,2], 
                        "read"    :     False,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["minit"] =  {
                        "h5name"  :     "BirthMasses", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["tstar"] =  {
                        "h5name"  :     "BirthScaleFactors", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["tstar"] =  {
                        "h5name"  :     "BirthTimes",         # same as "BirthScaleFactors"
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      aprops["rhostar"] =  {
                        "h5name"  :     "BirthDensities", 
                        "dtype"   :     np.float32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }



      aprops["sp_type"] =  {
                        "h5name"  :     "StellarParticleType", 
                        "dtype"   :     np.int32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      aprops["idp"] =  {
                        "h5name"  :     "ProgenitorIDs", 
                        "dtype"   :     np.uint32,                        
                        "dim"     :     1,
                        "ptypes"  :     [4], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }




      aprops["nsinkswallowed"] =  {
                        "h5name"  :     "NumberOfSinkSwallows", 
                        "dtype"   :     np.uint32,                        
                        "dim"     :     1,
                        "ptypes"  :     [3], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }

      aprops["ngasswallowed"] =  {
                        "h5name"  :     "NumberOfGasSwallows", 
                        "dtype"   :     np.uint32,                        
                        "dim"     :     1,
                        "ptypes"  :     [3], 
                        "read"    :     True,
                        "write"   :     True, 
                        "default" :     0,
                        "loaded"  :     False
                      }


      return aprops  
    
    
    
    def array_is_loaded(self,name):
      return self.arrays_props[name]["loaded"]

    def array_set_loaded(self,name):
      self.arrays_props[name]["loaded"]  = True     
  
    def array_set_unloaded(self,name):
      self.arrays_props[name]["loaded"]  = False 
      
    def array_h5_key(self,name):
      return self.arrays_props[name]["h5name"]
      
    def array_default_value(self,name):
      return self.arrays_props[name]["default"]      
      
    def array_dtype(self,name):
      return self.arrays_props[name]["dtype"]      

    def array_dimension(self,name):
      return self.arrays_props[name]["dim"]   

    def array_read(self,name):
      return self.arrays_props[name]["read"] 
        

  
    def get_npart_from_dataset(self,ptypes=None):
      '''
      Get npart from the block storing the positions
      '''

      if ptypes is None:
        ptypes = list(range(self.get_mxntpe()))
        
      h5_key = self.array_h5_key('pos')  
      
      if mpi.mpi_NTask() > 1:
          fd = h5py.File(self.p_name_global[0],'r',driver="mpio",comm=MPI.COMM_WORLD)
      else:
          fd = h5py.File(self.p_name_global[0],'r')
      
      npart = np.zeros(self.get_mxntpe(),np.uint32)
      
      # loop over particle types
      for i_type in ptypes:
        PartType = "PartType%d" % i_type
        
        if PartType in fd:
          block = fd[PartType]
          
          # if the dataset is present
          if h5_key in fd[PartType].keys():
            
            data_lenght   = block[h5_key].len()
          
            idx0=0
            idx1=data_lenght
            if mpi.mpi_NTask() > 1:
              idx0, idx1 = self.get_particles_limits(data_lenght)
            
            npart[i_type] = idx1-idx0   
            
          else:
            raise error.FormatError("%s is not in the file."%(h5_key))     
      
      
      # close the file
      mpi.mpi_barrier()
      fd.close()
      
      return npart
      

    def set_readable_arrays(self,ptypes=None):
      '''
      Set the arrays to be readable or not according to their presence in the blocks
      '''

      if ptypes is None:
        ptypes = list(range(self.get_mxntpe()))
        
      
      if mpi.mpi_NTask() > 1:
          fd = h5py.File(self.p_name_global[0],'r',driver="mpio",comm=MPI.COMM_WORLD)
      else:
          fd = h5py.File(self.p_name_global[0],'r')
      

      # loop over all arrays flagged as read=True in self.arrays_props
      for name in self.arrays_props.keys():
        
        # skip array if a list of array is provided in self.array
        if self.arrays is not None:
          if not name in self.arrays:
            self.arrays_props[name]["read"]=False
        
        
        if self.array_read(name):

          h5_key        = self.array_h5_key(name)
          flag = 0

          # loop over particle types
          for i_type in ptypes:
            PartType = "PartType%d" % i_type
            
            # if the dataset is present
            if PartType in fd:
              if h5_key in fd[PartType].keys():   
                flag = 1
            
          # if no dataset is found, 
          if flag==0:
            self.arrays_props[name]["read"]=False
          
      
      fd.close()  
        
      
  
    def load(self,name=None,ptypes=None,force=False):
      '''
      Load array from the hdf5 file.
      This function relays on the info stored self.arrays_props
      Here, we assume that npart is known and correct
      '''
      
      if ptypes is None:
        ptypes = list(range(self.get_mxntpe()))
      
      if name not in self.arrays_props:
        print("load error : %s not defined in self.arrays_props"%(name))
        return
      
      dtype         = self.array_dtype(name)
      h5_key        = self.array_h5_key(name)
      default_value = self.array_default_value(name)
      dim           = self.array_dimension(name)
      
      if mpi.mpi_NTask() > 1:
          fd = h5py.File(self.p_name_global[0],'r',driver="mpio",comm=MPI.COMM_WORLD)
      else:
          fd = h5py.File(self.p_name_global[0],'r')
      
      

            
      if not self.array_is_loaded(name) or force:
        if self.verbose > 0 and mpi.mpi_IsMaster():
          print("loading %s"%name)
      

        # erase existing instance
        try: 
          delattr(self,name)
        except AttributeError:
          pass

        
        # loop over particle types
        for i_type in ptypes:
          PartType = "PartType%d" % i_type
          
          if PartType in fd:
            block = fd[PartType]
            
            # if the dataset is present
            if h5_key in fd[PartType].keys():
              
              data_lenght   = block[h5_key].len()
                          
              idx0=0
              idx1=data_lenght
              if mpi.mpi_NTask() > 1:
                idx0, idx1 = self.get_particles_limits(data_lenght)
                
              # sanity check
              if idx1-idx0 !=self.npart[i_type]:
                raise error.FormatError("npart[%d]=%d is not equal to idx1-idx0=%d"%(i_type,npart[i_type],idx1-idx0))                 
                
              # read             
              data = block[h5_key][idx0:idx1].astype(dtype)
              
            
            # if the dataset is not present, compute from default value
            else:
              if dim==1:
                data = (default_value * np.ones(self.npart[i_type])).astype(dtype)
              else:
                data = (default_value * np.ones((self.npart[i_type],dim))).astype(dtype)
           
            try:
              setattr(self, name, np.concatenate((getattr(self, name), data)))
            except AttributeError:
              setattr(self, name, data)
          
    
      
      
      # set it as loaded        
      self.array_set_loaded(name)

      # close the file 
      mpi.mpi_barrier()
      fd.close()
        

        
        
        
    def read_arrays(self,ptypes=None):    
      '''
      Read arrays that must be loaded.
      '''      
      
      if ptypes is None:
        ptypes = list(range(self.get_mxntpe()))      
      
      # loop over all arrays flagged as read=True in self.arrays_props
      for name in self.arrays_props.keys():
        if self.array_read(name) and not self.array_is_loaded(name):
          self.load(name,ptypes)
      
 
 

    def _init_spec(self):
        # create an empty header (in case the user is creating a dataset)
        self._header = []

    def get_excluded_extension(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        return []

    def getParticleMatchingDict(self):
        """
        Return a list of file to avoid when extending the default class.
        """
        
        index = {
            'gas':    0,
            'halo':   1,
            'disk':   2,
            'sink':   3,
            'stars':  4,
            'bndry':  2,
            'stars1': 4,
            'halo1':  1}
        
        return index 

    def check_spec_ftype(self):
        try:
            if mpi.NTask > 1:
                fd = h5py.File(self.p_name_global[0],'r',driver="mpio",comm=MPI.COMM_WORLD)
            else:
                fd = h5py.File(self.p_name_global[0])

            test1 = "Units" not in fd
            test1 = test1 or "Unit temperature in cgs (U_T)" not in fd["Units"].attrs

            test2 = "PartType0/Offset" in fd
            fd.close()
            if test1 or test2:
                raise error.FormatError("swift")

        except IOError as e:
            if self.verbose > 1:
                print("swift not recognized: %s" % e)
            raise error.FormatError("swift")

    def set_pio(self, pio):
        """ Overwrite function
        """
        pNbody.Nbody.set_pio(self, "no")

    def get_read_fcts(self):
        return [self.read_particles]

    def get_write_fcts(self):
        return [self.write_particles]

    def get_mxntpe(self):
        return 6

    def get_header_translation(self):
        """
        Gives a dictionnary containing all the header translation.
        If a new variable is possible in the HDF5 format, only the translation
        is required for the reader/writer.
        As h5py is not supporting dictionnary, they need special care when reading/writing.
        """
        # dict containing all the main header variables (=> easy acces)
        # e.g. self.npart will contain NumPart_ThisFile
        header_var = {}

        # Size variables
        header_var["Header/NumPart_ThisFile"] = "npart"
        header_var["Header/NumPart_Total"] =  "npart" #   "npart_tot" check what must be done properly here
        header_var["Header/NumPart_Total_HighWord"] = "nallhw"
        header_var["Header/MassTable"] = "massarr"
        header_var["Header/NumFilesPerSnapshot"] = "num_files"
        header_var["Header/BoxSize"] = "boxsize"
        header_var["Header/Flag_Entropy_ICs"] = "flag_entr_ics"

        # Physics
        header_var["Header/Scale-factor"] = "atime"
        header_var["Header/Redshift"] = "redshift"
        header_var["Header/Time"] = "time"

        # Cosmology
        #header_var["Cosmology/Omega_b"] = "omegab"
        header_var["Cosmology/Omega_m"] = "omega0"
        header_var["Cosmology/Omega_lambda"] = "omegalambda"
        header_var["Cosmology/h"] = "hubbleparam"
        header_var["Cosmology/Cosmological run"] = "cosmorun"
        


        # Units
        #header_var["Units/Unit velocity in cgs (U_V)"] = "UnitVelocity_in_cm_per_s"
        header_var["Units/Unit length in cgs (U_L)"] = "UnitLength_in_cm"
        header_var["Units/Unit mass in cgs (U_M)"] = "UnitMass_in_g"
        header_var["Units/Unit time in cgs (U_t)"] = "Unit_time_in_cgs"
        header_var["Units/Unit temperature in cgs (U_T)"] = "Unit_temp_in_cgs"
        header_var["Units/Unit current in cgs (U_I)"] = "Unit_current_in_cgs"

        # Code
        header_var["Code/Code"] = "Code"
        header_var["Code/CFLAGS"] = "cflags"
        header_var["Code/Code Version"] = "code_version"
        header_var["Code/Compiler Name"] = "compiler_name"
        header_var["Code/Compiler Version"] = "compiler_version"
        header_var["Code/Configuration options"] = "config_options"
        header_var["Code/FFTW library version"] = "fftw_lib_version"
        header_var["Code/Git Branch"] = "git_branch"
        header_var["Code/Git Date"] = "git_date"
        header_var["Code/Git Revision"] = "git_revision"
        header_var["Code/HDF5 library version"] = "hdf5_lib_version"
        header_var["Code/MPI library"] = "mpi_lib"

        # HydroScheme
        header_var["HydroScheme/Adiabatic index"] = "adiabatic_index"
        header_var["HydroScheme/CFL parameter"] = "cfl_parameter"
        header_var["HydroScheme/Dimension"] = "dimension"
        header_var["HydroScheme/Kernel delta N_ngb"] = "kernel_delta_n_ngb"
        header_var["HydroScheme/Kernel eta"] = "kernel_eta"
        header_var["HydroScheme/Kernel function"] = "kernel_function"
        header_var["HydroScheme/Kernel target N_ngb"] = "kernel_target_n_ngb"
        header_var["HydroScheme/Max ghost iterations"] = "max_ghost_iterations"
        header_var["HydroScheme/Maximal smoothing length"] = "maximal_smoothing_length"
        header_var["HydroScheme/Scheme"] = "scheme"
        header_var["HydroScheme/Smoothing length tolerance"] = "smoothing_length_tolerance"
        header_var["HydroScheme/Thermal Conductivity Model"] = "thermal_conductivity_model"
        header_var["HydroScheme/Viscosity Model"] = "viscosity_model"
        header_var["HydroScheme/Viscosity alpha"] = "viscosity_alpha"
        header_var["HydroScheme/Viscosity beta"] = "viscosity_beta"
        header_var["HydroScheme/Volume log(max(delta h))"] = "volume_log"
        header_var["HydroScheme/Volume max change time-step"] = "volume_max_change"

        # Parameters
        header_var["Parameters/InitialConditions:shift"] = "InitialConditions_shift"



        # Swift directory
        header_var["RuntimePars/PeriodicBoundariesOn"] = "periodic"
        
        
        # chemistry (this is now done elsewhere)
        #header_var["Parameters/GEARFeedback:elements"] = "ChimieElements"
        #header_var["SubgridScheme/Chemistry element count"] = "ChimieNelements"

        return header_var

    def get_list_header(self):
        """
        Gives a list of header directory from self.get_header_translation
        """
        list_header = []
        trans = self.get_header_translation()
        for key, tmp in list(trans.items()):
            directory = key.split("/")[0]
            if directory not in list_header:
                list_header.append(directory)
        return list_header

    def get_array_translation(self):
        """
        Gives a dictionnary containing all the header translation with the particles type
        requiring the array.
        If a new variable is possible in the HDF5 format, the translation,
        default value (function get_array_default_values) and dimension (function get_array_dimension)
        are required for the reader/writer.
        """
        # hdf5 names -> pNbody names
        # [name, partType, type] where partType is a list of particles with the datas
        # True means all, False means none
        # and type is the data type
        ntab = {}
        
        ###################
        # common data
        ntab["Acceleration"] = ["acc", True, np.float32]
        ntab["Coordinates"] = ["pos", True, np.float32]
        ntab["Velocities"] = ["vel", True, np.float32]
        ntab["ParticleIDs"] = ["num", True, np.uint32]
        ntab["Masses"] = ["mass", True, np.float32]
        
        
        ntab["Potentials"] = ["pot", True, np.float32]
        
        ###################
        # gas data
        #ntab["SmoothingLength"] = ["rsp", [0], np.float32]       # needed for ICs...
        #ntab["InternalEnergy"] = ["u", [0], np.float32]          # needed for ICs...

        ntab["SmoothingLengths"] = ["rsp", [0], np.float32]
        ntab["InternalEnergies"] = ["u", [0], np.float32]
        ntab["Densities"] = ["rho", [0], np.float32]
        
        #ntab["Pressure"] = ["p", [0], np.float32]
        #ntab["Entropy"] = ["a", [0], np.float32]
        
        
        ntab["MetalMassFractions"] = ["metals", [0], np.float32]
        ntab["OptVar1"] = ["opt1", [0], np.float32]
        ntab["OptVar2"] = ["opt2", [0], np.float32]
        ntab["SNII_ThermalTime"] = ["snii_thermal_time", [0], np.float32]
        ntab["SNIa_ThermalTime"] = ["snia_thermal_time", [0], np.float32]
                
                
        ###################
        # dm data
        ntab["Softenings"]        = ["softening",   [1], np.float32]   
        
        ###################
        # bndry data
        ntab["Softenings"]        = ["softening",   [2], np.float32]             
        
        
        ###################
        # stars data
        ntab["BirthMasses"]        = ["minit",   [4], np.float32]
        ntab["BirthScaleFactors"]  = ["tstar",   [4], np.float32]        
        ntab["BirthTimes"]         = ["tstar",   [4], np.float32]
        ntab["BirthDensities"]     = ["rhostar", [4], np.float32]
        ntab["MetalMassFractions"] = ["metals", [4], np.float32]
        #ntab["SmoothedMetalMassFractions"] = ["metals", [4], np.float32]
        ntab["ProgenitorIDs"]      = ["idp", [4], np.uint32]
        
        
        

        #ntab["StarFormationTime"] = ["tstar", [1], np.float32]
        #ntab["StarIDProj"] = ["idp", [1], np.uint32]
        #ntab["StarHsml"] = ["rsp", [1], np.float32]
        #ntab["StarMetals"] = ["metals", [1], np.float32]
        #ntab["StarRho"] = ["rho", [1], np.float32]
        
        return ntab

    def get_array_default_value(self):
        """
        Gives a dictionary of default value for pNbody's arrays
        """
        # default value
        dval = {}
        dval["p"] = 0.0
        dval["a"] = 0.0
        dval["acc"] = 0.0
        dval["pos"] = 0.0
        dval["vel"] = 0.0
        dval["num"] = 0.0
        dval["mass"] = 0.0
        dval["pot"] = 0.0
        dval["u"] = 0.0
        dval["rho"] = 0.0
        dval["metals"] = 0.0
        dval["opt1"] = 0.0
        dval["opt2"] = 0.0
        dval["rsp"] = 0.0
        dval["softening"] = 0.0
        dval["minit"] = 0.0
        dval["tstar"] = 0.0
        dval["rhostar"] = 0.0
        dval["idp"] = 0.0
        dval["rsp_stars"] = 0.0
        dval["rho_stars"] = 0.0
        dval["snii_thermal_time"] = 0.0
        dval["snia_thermal_time"] = 0.0
        dval["entropy"] = 0.0
        dval["p"] = 0.0
        return dval

    def get_array_dimension(self):
        """
        Gives a dictionary of dimension for pNbody's arrays
        """
        # dimension
        vdim = {}
        vdim["pos"] = 3
        vdim["vel"] = 3

        vdim["p"] = 1
        vdim["a"] = 1
        vdim["acc"] = 3
        vdim["num"] = 1
        vdim["mass"] = 1
        vdim["pot"] = 1
        vdim["u"] = 1
        vdim["rho"] = 1
        vdim["opt1"] = 1
        vdim["opt2"] = 1
        vdim["rsp"] = 1
        vdim["softening"] = 1
        vdim["minit"] = 1
        vdim["tstar"] = 1
        vdim["rhostar"] = 1
        vdim["idp"] = 1
        vdim["rsp_stars"] = 1
        vdim["rho_stars"] = 1
        vdim["snii_thermal_time"] = 1
        vdim["snia_thermal_time"] = 1
        vdim["entropy"] = 1
        vdim["p"] = 1
        vdim["metals"] = self.ChimieNelements
        return vdim

    def get_default_spec_vars(self):
        """
        return specific variables default values for the class
        """

        return {'massarr': np.array([0, 0, 0, 0, 0, 0]),
                'atime': 0.,
                'redshift': 0.,
                'flag_sfr': 0,
                'flag_feedback': 0,
                'npart_tot': np.array([0, 0, self.nbody, 0, 0, 0]),
                'npart': np.array([0, 0, self.nbody, 0, 0, 0]),
                'flag_cooling': 0,
                'num_files': 1,
                'boxsize': 0.,
                'omega0': 0.,
                'omegalambda': 0.,
                'hubbleparam': 0.,
                'flag_age': 0.,
                'flag_metals': 0.,
                'nallhw': np.array([0, 0, 0, 0, 0, 0]),
                'flag_entr_ics': 0,
                'flag_chimie_extraheader': 0,
                'critical_energy_spec': 0.,
                'empty': 48 * '',
                'comovingintegration': True,
                'hubblefactorcorrection': False,
                'comovingtoproperconversion': True,
                'ChimieNelements': 0,
                'utype':"swift",
                }

    def get_particles_limits_from_npart(self, i):
        """ Gives the limits for a thread.
        In order to get the particles, slice them like this pos[start:end].
        :param int i: Particle type
        :returns: (start, end)
        """
        nber = float(self.npart_tot[i]) / mpi.mpi_NTask()
        start = int(mpi.mpi_ThisTask() * nber)
        end = int((mpi.mpi_ThisTask() + 1) * nber)
        return start, end
        
    def get_particles_limits(self,size):
        """ Gives the limits for a thread.
        In order to get the particles, slice them like this pos[start:end].
        :param int i: Particle type
        :returns: (start, end)
        """
        nber = float(size) / mpi.mpi_NTask()
        start = int(mpi.mpi_ThisTask() * nber)
        end = int((mpi.mpi_ThisTask() + 1) * nber)
        return start, end        
        

    def set_local_value(self):
        N = mpi.mpi_NTask()
        if N == 1:
            return
        part = len(self.npart_tot)
        for i in range(part):
            s, e = self.get_particles_limits_from_npart(i)
            self.npart[i] = e - s

    def get_massarr_and_nzero(self):
        """
        return massarr and nzero

        !!! when used in //, if only a proc has a star particle,
        !!! nzero is set to 1 for all cpu, while massarr has a length of zero !!!
        """

        if self.has_var('massarr') and self.has_var('nzero'):
            if self.massarr is not None and self.nzero is not None:
                if mpi.mpi_IsMaster():
                    print(
                        "warning : get_massarr_and_nzero : here we use massarr and nzero",
                        self.massarr,
                        self.nzero)
                return self.massarr, self.nzero

        massarr = np.zeros(len(self.npart), float)
        nzero = 0

        # for each particle type, see if masses are equal
        for i in range(len(self.npart)):
            first_elt = sum((np.arange(len(self.npart)) < i) * self.npart)
            last_elt = first_elt + self.npart[i]

            if first_elt != last_elt:
                c = (self.mass[first_elt] ==
                     self.mass[first_elt:last_elt]).astype(int)
                if sum(c) == len(c):
                    massarr[i] = self.mass[first_elt]
                else:
                    nzero = nzero + len(c)

        return massarr.tolist(), nzero

    def read_particles(self, f):
        """
        read gadget file
        """
        from copy import deepcopy
        import time
                
        # go to the end of the file
        if f is not None:
            f.seek(0, 2)

        if mpi.mpi_NTask() > 1:
            fd = h5py.File(self.p_name_global[0],'r',driver="mpio",comm=MPI.COMM_WORLD)
        else:
            fd = h5py.File(self.p_name_global[0],'r')

        ################
        # read header
        ################
        if self.verbose > 0 and mpi.mpi_IsMaster():
            print("reading header...")

        # set default values
        default = self.get_default_spec_vars()
        for key, i in list(default.items()):
            setattr(self, key, i)

        # get values from snapshot
        trans = self.get_header_translation()

        list_header = self.get_list_header()
        

        for name in list_header:
            if name not in fd:
                continue
                
            # e.g. create self.npart with the value
            # fd["Header"].attrs["NumPart_ThisFile"]
                        
            for key in fd[name].attrs:
                                                                    
              
                full_name = name + "/" + key
                if full_name not in list(trans.keys()):
                    trans[full_name] = full_name

                tmp = fd[name].attrs[key]
                if isinstance(tmp, bytes) and tmp == "None":
                    tmp = None
                if isinstance(tmp, bytes):
                    tmp = tmp.decode('utf-8')
                setattr(self, trans[full_name], tmp)
                
                
                if self.verbose > 0:
                  print(name,key,">>",trans[full_name],tmp)
                
        

        # additional stuffs
        if self.has_var('cosmorun'):
          if self.cosmorun[0]==0:
            self.atime = self.time[0] 
            self.setComovingIntegrationOff()
        
        # get local value from global ones
        self.set_local_value()
        
       
        
       
        # get the chemical elements and solar abundances
        if "SubgridScheme" in fd:
        
          subgridGrp = fd["SubgridScheme"]
          
          attrs = dict(subgridGrp.attrs)
          
          
          if "Chemistry element count" in attrs: 
            # this case is useful if SolarAbundances is not present
            self.ChimieNelements = int(attrs["Chemistry element count"][0])
            self.arrays_props["metals"]["dim"] = self.ChimieNelements 
            

          if "SolarAbundances" in subgridGrp: 


            d = dict(subgridGrp["SolarAbundances"].attrs)
        
        
            self.ChimieSolarMassAbundances = {}
        
            for key in d.keys():
              self.ChimieSolarMassAbundances[key] = d[key][0]
                    
          
            tmp = subgridGrp["NamedColumns"]
            self.ChimieElements  = list(tmp["MetalMassFractions"][:])    
        
            #print(self.ChimieElements)
        
            for i,elt in enumerate(self.ChimieElements):
              self.ChimieElements[i] = self.ChimieElements[i].decode("utf-8")
        
            self.ChimieNelements = len(self.ChimieElements)
            
            # we update 
            self.arrays_props["metals"]["dim"] = self.ChimieNelements 
          

          
          
        
        
        
        ###############
        # read units
        ###############
        if self.verbose > 0 and mpi.mpi_IsMaster():
            print("reading units...")

        # define system of units
        params = {}

        # consider that if we have length unit, we should have them all
        if hasattr(self, "UnitLength_in_cm"):
            params['UnitLength_in_cm'] = float(self.UnitLength_in_cm)
            if hasattr(self, "UnitVelocity_in_cm_per_s"):
                params['UnitVelocity_in_cm_per_s'] = float(
                    self.UnitVelocity_in_cm_per_s)
            else:
                self.UnitVelocity_in_cm_per_s = float(
                    self.UnitLength_in_cm) / float(self.Unit_time_in_cgs)
                params['UnitVelocity_in_cm_per_s'] = self.UnitVelocity_in_cm_per_s
            params['UnitMass_in_g'] = float(self.UnitMass_in_g)
            self.localsystem_of_units = units.Set_SystemUnits_From_Params(
                params)
        else:
            print("WARNING: snapshot seems broken! There is no units!")
        
        
        mpi.mpi_barrier()
        fd.close()
        
        ################
        # read particles
        ################

        '''
        if self.verbose > 0 and mpi.mpi_IsMaster():
            print("reading particles...")

        def getsize(n, dim):
            if dim == 1:
                return (n,)
            if dim > 1:
                return (n, dim)

        list_of_vectors = []

        # check what vector we needs to be created

        ntab = self.get_array_translation()
        vdim = self.get_array_dimension()
        dval = self.get_array_default_value()

        for i, n in enumerate(self.npart):

            if n != 0:

                if list(fd.keys()).count("PartType%d" % i) == 0:
                    raise IOError(
                        "type=%d n=%d but group %s is not found !" %
                        (i, n, "PartType%d" %
                         i))

                # loop over dataset
                block = fd["PartType%d" % i]
                for key in list(block.keys()):
                    if key not in ntab:
                        if self.verbose > 1:
                          print("'%s' (PartType=%d) key not recognized, please add it to config/format/swift.py file in ntab" %(key,i))
                        continue  # sys.exit(2)

                    varname = ntab[key][0]

                    if self.has_var(varname):
                        pass  # do nothing
                    else:
                        setattr(self, varname, None)

                    # record the variable in a list
                    if list_of_vectors.count(varname) == 0:
                        list_of_vectors.append(varname)

        # read the tables
        for i, n in enumerate(self.npart):

            if n != 0:
                if self.verbose > 1 and mpi.mpi_IsMaster():
                    print("Reading particles (type %i)..." % i)

                remaining_vectors = deepcopy(list_of_vectors)

                # loop over dataset
                block = fd["PartType%d" % i]
                if mpi.mpi_NTask() > 1:
                    init, end = self.get_particles_limits_from_npart(i)

                for key in list(block.keys()):
                    if key not in ntab:
                        continue
                    varname = ntab[key][0]
                    
                    var = getattr(self, varname)

                    if mpi.mpi_NTask() > 1:
                        data = block[key][init:end]
                    else:
                        data = block[key][()]
                    if var is not None:                             # here we should set the type
                        setattr(self, varname, np.concatenate(
                            (getattr(self, varname), data)))
                    else:
                        setattr(self, varname, data)      

                    remaining_vectors.remove(varname)

                # loop over vectors absent from the block
                for varname in remaining_vectors:
                    var = getattr(self, varname)
                    data = (
                        np.ones(
                            getsize(
                                n,
                                vdim[varname])) *
                        dval[varname]).astype(
                        np.float32)
                    if var is not None:
                        setattr(self, varname, np.concatenate(
                            (getattr(self, varname), data)))
                    else:
                        setattr(self, varname, data)

        '''
                
        # get npart from the blocks
        self.npart = self.get_npart_from_dataset(ptypes=self.ptypes)
        
        # check that arrays are present
        self.set_readable_arrays(ptypes=self.ptypes)
        
        # load all arrays according to the information
        # provided by self.arrays_props
        self.read_arrays(ptypes=self.ptypes)
                

        # set tpe
        self.tpe = np.array([], np.int32)
        for i in range(len(self.npart)):
            self.tpe = np.concatenate((self.tpe, np.ones(self.npart[i]) * i))

        # compute nzero
        nzero = 0
        mass = np.array([])

        for i in range(len(self.npart)):
            if self.massarr[i] == 0:
                nzero = nzero + self.npart[i]
            else:
                print(
                    "Massarr is not supported! Please specify the mass of all the particles!")

        self.nzero = nzero

        mpi.mpi_barrier()
        
        


        # specific final conversions
        
        if hasattr(self, 'idp'):
          self.idp = self.idp.astype(np.int) 
        
        if type(self.atime) == np.ndarray:
          self.atime = self.atime[0]
          
        if type(self.redshift) == np.ndarray:
          self.redshift = self.redshift[0]
        
        if type(self.hubbleparam) == np.ndarray:
          self.hubbleparam = self.hubbleparam[0]
          
        if type(self.omega0) == np.ndarray:
          self.omega0 = self.omega0[0]        

        if type(self.omegalambda) == np.ndarray:
          self.omegalambda = self.omegalambda[0]   
          
          
          
          

    def write_particles(self, f):
        """
        specific format for particle file
        """
        # go to the end of the file
        if f is not None:
            f.seek(0, 2)
            

        name = "Unit_temp_in_cgs"
        if not hasattr(self, name):
            setattr(self, name, 1.0)

        name = "Unit_current_in_cgs"
        if not hasattr(self, name):
            setattr(self, name, 1.0)

        import h5py
        # not clean, but work around pNbody
        filename = self.p_name_global[0]
        # open file
        if mpi.mpi_NTask() > 1:
            from mpi4py import MPI
            h5f = h5py.File(filename, "w", driver="mpio", comm=MPI.COMM_WORLD)
        else:
            h5f = h5py.File(filename, "w")

        # add units to the usual gh5 struct
        if hasattr(self, "unitsparameters"):
            units = self.unitsparameters.get_dic()
            for key, i in list(units.items()):
                if not hasattr(self, key):
                    setattr(self, key, i)

        if hasattr(self,"UnitVelocity_in_cm_per_s") and hasattr(self,"UnitLength_in_cm"):
          self.Unit_time_in_cgs = self.UnitLength_in_cm / self.UnitVelocity_in_cm_per_s
        
 
          

        ############
        # HEADER
        ############
        if self.verbose > 0 and mpi.mpi_IsMaster():
            print("Writing header...")

        list_header = self.get_list_header()
        trans = self.get_header_translation()
        # cheat a little bit in order to get the real number of particles in
        # this file
        trans["Header/NumPart_ThisFile"] = "npart_tot"
        
        for name in list_header:
            h5f.create_group(name)
        
                    
            
        for key in self.get_list_of_vars():
          
            #self.atime    = np.array(self.atime,dtype=float)   
            #self.redshift = np.array(self.redshift,dtype=float)  
            #self.omegalambda     = np.array(self.omegalambda,dtype=float) 
            #self.omega0          = np.array(self.omega0,dtype=float)  
            #self.hubbleparam     = np.array(self.hubbleparam,dtype=float) 
                      
          
            if key in list(trans.values()):
                ind = list(trans.values()).index(key)
                name, hdf5 = list(trans.keys())[ind].split("/")
                value = getattr(self, key)
                
            
                if type(value) is not str:
                  value = np.array(value)
                else:
                  value = np.array(value,dtype="S")  
                  
                if value.shape == ():
                  value = np.array([value])
                  
                
                if not isinstance(value, dict):
                    if value is None:
                        h5f[name].attrs[hdf5] = "None"
                    else:
                        h5f[name].attrs[hdf5] = value

        ##############
        # PARTICULES
        ##############
        if self.verbose > 0 and mpi.mpi_IsMaster():
            print("Writing particles...")

        for i in range(len(self.npart)):
            if self.massarr[i] != 0:
                print(
                    "Massarr is not supported! Please specify the mass of all the particles!")

        ntab = self.get_array_translation()
                
        # get particles type present
        type_part = []
        for i in range(len(self.npart)):
            if self.npart[i] > 0:
                type_part.append(i)

        # write particles
        for i in type_part:
            if mpi.mpi_NTask() > 1:
                init, end = self.get_particles_limits_from_npart(i)                
            if self.verbose > 1 and mpi.mpi_IsMaster():
                print(("Writing particles (type %i)..." % i))
            group = "PartType%i" % i
            grp = h5f.create_group(group)
            nb_sel = self.select(i)                                   # this is really bad as we duplicate the model
            
            for key, j in list(ntab.items()):
                varname = j[0]
                var_type = j[1]
                if (var_type is True) or i in var_type:
                    if hasattr(nb_sel, varname):
                        # get and transform type
                        tmp = getattr(nb_sel, varname)
                        if tmp is None:
                            continue
                        tmp = tmp.astype(j[2])
                        # create dataset
                        size = list(tmp.shape)
                        size[0] = self.npart_tot[i]
                        if mpi.mpi_NTask() > 1:
                            dset = grp.create_dataset(key, size, dtype=j[2])
                            dset[init:end] = tmp
                        else:
                            h5f[group][key] = tmp

        
        # write the chemical elements and solar abundances
        
        '''
        subgridGrp = fd["SubgridScheme"]
        d = dict(subgridGrp["SolarAbundances"].attrs)
        
        self.ChimieSolarMassAbundances = {}
        
        for key in d.keys():
          self.ChimieSolarMassAbundances[key] = d[key][0]
          
        tmp = subgridGrp["NamedColumns"]
        self.ChimieElements  = list(tmp["MetalMassFractions"][:])    
        for i,elt in enumerate(self.ChimieElements):
          self.ChimieElements[i] = self.ChimieElements[i].decode("utf-8")
        
        self.ChimieNelements = len(self.ChimieElements)       
        '''
        
        if self.has_var("ChimieSolarMassAbundances"):

                
          subgridGrp = h5f.create_group("SubgridScheme")
          tmp = h5f.create_group("SubgridScheme/SolarAbundances")
        
          for key, val in self.ChimieSolarMassAbundances.items():
            tmp.attrs[key] = np.array([val],np.float32)

          tmp = h5f.create_group("SubgridScheme/NamedColumns")
          asciiList = [n.encode("ascii", "ignore") for n in self.ChimieElements]
          tmp.create_dataset('MetalMassFractions', (len(asciiList),),'S10', asciiList)
        
        
        h5f.close()
        
        

        
