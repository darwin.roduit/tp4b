#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 7 18:41:44 2023

@author: Darwin

Output files produced by this script :
The data produced are stored in .npz files that can be retrieved using
numpy.load(). This will return an object working like a dictionnary to retrieve
the data. The files generated and the name of the variables are the following:
    - density_profile_data.npz : contains data about the density profiles of
      the stars and DM at different times
        - r_stars (dtype=object): radius of the stars
        - r_DM (dtype=object): radius of the dark matter
        - density_stars (dtype=object): density profile of the stars
        - density_stars (dtype=object): density profile of the dark matter
        - t (dtype=float): time of the snapshots used for the density profiles
    - mass_profile_data.npz : contains data about the mass profiles of the
      stars and DM at different times
        - r_stars (dtype=object): radius of the stars
        - r_DM (dtype=object): radius of the dark matter
        - mass_stars (dtype=object): mass profile of the stars
        - mass_stars (dtype=object): mass profile of the dark matter
        - t (dtype=float): time of the snapshots used for the mass profiles
    - radius_of_half_mass.npz : contains data about the time evolution of the
      radius containing half of the mass of the stars and dark matter ; the
      grid is not fixed
        - mass_half_stars (one float): half mass of the stars in the system
        - mass_half_DM (one float): half mass of the DM in the system
        - r_half_stars (dtype=float): radius containing the half mass of the
          stars
        - D_r_half_stars (dtype=float): uncertainty on r_half_stars ; it was
          estimated by taking the maximum interval between the previous radius
          and next one in the grids.
        - mass_half_stars_bin (dtype=float): the half mass in the grids
        - r_half_DM (dtype=float): radius containing the half mass of the DM
        - D_r_half_DM (dtype=float): uncertainty on r_half_DM ; it was
          estimated by taking the maximum interval between the previous radius
          and next one in the grids.
        - mass_half_DM_bin (dtype=float): the half mass in the grids
        - t (dtype=float): time of every snapshots
    - mass_at_given_radius.npz : contains data about the evolution in time of
      the mass contained within a provided radius r_tilde (/!\ not star*s*) for
      the stars and dark matter; the grid is kept fixed
        - r_tilde (one float): radius within which the mass is determined
        - r_star_bin (one float): closest value of r_tilde available in the
          grid
        - mass_star_stars (dtype=float): mass of the stars contained within the
          radius r_star_bin
        - D_m_star_stars (dtype=float): uncertainty on mass_star_stars
        - mass_star_DM (dtype=float): mass of the DM contained within the
          radius r_star_bin
        - D_m_star_DM (dtype=float): uncertainty on D_m_star_DM
        - t (dtype=float): time of every snapshots
    - velocity_dispersion.npz : contains data about the velocity dispersion
      (sigma_z) for the stars and dark matter at different times
        - r_stars (dtype=object): radius of the stars
        - r_DM (dtype=object): radius of the dark matter
        - sigma_stars (dtype=object): velocity dispersion of the stars
        - D_sigma_stars (dtype=object): uncertainty on sigma_stars
        - sigma_DM (dtype=object): velocity dispersion of DM
        - D_sigma_DM (dtype=object): uncertainty on sigma_DM
        - t (dtype=float): time of the snapshots used for the mass profiles
    - wolf_relation.npz : contains data about Wolf relation
        - M_half_wolf (dtype=float): mass half light computed with Wolf
          relation
        - D_M_half_wolf (dtype=float): uncertainty on M_half_wolf
        - M_dyn (dtype=float): dynamical mass of the system with the radius R_e
          in Wolf relation
        - t (dtype=float): time of every snapshots
"""

import os
import numpy as np
import argparse

from concurrent.futures import ProcessPoolExecutor

from matplotlib import cm

from argparse_utils import RawTextArgumentDefaultsHelpFormatter
import data_analysis_module.spherical_density_profile as spherical_density_profile
import data_analysis_module.half_mass_radius as half_mass_radius
import data_analysis_module.mass_given_radius as mass_given_radius
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, choose_n_filenames, get_all_snapshot_in_directory
import data_analysis_module.spherical_mass_profile as spherical_mass_profile
import data_analysis_module.velocity_dispersion as velocity_dispersion
import data_analysis_module.wolf_relation as wolf_relation

# %%Parameters


def parse_options():
    """Parse the options given in the terminal"""
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter,
                                     description="Analyses the snapshots produced "
                                     "by simulating tidal striping of ultra faint dwarf galaxies.")

    parser.add_argument(
        'input_location', help='Folder containing all the snapshot of one simulation.')
    parser.add_argument('output_location',
                        help='Output folder to store the results.')

    parser.add_argument('--serial_computation', default=False,
                        action="store_true",
                        help="False : Does the analysis in a parallelized"
                        "manner ; False : Does the analysis without"
                        "parallelization.")

    parser.add_argument('--ftype', default="swift", type=str,
                        help='Type of the simulation files, e.g. swift, gadget,'
                        'etc.')

    parser.add_argument('--snap_basename', default="snapshot", type=str,
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")

    parser.add_argument('--n_profile', default=10, type=int,
                        help="Number of snapshots to use to determine the spherical "
                        "mass profile and the velocity dispersion evolution in time.")

    parser.add_argument('--n_bins', default=64, type=int,
                        help="Number of bins of the grid.")

    parser.add_argument('--n_los', default=10, type=int,
                        help="Number of lines of sight.")

    parser.add_argument('--n_stars_per_bin_1', default=20, type=int,
                        help="Number of stars per bin (sub plots).")

    parser.add_argument('--n_stars_per_bin_2', default=40, type=int,
                        help="Number of stars per bin (total plot).")

    parser.add_argument('--n_DM_per_bin_1', default=250, type=int,
                        help="Number of DM particles per bin (sub plots)"
                        "(except for half mass radius).")

    parser.add_argument('--n_DM_per_bin_2', default=250, type=int,
                        help="Number of DM particles per bin (total plot) (except for half mass radius).")

    parser.add_argument('--n_DM_per_bin_1_half_mass', default=10000, type=int,
                        help="Number of DM particles per bin (sub plots) for the half mass computations. "
                        "Warning: Set too low values results in high computing times.")

    parser.add_argument('--n_DM_per_bin_2_half_mass', default=20000, type=int,
                        help="Number of DM particles per bin (total plot) for the half mass computations. "
                        "Warning: Set too low values results in high computing times.")

    parser.add_argument('--r_tilde', default=0.25, type=float,
                        help="Fixed radius at which the evolution in time of the mass will be computed. ")

    parser.add_argument('--r_max_DM', default=20, type=float,
                        help="Maximal radius of the grid for the dark matter.")

    parser.add_argument('--r_max_stars', default=0.5, type=float,
                        help="Maximal radius of the grid for the stars.")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Centers the snapshot on the minimum of the potential ; False : Does nothing.")

    parser.add_argument('--no_DM_half_light_radius_computations', default=False, action="store_true",
                        help="True : Performs the computations of the DM half-light radius. This takes a lot of time."
                        "False : Does not do it ; it's quicker.")

    args = parser.parse_args()
    return args


def analyze_data(args):
    """Analyze the data."""
    print("\n-------------Welcome to data_analysis.py-------------\n")
    output_location = args.output_location

    # Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)

    # Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location = output_location+"/"

    # --------------Files lists preparation
    n_snapshot = get_number_snapshot_directory(
        args.input_location, args.snap_basename)  # number of snapshots

    # Determines n files to draw profiles, ...
    files_n_profile = choose_n_filenames(
        n_snapshot, args.n_profile, args.snap_basename, args.input_location)

    # Gets all files in the snapshot directory
    files = get_all_snapshot_in_directory(
        args.input_location, args.snap_basename)

    # Colormap (chooses n colors in the colormap)
    colours = cm.rainbow(np.linspace(0, 1, args. n_profile+1))

    if (not args.serial_computation):  # Parallelization
        # Creates the process for parallelization
        with ProcessPoolExecutor() as executor:
            # Submit the tasks to be executed in parallel
            task_density_profile = executor.submit(
                spherical_density_profile.spherical_density_profile,
                files_n_profile, args.n_profile, args.ftype, args.n_bins,
                args.do_potcenter, args.no_DM_half_light_radius_computations)
            task_mass_profile = executor.submit(spherical_mass_profile.spherical_mass_profile,
                                                files_n_profile,
                                                args.n_profile, args.ftype,
                                                args.n_bins, args.do_potcenter, 
                                                args.no_DM_half_light_radius_computations)

            task_mass_given_radius = executor.submit(mass_given_radius.mass_given_radius, files,
                                                     n_snapshot, args.ftype,
                                                     args.n_bins, args.r_tilde,
                                                     args.r_max_stars,
                                                     args.do_potcenter, 
                                                     args.no_DM_half_light_radius_computations)
            task_velocity_dispersion = executor.submit(velocity_dispersion.velocity_dispersion,
                                                       files_n_profile,
                                                       args.n_profile,
                                                       args.ftype,
                                                       args. n_DM_per_bin_1,
                                                       args.n_DM_per_bin_2,
                                                       args.n_stars_per_bin_1,
                                                       args.n_stars_per_bin_2,
                                                       args.n_los,
                                                       args.r_max_DM,
                                                       args.r_max_stars,
                                                       args.do_potcenter, 
                                                       args.no_DM_half_light_radius_computations)

            task_half_mass = executor.submit(half_mass_radius.half_mass_radius,
                                             files, n_snapshot, args.ftype,
                                             args.n_los,  args.n_bins,
                                             args.n_stars_per_bin_1,
                                             args.n_stars_per_bin_2,
                                             args.n_DM_per_bin_1_half_mass,
                                             args.n_DM_per_bin_2_half_mass,
                                             args.r_max_stars,
                                             args.r_max_DM, args.do_potcenter,
                                             args.no_DM_half_light_radius_computations)

            # ----Retrieve the results, save and make the plot (in parallel
            # !). Also, perform Wolf relation once its dependency is done

            # Density profile
            r_array_stars_d, r_array_DM_d, density_array_stars, density_array_DM, time_array_n_profile_d = task_density_profile.result()
            executor.submit(np.savez_compressed,
                            output_location+"density_profile_data",
                            r_stars=r_array_stars_d, r_DM=r_array_DM_d,
                            density_stars=density_array_stars,
                            density_DM=density_array_DM,
                            t=time_array_n_profile_d)
            executor.submit(spherical_density_profile.makePlot,
                            output_location, r_array_stars_d, r_array_DM_d,
                            density_array_stars, density_array_DM,
                            time_array_n_profile_d, colours)

            # Mass profile
            r_array_stars_m, r_array_DM_m, mass_array_stars, mass_array_DM, time_array_n_profile_m = task_mass_profile.result()
            executor.submit(np.savez_compressed,
                            output_location+"mass_profile_data",
                            r_stars=r_array_stars_m, r_DM=r_array_DM_m,
                            mass_stars=mass_array_stars, mass_DM=mass_array_DM,
                            t=time_array_n_profile_m)
            executor.submit(spherical_mass_profile.makePlot, output_location,
                            r_array_stars_m, r_array_DM_m, mass_array_stars,
                            mass_array_DM, time_array_n_profile_m, colours)

            # Mass at given radius
            r_star_approx, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM, time_array_m = task_mass_given_radius.result()
            executor.submit(np.savez_compressed,
                            output_location+"mass_at_given_radius",
                            r_tilde=args.r_tilde, r_star_bin=r_star_approx,
                            mass_star_stars=mass_star_stars,
                            mass_star_DM=mass_star_DM,
                            D_m_star_stars=Delta_m_star_stars,
                            D_m_star_DM=Delta_m_star_DM, t=time_array_m)
            executor.submit(mass_given_radius.makePlot, output_location,
                            args.r_tilde, time_array_m, mass_star_stars,
                            mass_star_DM, Delta_m_star_stars, Delta_m_star_DM)

            # Velocity dispersion
            r_array_stars_s, r_array_DM_s, sigma_array_stars, sigma_array_DM, D_sigma_array_stars, D_sigma_array_DM, time_array_n_profile_s = task_velocity_dispersion.result()
            executor.submit(np.savez_compressed,
                            output_location+"velocity_dispersion",
                            r_stars=r_array_stars_s, r_DM=r_array_DM_s,
                            sigma_stars=sigma_array_stars,
                            sigma_DM=sigma_array_DM,
                            D_sigma_stars=D_sigma_array_stars,
                            D_sigma_DM=D_sigma_array_DM,
                            t=time_array_n_profile_s)
            executor.submit(velocity_dispersion.makePlot, output_location,
                            r_array_stars_s,
                            r_array_DM_s, sigma_array_stars, sigma_array_DM,
                            time_array_n_profile_s, colours)

            # Half mass
            mass_half_stars, mass_half_DM, r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time_array_h = task_half_mass.result()
            executor.submit(np.savez_compressed,
                            output_location+"radius_of_half_mass",
                            mass_half_stars=mass_half_stars,
                            mass_half_DM=mass_half_DM,
                            r_half_stars=r_half_stars,
                            D_r_half_stars=Delta_r_half_stars,
                            r_half_DM=r_half_DM, D_r_half_DM=Delta_r_half_DM,
                            t=time_array_h)
            executor.submit(half_mass_radius.makePlot, output_location,
                            time_array_h, r_half_DM, Delta_r_half_DM,
                            r_half_stars, Delta_r_half_stars)

            # Submit Wolf relation
            task_wolf_relation = executor.submit(wolf_relation.wolf_relation,
                                                 files, n_snapshot, args.ftype,
                                                 args.n_stars_per_bin_1,
                                                 args.n_stars_per_bin_2,
                                                 args.n_los, mass_half_stars,
                                                 args.do_potcenter)
            M_half_wolf, mass_dyn, D_M_half_wolf, time_array_w = task_wolf_relation.result()
            executor.submit(np.savez_compressed, output_location+"wolf_relation",
                            M_half_wolf=M_half_wolf, M_dyn=mass_dyn,
                            D_M_half_wolf=D_M_half_wolf, t=time_array_w)
            executor.submit(wolf_relation.makePlot, output_location,
                            M_half_wolf, mass_dyn, D_M_half_wolf, time_array_w)

    else:  # No parallelization
        # --------------Density profile
        r_array_stars_d, r_array_DM_d, density_array_stars, density_array_DM, time_array_n_profile_d = spherical_density_profile.spherical_density_profile(
            files_n_profile, args.n_profile, args.ftype, args.n_bins, args.do_potcenter, args.no_DM_half_light_radius_computations)

        # Save the data
        np.savez_compressed(output_location+"density_profile_data",
                            r_stars=r_array_stars_d, r_DM=r_array_DM_d,
                            density_stars=density_array_stars,
                            density_DM=density_array_DM,
                            t=time_array_n_profile_d)

        # Make plot
        spherical_density_profile.makePlot(output_location, r_array_stars_d,
                                           r_array_DM_d,
                                           density_array_stars,
                                           density_array_DM,
                                           time_array_n_profile_d, colours)

        # --------------Spherical mass profile
        r_array_stars_m, r_array_DM_m, mass_array_stars, mass_array_DM, time_array_n_profile_m = spherical_mass_profile.spherical_mass_profile(
            files_n_profile, args.n_profile, args.ftype, args.n_bins,
            args.do_potcenter, args.no_DM_half_light_radius_computations)

        # Save the data
        np.savez_compressed(output_location+"mass_profile_data",
                            r_stars=r_array_stars_m, r_DM=r_array_DM_m,
                            mass_stars=mass_array_stars, mass_DM=mass_array_DM,
                            t=time_array_n_profile_m)

        # Make plot
        spherical_mass_profile.makePlot(output_location, r_array_stars_m,
                                        r_array_DM_m,
                                        mass_array_stars, mass_array_DM,
                                        time_array_n_profile_m, colours)

        # --------------Half mass
        mass_half_stars, mass_half_DM, r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time_array_h = half_mass_radius.half_mass_radius(files, n_snapshot, args.ftype, args.n_los,
                                                                                                                                                      args.n_bins, args.n_stars_per_bin_1, args.n_stars_per_bin_2,  args.n_DM_per_bin_1_half_mass, args.n_DM_per_bin_2_half_mass,
                                                                                                                                                      args.r_max_stars, args.r_max_DM, args.do_potcenter, args.no_DM_half_light_radius_computations)

        # Save the data
        np.savez_compressed(output_location+"radius_of_half_mass", mass_half_stars=mass_half_stars,
                            mass_half_DM=mass_half_DM, r_half_stars=r_half_stars, D_r_half_stars=Delta_r_half_stars,
                            r_half_DM=r_half_DM, D_r_half_DM=Delta_r_half_DM, t=time_array_h)

        # Make plot
        half_mass_radius.makePlot(output_location, time_array_h,
                                  r_half_DM, Delta_r_half_DM, r_half_stars, Delta_r_half_stars)

        # --------------Mass at a given radius r_tilde
        r_star_approx, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM, time_array_m = mass_given_radius.mass_given_radius(files, n_snapshot, args.ftype,
                                                                                                                                              args.n_bins, args.r_tilde, args.r_max_stars, args.do_potcenter, args.no_DM_half_light_radius_computations)

        # Save the data
        np.savez_compressed(output_location+"mass_at_given_radius",
                            r_tilde=args.r_tilde, r_star_bin=r_star_approx,
                            mass_star_stars=mass_star_stars,
                            mass_star_DM=mass_star_DM,
                            D_m_star_stars=Delta_m_star_stars,
                            D_m_star_DM=Delta_m_star_DM, t=time_array_m)

        # Make plot
        mass_given_radius.makePlot(output_location, args.r_tilde, time_array_m,
                                   mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM)

        # --------------Velocity dispersion
        r_array_stars_s, r_array_DM_s, sigma_array_stars, sigma_array_DM, D_sigma_array_stars, D_sigma_array_DM, time_array_n_profile_s = velocity_dispersion.velocity_dispersion(files_n_profile, args.n_profile,
                                                                                                                                                                                  args.ftype, args.n_DM_per_bin_1, args.n_DM_per_bin_2,  args.n_stars_per_bin_1, args.n_stars_per_bin_2, args.n_los,
                                                                                                                                                                                  args.r_max_DM, args.r_max_stars, args.do_potcenter, args.no_DM_half_light_radius_computations)

        # Save the data
        np.savez_compressed(output_location+"velocity_dispersion",
                            r_stars=r_array_stars_s,
                            r_DM=r_array_DM_s, sigma_stars=sigma_array_stars,
                            sigma_DM=sigma_array_DM,
                            D_sigma_stars=D_sigma_array_stars,
                            D_sigma_DM=D_sigma_array_DM,
                            t=time_array_n_profile_s)

        # Make plot
        velocity_dispersion.makePlot(output_location, r_array_stars_s,
                                     r_array_DM_s, time_array_n_profile_s,
                                     colours)

        # --------------Relation de Wolf
        M_half_wolf, mass_dyn, D_M_half_wolf, time_array_w = wolf_relation.wolf_relation(files, n_snapshot, args.ftype, args.n_stars_per_bin_1,
                                                                                         args.n_stars_per_bin_2, args.n_los, mass_half_stars, args.do_potcenter)

        # Save the data
        np.savez_compressed(output_location+"wolf_relation",
                            M_half_wolf=M_half_wolf,
                            M_dyn=mass_dyn, D_M_half_wolf=D_M_half_wolf,
                            t=time_array_w)

        # Make plot
        wolf_relation.makePlot(output_location, M_half_wolf,
                               mass_dyn, D_M_half_wolf, time_array_w)

    # --------------Writes into a text file the units of our data (so that we
    # --------------do not forget them)

    with open(output_location+'Readme', 'w') as readme:
        readme.write(
            'The units of the data contained in this folder are :\nlength:'
            'kpc\nmass: M_sun\ntime: Gyr\nvelocity: km/s')

    print("\n-------------End of data_analysis.py-------------\n")
    return


# %%
if __name__ == '__main__':
    args = parse_options()
    analyze_data(args)
