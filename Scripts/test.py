#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 15 13:38:45 2023

@author: darwin
"""

from galpy.potential import NFWPotential
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
from astropy import units as u
import astropy.coordinates as coord
from galpy.potential import MWPotential2014
from galpy.orbit import Orbit

coord.galactocentric_frame_defaults.set("v4.0")

# %%


def compute_distance(distance_modulus):
    return 10**(distance_modulus/5.0 + 1)


def convert_ICRS_to_carthesian(distance_modulus, right_ascension, declination,
                               pm_RA, pm_DEC, v_radial, unit_length, unit_velocity):
    dist_particles = compute_distance(distance_modulus)*u.pc
    dist_particles = dist_particles.to(u.kpc).value
    ics = coord.SkyCoord(frame='icrs', ra=right_ascension*u.degree, dec=declination*u.degree,
                         distance=dist_particles*u.kpc,
                         # pm_ra=pm_RA*u.mas/u.yr,
                         pm_ra_cosdec=pm_RA*u.mas/u.yr,
                         pm_dec=pm_DEC*u.mas/u.yr,
                         radial_velocity=v_radial*u.km/u.s
                         # , differential_type=coord.SphericalDifferential
                         )
    ics = ics.transform_to(coord.Galactocentric(galcen_distance=8.129*u.kpc,
                                                galcen_v_sun=(11.1, 241.24, 7.25)*u.km/u.s))
    # Using the parameters given in sec7, p. 13 from Battaglia et al.
    position = ics.data.get_xyz().to(unit_length)
    velocity = ics.velocity.get_d_xyz().to(unit_velocity)
    return position, velocity

# %%Velocity and positions computations


unit_length = 3.086e21*u.cm
unit_length = unit_length.to(u.kpc)
unit_mass = 1.98848e43*u.g
unit_mass = unit_mass.to(1e10*u.M_sun)
unit_time = 3.086e16*u.s
unit_time = unit_time.to(u.Gyr)
unit_velocity = (unit_length/unit_time).to(u.cm/u.s)
unit_velocity = unit_velocity.to(u.km/u.s)

pos_bootes_1, vel_bootes_1 = convert_ICRS_to_carthesian(19.11, 210.02250, 14.50060,
                                                        -0.39, -1.06, 101.80,
                                                        unit_length,
                                                        unit_velocity)
pos_hercules, vel_hercules = convert_ICRS_to_carthesian(20.68, 247.77220, 12.78520,
                                                        -0.04, -0.34, 45.00,
                                                        unit_length,
                                                        unit_velocity)
pos_tucana_2, vel_tucana_2 = convert_ICRS_to_carthesian(18.80, 342.97960, -58.56890,
                                                        0.91, -1.27, -129.10,
                                                        unit_length,
                                                        unit_velocity)

# %%
t = np.linspace(0, 3, 1000)*u.Gyr
# c = coord.SkyCoord(x=30*u.kpc, y=0*u.kpc, z=0*u.kpc, v_x=0*u.km/u.s,
#                     v_y=188*u.km/u.s, v_z=0*u.km/u.s,
#                     frame=coord.Galactocentric)
c = coord.SkyCoord(x=0*u.kpc, y=5*u.kpc, z=0*u.kpc,
                   v_x=225.55900735974072*u.km/u.s,
                   v_y=0*u.km/u.s, v_z=0*u.km/u.s, m
                   frame=coord.Galactocentric)
# c = coord.SkyCoord(x=0*u.kpc, y=0*u.kpc, z=5*u.kpc, v_x=198*u.km/u.s,
#                     v_y=0*u.km/u.s, v_z=0*u.km/u.s,
#                     frame=coord.Galactocentric)
orbit = Orbit(c)
orbit.integrate(t, MWPotential2014)
orbit.plot3d()
orbit.plot()

# %%Check des valeurs des paramètres dans Galpy
nfw = NFWPotential(a=2.0, normalize=0.35)  # 16/8 = 2 de Galpy
# nfw = NFWPotential(a=2.0) #16/8 = 2 de Galpy

# Attention, tout est en unité de galpy...
r_s = nfw.a*8.0
c = nfw.conc()
M_vir = nfw.mvir() * 1e12*u.M_sun
M_vir = M_vir.to(1e10*u.M_sun)
r_vir = nfw.rvir(use_physical=True, ro=8.0, vo=220)*u.kpc

print("r_s = {}   c = {}   M_vir = {}    r_vir = {}   amp = {}".format(
    r_s, c, M_vir, r_vir, nfw._amp))

# %%
bulge = MWPotential2014[0]
alpha = bulge.alpha
r_c = bulge.rc*8
amp = bulge._amp
print("alpha={}   r_c = {}   amp = {}   ".format(alpha, r_c, amp))

# Attention aux unités : Est-ce que c'est normalisé
mn = MWPotential2014[1]
a = mn._a*8
b = mn._b*8
amp = mn._amp
print("R_disk = {}    z_disk = {}    amp = {}    ".format(a, b, amp))
