#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
#from numba import njit

from pylab import *

import Ptools as pt
from pNbody import *
from pNbody import units
from pNbody import ctes
from pNbody import libgrid, libdisk 

plt.style.use(r"/home/darwin/Bureau/TP4b/Scripts/lab_style_figure.mplstyle")

#%%Functions
def f(r, rc): return np.log(r / rc + 1.)
def fm(r, rc): return rc * (np.exp(r) - 1.)

def SetAxis(ax, xmin, xmax, ymin, ymax, log=None):
    #################################

    if log is not None:
        if str.find(log, 'x') != -1 and str.find(log, 'y') != -1:
            loglog()
        elif str.find(log, 'x') != -1:
            semilogx()
        else:
            semilogy()

    axis([xmin, xmax, ymin, ymax])

    if log is None:
        log = 'None'

    if str.find(log, 'x') == -1:
        ax.xaxis.set_major_locator(AutoLocator())
        try:
            x_major = ax.xaxis.get_majorticklocs()
            dx_minor = (x_major[-1] - x_major[0]) / (len(x_major) - 1) / 5.
            ax.xaxis.set_minor_locator(MultipleLocator(dx_minor))
        except BaseException:
            print("SetAxis : you should uptdate matplotlib")

    if str.find(log, 'y') == -1:
        ax.yaxis.set_major_locator(AutoLocator())
        try:
            y_major = ax.yaxis.get_majorticklocs()
            dy_minor = (y_major[-1] - y_major[0]) / (len(y_major) - 1) / 5.
            ax.yaxis.set_minor_locator(MultipleLocator(dy_minor))
        except BaseException:
            print("SetAxis : you should uptdate matplotlib")
        return 

def density_spherical_grid(files, **kwargs):
    #kwargs parameters----------------------------------------------------------------
    ftype=kwargs.get("ftype", "swift") ; part_type=kwargs.get("part_type", None)
    r_max=kwargs.get("r_max", 50) ; n_bins=kwargs.get("n_bins", 32)
    #--------------------------------------------------------------------------
    
    colors = pt.Colors(n=1)
    datas = []
    
    rc = 1    
    def f(r): return np.log(r / rc + 1.)
    def fm(r): return rc * (np.exp(r) - 1.)
    
    for file in files:
        nb = Nbody(file, ftype=ftype)
        nb.potcenter()
        if part_type is not None:
            nb = nb.selectc(nb.tpe[nb.tpe == part_type]) #selects the type of particules
            
        nb.potcenter() #Centers at the min of the potential
        G = libgrid.Spherical_1d_Grid(rmin=0, rmax=r_max, nr=n_bins, g=f, gm=fm)
        
        # nb.pos = nb.Pos("kpc") ; 
        # nb.vel = nb.Vel("km/s") ; 
        # nb.mass = nb.Mass("Ms")
        
        x = G.get_r()
        y = G.get_DensityMap(nb)
        
        # output units
        out_units_x = units.UnitSystem('local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
        Unit_atom.set_symbol('Unit_atom')
        out_units_y = units.UnitSystem('local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])
    
        fx = nb.localsystem_of_units.convertionFactorTo(out_units_x.UnitLength)
        fy = nb.localsystem_of_units.convertionFactorTo(out_units_y.UnitDensity)
    
        x = x * fx
        y = y * fy
                
        x, y = pt.CleanVectorsForLogX(x, y)
        x, y = pt.CleanVectorsForLogY(x, y)
        datas.append(pt.DataPoints(x, y, color=colors.get(), label=file, tpe='points'))
        
    #Sets labels
    xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
    ylabel = r'$\rm{Density}\,\left[ \rm{atom/cm^3} \right]$'
    return datas, xlabel, ylabel

def V_circ_spherical_grid(nb, r, M, spehrical_grid):
    """Computes the circular velocity on a spherical grid"""
    
    # Newton theorem
    G=ctes.GRAVITY.into(nb.localsystem_of_units)   
    vc = np.sqrt(G*M/r) 
    return vc

def v_circ_spherical_grid(files, **kwargs):
    #kwargs parameters---------------------------------------------------------
    ftype=kwargs.get("ftype", "swift") ; part_type=kwargs.get("part_type", None)
    r_max=kwargs.get("r_max", 50) ; n_bins=kwargs.get("n_bins", 32)
    #--------------------------------------------------------------------------
    
    colors = pt.Colors(n=1)
    datas = []
    rc = 1    
    
    for file in files:
        nb = Nbody(file, ftype=ftype)
        nb.potcenter()
        if part_type is not None:
            nb = nb.selectc(nb.tpe[nb.tpe == part_type]) #selects the type of particules
            
        nb.potcenter() #Centers at the min of the potential
        G = libgrid.Spherical_1d_Grid(rmin=5e-3, rmax=r_max, nr=n_bins, g=lambda r:f(r, rc), 
                                     gm=lambda r:fm(r, rc))
        
        #r and M
        r = G.get_r()
        # print(r)
        M = G.get_MassMap(nb) ; M = np.add.accumulate(M)
              
        x = r      
        y = V_circ_spherical_grid(nb, r, M, G)

        # output units
        out_units_x = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        out_units_y = units.UnitSystem(
            'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

        fx = nb.localsystem_of_units.convertionFactorTo(
            out_units_x.UnitLength)
        fy = nb.localsystem_of_units.convertionFactorTo(
            out_units_y.UnitVelocity)

        x = x * fx
        y = y * fy

        # set labels
        x, y = pt.CleanVectorsForLogX(x, y)
        x, y = pt.CleanVectorsForLogY(x, y)
        datas.append(pt.DataPoints(x, y, color=colors.get(), label=file, tpe='points'))
        
    #Sets labels
    xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
    ylabel = r'$V_{\rm c}\,\left[ \rm{km}/\rm{s} \right]$'
    return datas, xlabel, ylabel

def sigma_z_spherical_grid(files, **kwargs):
    #kwargs parameters---------------------------------------------------------
    ftype=kwargs.get("ftype", "swift") ; part_type=kwargs.get("part_type", None)
    r_max=kwargs.get("r_max", 50) ; n_bins=kwargs.get("n_bins", 32)
    #--------------------------------------------------------------------------
    
    colors = pt.Colors(n=1)
    datas = []
    rc = 1    
    
    for file in files:
        nb = Nbody(file, ftype=ftype)
        nb.potcenter()
        if part_type is not None:
            nb = nb.selectc(nb.tpe[nb.tpe == part_type]) #selects the type of particules
            
        nb.potcenter() #Centers at the min of the potential
        
        # output units
        out_units_x = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        out_units_y = units.UnitSystem(
            'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

        fx = nb.localsystem_of_units.convertionFactorTo(
            out_units_x.UnitLength)
        fy = nb.localsystem_of_units.convertionFactorTo(
            out_units_y.UnitVelocity)

        nb.pos = nb.pos * fx
        nb.vel = nb.vel * fy
        
        # nb.pos = nb.Pos(units="kpc")
        # nb.vel = nb.Pos(units="km/s")

        G = libgrid.Spherical_1d_Grid(rmin=1e-2, rmax=r_max, nr=n_bins, g=lambda r:f(r, rc), 
                                     gm=lambda r:fm(r, rc))
        x = G.get_r()
        y = G.get_SigmaValMap(nb, nb.Vz())   
        datas.append(pt.DataPoints(x, y, color=colors.get(), label=file, tpe='points'))
        
    #Sets labels
    xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
    ylabel = r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$'         
    return datas, xlabel, ylabel

#%%

#Enter the files to analyze
# files = [r"nfw_plummer.hdf5"]
files = [r"../ICs/nfw_plummer.hdf5", 
          r"/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S4/snap/snapshot_0051.hdf5", 
          r"/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S5/snap/snapshot_0051.hdf5",
          r"/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S1/snap/snapshot_0501.hdf5",
          r"/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S2/snap/snapshot_0051.hdf5",
          r"/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S3/snap/snapshot_0051.hdf5"] 

#Enter the labels that will appear on each plot
labels = ["ICs", r"$\varepsilon=12.5$ pc", r"$\varepsilon=20$ pc", r"$\varepsilon=25$ pc", 
          r"$\varepsilon=50$ pc", r"$\varepsilon=100$ pc"] 

#Options
ftype="swift" ; boxsize = 60 ; r_max=10 ; n_bins=512
opt_xmin = None ; opt_xmax = 10 ; opt_ymin = None ; opt_ymax = None ; opt_log = "xy"

#%%Density plots
#Density for the DM
datas, xlabel, ylabel = density_spherical_grid(files, ftype=ftype, part_type=1, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(1) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()

#Density for the stars
datas, xlabel, ylabel = density_spherical_grid(files, ftype=ftype, part_type=4, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(2) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()

#%%Circular velocity
#DM
datas, xlabel, ylabel = v_circ_spherical_grid(files, ftype=ftype, part_type=1, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(3) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()

#Stars
datas, xlabel, ylabel = v_circ_spherical_grid(files, ftype=ftype, part_type=4, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(4) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()
#%%Velocity dispersion (sigma_z)
n_bins=512
#DM
datas, xlabel, ylabel = sigma_z_spherical_grid(files, ftype=ftype, part_type=1, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(5) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()

#Stars
datas, xlabel, ylabel = sigma_z_spherical_grid(files, ftype=ftype, part_type=4, 
                                              r_max=r_max, n_bins=n_bins)
xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt_xmin, opt_xmax, opt_ymin, 
                                                    opt_ymax, datas, opt_log)
fig = plt.figure(6) ; ax = plt.subplot(); ax.cla() ;
SetAxis(ax, xmin, xmax, ymin, ymax, opt_log)
ax.set_xlabel(xlabel) ; ax.set_ylabel(ylabel)
for data, label in zip(datas, labels):
    ax.plot(data.x, data.y, label=label)
ax.legend()
