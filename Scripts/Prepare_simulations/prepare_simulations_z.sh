#! /bin/bash
#---------------------------------------------------------------------------------------------
#Some functions
#---------------------------------------------------------------------------------------------
fct_is_on_lesta() {
local is_on_lesta
if [ -d "/hpcstorage" ]; then
#     echo "We are on lesta. Using lesta folder locations"
    is_on_lesta="true"
else
#     echo "We are on Darwin's laptop. Using his folder locations"
    is_on_lesta="false"
fi
echo $is_on_lesta
}

is_on_lesta=$(fct_is_on_lesta)

if [[ $is_on_lesta == "true" ]] ; then
  source /hpcstorage/roduit/Scripts/Prepare_simulations/functions.sh #on lesta
else
  source ./functions.sh
fi

#---------------------------------------------------------------------------------------------
#Parameters allowed to be changed
#---------------------------------------------------------------------------------------------
#Enters values
# z_tab=($(seq 30 20 300))
z_tab=(30 70 130 170)

#---------------------------------------------------------------------------------------------
# This code launches Dwarf galaxies at different z position (0 0 z) in circular orbit for
# two periods of the circular orbit
#---------------------------------------------------------------------------------------------
#Iterate over all ICs we want to generate
#1) Generate ICs
#2) Create the simulation directory, then the snap directory
#3) Create the params.yml file
#4) Create the film_params.py files
#5) Create the start.slurm file
#6) Generate a file containing useful informations about the parameters

#ICs file location and launch script location
if [[ $is_on_lesta == "true" ]] ; then
    echo "We are on lesta. Using lesta folders locations"
#     IC_input_filename="/hpcstorage/roduit/ICs/nfw_plummer.hdf5"
    IC_input_filename="/hpcstorage/roduit/ICs/nfw_plummer_8_more_particles.hdf5"
    launch_orbits_location="/hpcstorage/roduit/Scripts/orbit_launch_MW.py"
else
    echo "We are on Darwin's laptop. Using his folders locations"
#     IC_input_filename="/home/darwin/Bureau/TP4b/ICs/nfw_plummer.hdf5"
    IC_input_filename="/home/darwin/Bureau/TP4b/ICs/nfw_plummer_8_more_particles.hdf5"
    launch_orbits_location="/home/darwin/Bureau/TP4b/Scripts/orbit_launch_MW.py"
fi
epsilon="0.0125" #0.025

#Directory where the snapshots are stored
snap_dir="snap" #Not supposed to change

#Starts a for loop with taking as parameters the number of ICs we have to generate
for z in ${!z_tab[@]}; do
  printf "Generating ICs for z=${z_tab[$z]}-----------------------\n"
  simulation_dir="z=${z_tab[$z]}"

  #Check if the directory exists
  if [ -d "$simulation_dir" ];
  then
    printf "$simulation_dir directory already exists. Skipped\n"
  else
    #1) Create the simulation directory, then the snap directory
    mkdir $simulation_dir
    cd $simulation_dir
    mkdir $snap_dir
    ICs_output_filename="z=${z_tab[$z]}_launched.hdf5"

    #2) Generate ICs
    python_output=$(python3 "$launch_orbits_location" "$IC_input_filename" $ICs_output_filename 0 0 ${z_tab[$z]} --position_coord="carthesian" --velocity_direction 1 0 0) #--boxsize_multiplier=3

    #Gets the last line (where the output var are printed)
    outputvar=($(printf "$python_output" | tail -n1))
    boxsize=${outputvar[0]}
    T_orbit=${outputvar[1]}
    T_simulation=$(bc -l<<<"2.0*$T_orbit")

    #Displays python output process (unless the last line which are the outputs)
    printf "$python_output\n" | head -n -1

    #3) Create the params.yml file ; generate the input file and the shift
    shift=$(bc -l<<<"$boxsize/2.0")
    generate_params $ICs_output_filename $shift $T_simulation 500 $epsilon #Only for the case with more particles (you must lower the softening length when we have more particles): 500 0.0125

    #4)Creates the film_params.py
    film_param_files=("film_param.py" "film_param_potcenter.py")
    pngs_dir=("pngs_xz" "pngs_xz_potcenter")
    mkdir {${pngs_dir[0]},${pngs_dir[1]}}
    generate_film_params $boxsize ${film_param_files[0]} "xz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[1]} "xz" "nb.potcenter() ;"

    #4) Create the start.slurm file
    job_name="dg_z=${z_tab[$z]}"
    video_name=("z=${z_tab[$z]}_xz_launched.mp4" "z=${z_tab[$z]}_xz_potcenter.mp4")
    snap_dir=("snap" "snap_corrected")
    a=$film_param_files
    generate_start_slurm_more_videos "$job_name" "params.yml" $video_name $a $pngs_dir $snap_dir

    #5) Generate a file containing useful informations about the parameters
    params_summary="params_summary.txt"
    touch $params_summary
    printf "T_simulation= $T_simulation\nboxsize= $boxsize" >> $params_summary

    #Back to the initial directory
    cd ..
  fi
  printf "\n"
done
