#------------------------------------------------------------------------------
#Put this function at the beginning of the files performing the computations :
# fct_is_on_lesta() {
# if [ -d "/hpcstorage" ]; then
#     echo "We are on lesta. Using lesta folder locations"
#     is_on_lesta=true
# else
#     echo "We are on Darwin's laptop. Using his folder locations"
#     is_on_lesta=false
# fi
# }
#------------------------------------------------------------------------------

#This function generate the parameter file for the swift simulation
generate_params() {
#Mandatory arguments
local ICs_filename=$1
local shift=$2

#Optionnal arguments
local sim_time_end=${3:-5.0}

local snapshot_number=${4:-500}
local sim_time_between_snapshot=$(bc -l <<< "$sim_time_end/$snapshot_number")
local softening_length=${5:-0.025}

#---

#Creates the parameters file
local params_file="params.yml"
touch $params_file

local content="#Define the system of units to use internally.
InternalUnitSystem:
  UnitMass_in_cgs:     1.988e+43 # 10^10 Solar masses
  UnitLength_in_cgs:   3.086e+21 # kpc
  UnitVelocity_in_cgs: 1e5       # km / s
  UnitCurrent_in_cgs:  1         # Amperes
  UnitTemp_in_cgs:     1         # Kelvin

# Parameters governing the time integration
TimeIntegration:
  time_begin:          0.      # The starting time of the simulation (in internal units).
  time_end:            $sim_time_end     # The end time of the simulation (in internal units).
  dt_min:              1e-11   # The minimal time-step size of the simulation (in internal units).
  dt_max:              1e-3    # The maximal time-step size of the simulation (in internal units).

# Parameters for the self-gravity scheme
Gravity:
  eta:                           0.005      # Constant dimensionless multiplier for time integration.
  MAC:                           adaptive   # Choice of mulitpole acceptance criterion: 'adaptive' OR 'geometric'.
  epsilon_fmm:                   0.001      # Tolerance parameter for the adaptive multipole acceptance criterion.
  theta_cr:                      0.7        # Opening angle for the purely gemoetric criterion.
  max_physical_DM_softening:     $softening_length      # Physical softening length (in internal units).
  max_physical_baryon_softening: $softening_length      # Physical softening length (in internal units).
  rebuild_frequency:             0.00001    # Tree rebuild frequency

# Parameters governing the snapshots
Snapshots:
  subdir:              snap
  basename:            snapshot  # Common part of the name of output files
  time_first:          0.        # Time of the first output (in internal units)
  delta_time:          $sim_time_between_snapshot      # Time difference between consecutive outputs (in internal units)

# Parameters governing the conserved quantities statistics
Statistics:
  delta_time:          2e-1    # Time between statistics output

# Parameters related to the initial conditions
InitialConditions:
  file_name:          $ICs_filename # The file to read
  shift:              [$shift,$shift,$shift]
  periodic:           0

Restarts:
  delta_hours:        2.5

MWPotential2014Potential:
  useabspos:       0        # 0 -> positions based on centre, 1 -> absolute positions
  position:        [0.,0.,0.]    #Centre of the potential with respect to centre of the box
  timestep_mult:   0.005     # Dimensionless pre-factor for the time-step condition
  epsilon:         0.001      # Softening size (internal units)
  #All other parameters of the potential take their default values"


#Writes in the parameters files
printf "$content" > $params_file
}

#This function generate a film parameter file to produce a pngs of the simulation with gmkgmov
generate_film_params() {

#Mandatory
local boxsize=$1

#Optionnal
local filename=${2:-"film_param.py"}
local view=${3:-"xz"} #possible values : xz, xy, yz
local command=${4:-""}
local particle_component=${5:-"#all"} #Can be #all, #stars
local width=${6:-"boxsize/2.0"}
local height=${7:-"boxsize/2.0"}

touch $filename

local content="#global film
boxsize = $boxsize

film = {}
film['ftype']  = 'swift'
film['time']   = 'nb.atime'
film['exec']   = '$command'

film['format'] = 'png' #fits
film['frames'] = []

frame = {}
frame['width']      = 1920
frame['height']     = 1920
frame['size']       = ($width,$height)
frame['tdir']       = None
frame['pfile']      = None
frame['exec']       = None
frame['macro']      = None
frame['components'] = []
frame['xp'] = None
frame['x0'] = None
frame['view'] = '$view'

component = {}
component['id'] = '$particle_component'

# append component to frame
frame['components'].append(component)

# append frame to film
film['frames'].append(frame)"

printf "$content" > $filename
}

generate_start_slurm() {
#Mandatory inputs:
local job_name=$1
local param_file=$2
local video_name=$3

#Optionnal inputs:
local film_param_file=${4:-film_param.py}
local pngs_dir=${5:-pngs_xz}
local command=${6:-""} #to fine tune the data analysis

#---
#Creates the file
local start_file="start.slurm"
touch $start_file
local start_content="#!/bin/bash

#SBATCH --job-name=$job_name
#SBATCH --ntasks=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=darwin.roduit@epfl.ch

#~ Load modules
module load GCC/11.2.0
module load OpenMPI/4.1.1
module load FFmpeg/4.3.2

#Runs the simulation
srun --ntasks=1 /hpcstorage/roduit/Simulations/swiftsim/swift --external-gravity --self-gravity --stars --threads=\${SLURM_CPUS_PER_TASK} $param_file

#Then corrects the potential of swift with pNbody potcenter (takes A LOT of time)
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/correct_snapshot_potential.py --folder=\"snap/\" --out_folder=\"snap_corrected\"

#Then produces the movies
srun --ntasks=1 gmkgmov -p $film_param_file --imdir $pngs_dir snap_corrected/snapshot_0*
srun --ntasks=1 ffmpeg -framerate 30 -pattern_type glob -i '$pngs_dir/*.png' $video_name

#Finally analyzes the data
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/data_analysis.py \"snap_corrected/\" \"data_analysis\" $command
"

#Writes the content in the file
printf "$start_content" > $start_file
}

generate_start_slurm_2_videos() {
#Mandatory inputs:
local job_name=$1
local param_file=$2
local video_name_1=$3
local video_name_2=$4

#Optionnal inputs:
local film_param_file_1=${5:-film_param_xz.py}
local pngs_dir_1=${6:-pngs_xz}
local film_param_file_2=${7:-film_param_yz.py}
local pngs_dir_2=${8:-pngs_yz}
local command=${9:-""} #to fine tune the data analysis


#---
#Creates the file
local start_file="start.slurm"
touch $start_file
local start_content="#!/bin/bash

#SBATCH --job-name=$job_name
#SBATCH --ntasks=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=darwin.roduit@epfl.ch

#~ Load modules
module load GCC/11.2.0
module load OpenMPI/4.1.1
module load FFmpeg/4.3.2

#Runs the simulation
srun --ntasks=1 /hpcstorage/roduit/Simulations/swiftsim/swift --external-gravity --self-gravity --stars --threads=\${SLURM_CPUS_PER_TASK} $param_file

#Then produces the movies
srun --ntasks=1 gmkgmov -p $film_param_file_1 --imdir $pngs_dir_1 snap/snapshot_0*
srun --ntasks=1 gmkgmov -p $film_param_file_2 --imdir $pngs_dir_2 snap/snapshot_0*
srun --ntasks=1 ffmpeg -framerate 30 -pattern_type glob -i '$pngs_dir_1/*.png' $video_name_1
srun --ntasks=1 ffmpeg -framerate 30 -pattern_type glob -i '$pngs_dir_2/*.png' $video_name_2

#Then corrects the potential of swift with pNbody potcenter (takes A LOT of time)
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/correct_snapshot_potential.py --folder=\"snap/\" --out_folder=\"snap_corrected\"


#Finally analyzes the data
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/data_analysis.py \"snap_corrected/\" \"data_analysis\" $command
"

#Writes the content in the file
printf "$start_content" > $start_file
}

generate_start_slurm_more_videos() {
#Mandatory inputs:
local job_name=$1
local param_file=$2
video_name=$3 #array of the video names
film_param_files=$4 #array of the film_param.py names
pngs_dir=$5 #names of the directories to store the pngs
snap_dir=$6

#Optionnal input
local command_correct_snapshots=${7:-""} #to fine tune the snapshot_correction
local command_analyze_data=${8:-""} #to fine tune the data analysis

#Produces the movie part
local movie_text=""
for i in ${!film_param_files[@]}; do
    movie_text+="srun --ntasks=1 gmkgmov -p ${film_param_files[$i]} --imdir ${pngs_dir[$i]} ${snap_dir[$i]}/snapshot_0* \n"
    movie_text+="srun --ntasks=1 ffmpeg -framerate 30 -pattern_type glob -i '${pngs_dir[$i]}/*.png' ${video_name[$i]}\n\n"
done

#---
#Creates the file
local start_file="start.slurm"
touch $start_file
local start_content="#!/bin/bash

#SBATCH --job-name=$job_name
#SBATCH --ntasks=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=darwin.roduit@epfl.ch

#~ Load modules
module load GCC/11.2.0
module load OpenMPI/4.1.1
module load FFmpeg/4.3.2

#Runs the simulation
srun --ntasks=1 /hpcstorage/roduit/Simulations/swiftsim/swift --external-gravity --self-gravity --stars --threads=\${SLURM_CPUS_PER_TASK} $param_file

#Then corrects the potential of swift with pNbody potcenter (takes A LOT of time)
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/correct_snapshot_potential.py --folder=\"snap/\" --out_folder=\"snap_corrected\" $command_correct_snapshots

#Then produces the movies
$movie_text
#Finally analyzes the data
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/data_analysis.py \"snap_corrected/\" \"data_analysis\" $command_analyze_data
"

#Writes the content in the file
printf "$start_content" > $start_file
# printf "$start_content"
}

generate_start_slurm_more_videos_isolated_case() {
#Mandatory inputs:
local job_name=$1
local param_file=$2
video_name=$3 #array of the video names
film_param_files=$4 #array of the film_param.py names
pngs_dir=$5 #names of the directories to store the pngs
snap_dir=$6

#Optionnal input
local command_correct_snapshots=${7:-""} #to fine tune the snapshot_correction
local command_analyze_data=${8:-""} #to fine tune the data analysis

#Produces the movie part
local movie_text=""
for i in ${!film_param_files[@]}; do
    movie_text+="srun --ntasks=1 gmkgmov -p ${film_param_files[$i]} --imdir ${pngs_dir[$i]} ${snap_dir[$i]}/snapshot_0* \n"
    movie_text+="srun --ntasks=1 ffmpeg -framerate 30 -pattern_type glob -i '${pngs_dir[$i]}/*.png' ${video_name[$i]}\n\n"
done

#---
#Creates the file
local start_file="start.slurm"
touch $start_file
local start_content="#!/bin/bash

#SBATCH --job-name=$job_name
#SBATCH --ntasks=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=darwin.roduit@epfl.ch

#~ Load modules
module load GCC/11.2.0
module load OpenMPI/4.1.1
module load FFmpeg/4.3.2

#Runs the simulation #Notice : without --external_gravity !
srun --ntasks=1 /hpcstorage/roduit/Simulations/swiftsim/swift --self-gravity --stars --threads=\${SLURM_CPUS_PER_TASK} $param_file

#Then corrects the potential of swift with pNbody potcenter (takes A LOT of time)
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/correct_snapshot_potential.py --folder=\"snap/\" --out_folder=\"snap_corrected\" $command_correct_snapshots

#Then produces the movies
$movie_text
#Finally analyzes the data
srun --ntasks=1 python3 /hpcstorage/roduit/Scripts/data_analysis.py \"snap_corrected/\" \"data_analysis\" $command_analyze_data
"

#Writes the content in the file
printf "$start_content" > $start_file
# printf "$start_content"
}
