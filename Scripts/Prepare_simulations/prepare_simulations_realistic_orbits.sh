#! /bin/bash
#---------------------------------------------------------------------------------------------
#Some functions
#---------------------------------------------------------------------------------------------
fct_is_on_lesta() {
local is_on_lesta
if [ -d "/hpcstorage" ]; then
#     echo "We are on lesta. Using lesta folder locations"
    is_on_lesta="true"
else
#     echo "We are on Darwin's laptop. Using his folder locations"
    is_on_lesta="false"
fi
echo $is_on_lesta
}

is_on_lesta=$(fct_is_on_lesta)

if [[ $is_on_lesta == "true" ]] ; then
  source /hpcstorage/roduit/Scripts/Prepare_simulations/functions.sh #on lesta
else
  source ./functions.sh
fi

#---------------------------------------------------------------------------------------------
#Parameters allowed to be changed
#---------------------------------------------------------------------------------------------
#Enter values

#Values for Bootes 1, Hercules and Tucana 2
# x_tab=("23.62030812" "-33.37094458" "-5.32264025")
# y_tab=("-29.97283635" "-75.646949" "-174.68570212")
# z_tab=("-100.08975643" "-467.38534651" "-85.94990389")
# v_x_tab=("-80.76541443" "-34.08905498" "-23.44726264")
# v_y_tab=("47.80877663" "-17.27932774" "-14.69303303")
# v_z_tab=("-33.37094458" "8.31711564" "31.19015143")

#Values for Bootes 3 and Tucana 3
x_tab=("5.30719459"   "9.77424759")
y_tab=("-22.30953578" "-33.00174105")
z_tab=("142.38559628"  "33.05584158")
v_x_tab=("-0.22518842"  "15.44508026")
v_y_tab=("-18.97515499" "-15.95203443")
v_z_tab=("-2.929447"   "-20.57211347")

#Choose how to name the simulations. These will be the name of the folder, the movies and the job with sbatch system
# name=("Bootes_1" "Hercules" "Tucana_2")
name=("Bootes_3" "Tucana_3")

#Choose the file containing the Initial conditions. Make sure it exists in the right folder, either on lesta or on my computer
init_cond_file="nfw_0025_0_8+plummer.hdf5"


#softening length ot use
# epsilon="0.025"
epsilon="0.0125"
#NB: You must lower the softening length when we have more particles

#Number of snapshots
n_snapshots="500"

#Duration of the simulations (in Gyr)
T="6.5"

#---------------------------------------------------------------------------------------------
# This code launches Dwarf galaxies at different positions (x y z) with velocities (v_x v_y v_z)
#---------------------------------------------------------------------------------------------
#Iterate over all ICs we want to generate
#1) Generate ICs
#2) Create the simulation directory, then the snap directory
#3) Create the params.yml file
#4) Create the film_params.py files
#5) Create the start.slurm file
#6) Generate a file containing useful informations about the parameters

#ICs file location and launch script location
if [[ $is_on_lesta == "true" ]] ; then
    echo "We are on lesta. Using lesta folders locations"
    IC_input_filename="/hpcstorage/roduit/ICs/$init_cond_file"
    launch_orbits_location="/hpcstorage/roduit/Scripts/orbit_launch_MW.py"
else
    echo "We are on Darwin's laptop. Using his folders locations"
    IC_input_filename="/home/darwin/Bureau/TP4b/ICs/$init_cond_file"
    launch_orbits_location="/home/darwin/Bureau/TP4b/Scripts/orbit_launch_MW.py"
fi

#Directory where the snapshots are stored
snap_dir="snap" #Not supposed to change

#Starts a for loop with taking as parameters the number of ICs we have to generate
for i in ${!z_tab[@]}; do
  x=${x_tab[$i]}
  y=${y_tab[$i]}
  z=${z_tab[$i]}

  printf "Generating ICs for ${name[$i]}-----------------------\n"
  simulation_dir="${name[$i]}"

  #Check if the directory exists
  if [ -d "$simulation_dir" ];
  then
    printf "$simulation_dir directory already exists. Skipped\n"
  else
    #1) Create the simulation directory, then the snap directory
    mkdir $simulation_dir
    cd $simulation_dir
    mkdir $snap_dir
    ICs_output_filename="${name[$i]}_launched.hdf5"

    #2) Generate ICs
    python_output=$(python3 $launch_orbits_location $IC_input_filename $ICs_output_filename $x $y $z --position_coord="carthesian" --vel_orbit ${v_x_tab[$i]} ${v_y_tab[$i]} ${v_z_tab[$i]} --velocity_coord="carthesian" --T_simulation=$T)

    #Gets the last line (where the output var are printed)
    outputvar=($(printf "$python_output" | tail -n1))
    boxsize=${outputvar[0]}
    T_orbit=${outputvar[1]}
    T_simulation=$(bc -l<<<"$T_orbit")

    #Displays python output process (unless the last line which are the outputs)
    printf "$python_output\n" | head -n -1

    #3) Create the params.yml file ; generate the input file and the shift
    shift=$(bc -l<<<"$boxsize/2.0")
    generate_params $ICs_output_filename $shift $T_simulation $n_snapshots $epsilon

    #4)Creates the film_params.py
    film_param_files=("film_param_xz.py" "film_param_yz.py" "film_param_xy.py" "film_param_xy_potcenter.py" "film_param_xy_potcenter_stars.py")
    pngs_dir=("pngs_xz" "pngs_yz" "pngs_xy" "pngs_potcenter_xy" "pngs_potcenter_stars")
    mkdir {${pngs_dir[0]},${pngs_dir[1]},${pngs_dir[2]},${pngs_dir[3]},${pngs_dir[4]}}
    width_for_stars="2.0"
    height_for_stars="2.0"

    generate_film_params $boxsize ${film_param_files[0]} "xz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[1]} "yz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[2]} "xy" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[3]} "xy" ""
    generate_film_params $boxsize ${film_param_files[4]} "xy" "" "stars" $width_for_stars $height_for_stars

    #4) Create the start.slurm file
    job_name="${name[$i]}"
    video_name=("${name[$i]}_xz.mp4" "${name[$i]}_yz.mp4" "${name[$i]}_xy.mp4" "${name[$i]}_xy_potcenter.mp4" "${name[$i]}_potcenter_stars.mp4")
    snap_dir=("snap" "snap" "snap" "snap_corrected" "snap_corrected")
    generate_start_slurm_more_videos "$job_name" "params.yml" $video_name $film_param_files $pngs_dir $snap_dir "--method=histocenter"

    #5) Generate a file containing useful informations about the parameters
    params_summary="params_summary.txt"
    touch $params_summary
    printf "T_simulation= $T_simulation\nboxsize= $boxsize" >> $params_summary

    #Back to the initial directory
    cd ..
  fi
  printf "\n"
done
