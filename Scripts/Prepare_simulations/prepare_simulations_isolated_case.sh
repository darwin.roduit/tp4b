#! /bin/bash
#---------------------------------------------------------------------------------------------
#Some functions
#---------------------------------------------------------------------------------------------
fct_is_on_lesta() {
local is_on_lesta
if [ -d "/hpcstorage" ]; then
#     echo "We are on lesta. Using lesta folder locations"
    is_on_lesta="true"
else
#     echo "We are on Darwin's laptop. Using his folder locations"
    is_on_lesta="false"
fi
echo $is_on_lesta
}

is_on_lesta=$(fct_is_on_lesta)

if [[ $is_on_lesta == "true" ]] ; then
  source /hpcstorage/roduit/Scripts/Prepare_simulations/functions.sh #on lesta
else
  source ./functions.sh
fi

#---------------------------------------------------------------------------------------------
#Parameters allowed to be changed
#Enters values

# init_cond_file=("nfw_plummer.hdf5" "nfw_plummer_8_more_particles.hdf5")
init_cond_file=("nfw_0025_0_8+plummer.hdf5" "2s0_3_003_035+plummer.hdf5" "2s0_3_0008_05+plummer.hdf5")
softening_length=("0.0125" "0.0125" "0.0125")

#---------------------------------------------------------------------------------------------
# This code launches in the isolated cases
#---------------------------------------------------------------------------------------------
#Iterate over all ICs we want to generate
#1) Generate ICs
#2) Create the simulation directory, then the snap directory
#3) Create the params.yml file
#4) Create the film_params.py files
#5) Create the start.slurm file
#6) Generate a file containing useful informations about the parameters

#ICs file location and launch script location
if [[ $is_on_lesta == "true" ]] ; then
    echo "We are on lesta. Using lesta folders locations"
    IC_input_filename="/hpcstorage/roduit/ICs/"
    launch_orbits_location="/hpcstorage/roduit/Scripts/orbit_launch_MW.py"
else
    echo "We are on Darwin's laptop. Using his folders locations"
    IC_input_filename="/home/darwin/Bureau/TP4b/ICs/"
    launch_orbits_location="/home/darwin/Bureau/TP4b/Scripts/orbit_launch_MW.py"
fi

#Directory where the snapshots are stored
snap_dir="snap" #Not supposed to change

#Starts a for loop with taking as parameters the number of ICs we have to generate
snap_dir="snap" #Does not change
for i in ${!init_cond_file[@]}; do
  name_IC_file=$(basename "${init_cond_file[$i]}" .hdf5)
#   name_IC_file=${init_cond_file[$i]}
  printf "Generating ICs for the IC=$name_IC_file-----------------------\n"
  simulation_dir="IC=$name_IC_file"

  #Check if the directory exists
  if [ -d "$simulation_dir" ]; #Mettre ça avant la génération du fichier python
  then
    printf "$simulation_dir directory already exists. Skipped\n"
  else
    #1) Create the simulation directory, then the snap directory
    mkdir $simulation_dir
    cd $simulation_dir
    mkdir $snap_dir

    #Computes the boxsize
    ICs_output_filename="IC=$name_IC_file""_launched.hdf5"
    #2) Generate ICs

    python_output=$(python3 "$launch_orbits_location" "$IC_input_filename${init_cond_file[$i]}" $ICs_output_filename 0 0 0 --position_coord="carthesian" --vel_orbit 0 0 0 --boxsize_multiplier=4 --T_simulation=5.0)

    #Gets the last line (where the output var are printed)
    outputvar=($(printf "$python_output" | tail -n1))
    boxsize=${outputvar[0]}
    T_orbit=${outputvar[1]}
    T_simulation=$(bc -l<<<"1.0*$T_orbit")

    #Displays python output process (unless the last line which are the outputs)
    printf "$python_output\n" | head -n -1

    #3) Create the params.yml file ; generate the input file and the shift
    shift=$(bc -l<<<"$boxsize/2.0")
    generate_params $ICs_output_filename $shift $T_simulation 500 ${softening_length[$i]}

    #4)Creates the film_params.py
    film_param_files=("film_param_xz.py" "film_param_yz.py" "film_param_xy.py" "film_param_xy_potcenter.py" "film_param_xy_potcenter_stars.py")
    pngs_dir=("pngs_xz" "pngs_yz" "pngs_xy" "pngs_potcenter_xy" "pngs_potcenter_stars")
    mkdir {${pngs_dir[0]},${pngs_dir[1]},${pngs_dir[2]},${pngs_dir[3]},${pngs_dir[4]}}
    width_for_stars="2.0"
    height_for_stars="2.0"

    generate_film_params $boxsize ${film_param_files[0]} "xz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[1]} "yz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[2]} "xy" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
    generate_film_params $boxsize ${film_param_files[3]} "xy" ""
    generate_film_params $boxsize ${film_param_files[4]} "xy" "" "stars" $width_for_stars $height_for_stars

    #4) Create the start.slurm file
    job_name="$name_IC_file"
    video_name=("$name_IC_file""_xz.mp4" "$name_IC_file""_yz.mp4" "$name_IC_file""_xy.mp4" "$name_IC_file""_xy_potcenter.mp4" "$name_IC_file""_potcenter_stars.mp4")
    snap_dir=("snap" "snap" "snap" "snap_corrected" "snap_corrected")
    generate_start_slurm_more_videos_isolated_case "$job_name" "params.yml" $video_name $film_param_files $pngs_dir $snap_dir "--method=histocenter"

    #5) Generate a file containing usefule informations about the parameters
    params_summary="params_summary.txt"
    touch $params_summary
    printf "T_simulation= $T_simulation\nboxsize= $boxsize" >> $params_summary

    #Back to the initial directory
    cd ..
  fi
  printf "\n"
done
