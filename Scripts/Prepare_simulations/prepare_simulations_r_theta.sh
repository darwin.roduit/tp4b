#! /bin/bash
#---------------------------------------------------------------------------------------------
#Some functions
#---------------------------------------------------------------------------------------------
fct_is_on_lesta() {
local is_on_lesta
if [ -d "/hpcstorage" ]; then
#     echo "We are on lesta. Using lesta folder locations"
    is_on_lesta="true"
else
#     echo "We are on Darwin's laptop. Using his folder locations"
    is_on_lesta="false"
fi
echo $is_on_lesta
}

is_on_lesta=$(fct_is_on_lesta)

if [[ $is_on_lesta == "true" ]] ; then
  source /hpcstorage/roduit/Scripts/Prepare_simulations/functions.sh #on lesta
else
  source ./functions.sh
fi

#---------------------------------------------------------------------------------------------
#Parameters allowed to be changed
#---------------------------------------------------------------------------------------------
#Enters values
pi=$(echo "scale=10; 4*a(1)" | bc -l)
r_tab=(70)
angle_tab=($(bc -l <<< "$pi/4.0") $(bc -l <<< "$pi/2.0"))

#---------------------------------------------------------------------------------------------
# This code launches DG at different (r, theta) position
#---------------------------------------------------------------------------------------------
#Iterate over all ICs we want to generate
#1) Generate ICs
#2) Create the simulation directory, then the snap directory
#3) Create the params.yml file
#4) Create the film_params.py
#5) Create the start.slurm file
#6) Generate a file containing useful informations about the parameters

#ICs file location and launch script location
if [[ $is_on_lesta == "true" ]] ; then
    echo "We are on lesta. Using lesta folders locations"
#     IC_input_filename="/hpcstorage/roduit/ICs/nfw_plummer.hdf5"
    IC_input_filename="/hpcstorage/roduit/ICs/nfw_plummer_8_more_particles.hdf5"
    launch_orbits_location="/hpcstorage/roduit/Scripts/orbit_launch_MW.py"
else
    echo "We are on Darwin's laptop. Using his folders locations"
#     IC_input_filename="/home/darwin/Bureau/TP4b/ICs/nfw_plummer.hdf5"
    IC_input_filename="/home/darwin/Bureau/TP4b/ICs/nfw_plummer_8_more_particles.hdf5"
    launch_orbits_location="/home/darwin/Bureau/TP4b/Scripts/orbit_launch_MW.py"
fi

#Directory where the snapshots are stored
snap_dir="snap" #Does not change

#Starts a for loop with taking as parameters the number of ICs we have to generate
for r in ${!r_tab[@]}; do
  printf "Generating ICs for r=${r_tab[$r]}-----------------------\n"
  for theta in ${!angle_tab[@]}; do
    theta_rounded=`printf "%.2f" ${angle_tab[$theta]}`
    printf "    Generating ICs for theta=$theta_rounded-----------------------\n"
    simulation_dir="r=${r_tab[$r]}_theta=$theta_rounded"

    #Check if the directory exists
    if [ -d "$simulation_dir" ];
    then
      printf "$simulation_dir directory already exists. Skipped\n"
    else
      #1) Create the simulation directory, then the snap directory
      mkdir $simulation_dir
      cd $simulation_dir
      mkdir $snap_dir
      ICs_output_filename="r=${r_tab[$r]}_theta=${theta_rounded}_launched.hdf5"

      #2) Generate ICs
      phi=$(bc -l <<< "$pi/2.0")

      python_output=$(python3 "$launch_orbits_location" "$IC_input_filename" $ICs_output_filename ${r_tab[$r]} $phi ${angle_tab[$theta]} --position_coord="spherical" --velocity_direction 1 0 0)

      #Gets the last line (where the output var are printed)
      outputvar=($(printf "$python_output" | tail -n1))
      boxsize=${outputvar[0]}
      T_orbit=${outputvar[1]}
      T_simulation=$(bc -l<<<"2.0*$T_orbit")
      printf "Simulation time (2*T_orbit): $T_simulation "

      #Displays python output process (unless the last line which are the outputs)
      printf "$python_output\n" | head -n -1

      #3) Create the params.yml file ; generate the input file and the shift
      shift=$(bc -l<<<"$boxsize/2.0")
      generate_params $ICs_output_filename $shift $T_simulation

      #4) Creates the film_params.py
      film_param_files=("film_param_xz.py" "film_param_yz.py" "film_param_xy.py" "film_param_xy_center.py")
      pngs_dir=("pngs_xz" "pngs_yz" "pngs_xy" "pngs_center_xy")
      mkdir {${pngs_dir[0]},${pngs_dir[1]},${pngs_dir[2]},${pngs_dir[3]}}

      generate_film_params $boxsize ${film_param_files[0]} "xz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
      generate_film_params $boxsize ${film_param_files[1]} "yz" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
      generate_film_params $boxsize ${film_param_files[2]} "xy" "nb.translate([-nb.boxsize[0]/2.,-nb.boxsize[1]/2.,-nb.boxsize[2]/2.]) ;"
      generate_film_params $boxsize ${film_param_files[3]} "xy" ""

      #5) Create the start.slurm file
      job_name="dg_r_theta=${r_tab[$r]}_${theta_rounded}"
      video_name=("r=${r_tab[$r]}_theta=${theta_rounded}_xz_launched.mp4" "r=${r_tab[$r]}_theta=${theta_rounded}_yz_launched.mp4" "r=${r_tab[$r]}_theta=${theta_rounded}_xy_launched.mp4" "r=${r_tab[$r]}_theta=${theta_rounded}_xy_center_launched.mp4")
      snap_dir=("snap" "snap" "snap" "snap_corrected")
      generate_start_slurm_more_videos $job_name "params.yml" $video_name $film_param_files $pngs_dir $snap_dir "--method='histocenter'"

      #6) Generate a file containing useful informations about the parameters
      params_summary="params_summary.txt"
      touch $params_summary
      printf "T_simulation= $T_simulation\nboxsize= $boxsize\nr= ${r_tab[$r]}\ntheta= ${angle_tab[$theta]}" >> $params_summary

      #Back to the initial directory
      cd ..
    fi
    printf "\n"
  done
  printf "\n"
done
