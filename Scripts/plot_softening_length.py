#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u

from pNbody import *

# plt.style.use("seaborn-v0_8")
plt.style.use(r"/home/darwin/Bureau/TP4b/Scripts/lab_style_figure.mplstyle")


#%% Functions

def plummer_acc(nb, M, G, a):
    r = nb.rxyz(center=np.array([0, 0, 0]))
    return G*M*r / (r**2 + a**2)**(3/2)

def NFW_acc(nb, G, rho_0, r_c):
    r = nb.rxyz(center=np.array([0, 0, 0]))
    return 4*np.pi*G*rho_0*r_c**2* np.abs( ( 1/(r*(1+r/r_c)) - r_c/r**2 * np.log(1 + r/r_c) ) )

#%% Creates Nbody objects

boxsize = np.array([60, 60, 60])
center = np.array([0,0,0])
files = ["/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S4/snap/snapshot_0051.hdf5", 
          "/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S5/snap/snapshot_0051.hdf5",
          "/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S1/snap/snapshot_0501.hdf5",
          "/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S2/snap/snapshot_0051.hdf5",
          "/home/darwin/Bureau/TP4b/Simulations/Smooting_length_determination/S3/snap/snapshot_0051.hdf5"] 
softening_length = np.array([0.0125, 0.02, 0.025, 0.050, 0.1])
nb_array = np.zeros(softening_length.shape, dtype=Nbody)

for index, file in enumerate(files):
    nb_array[index] = Nbody(file, ftype="swift") 
    nb_array[index].translate(-boxsize/2)

# acc_0 = np.zeros((nb_array.shape[0], nb_array[0].pos.shape[0]), dtype = object)
# acc = np.zeros((nb_array.shape[0], nb_array[0].pos.shape[0]))

acc_0 = np.empty(softening_length.shape[0], dtype = object)
acc = np.empty(softening_length.shape[0], dtype = object)

#%%Computes the analytical acceleration for each particles in each simulation
G = 1 ; 
rho_0 = 0.007 ; r_c = 0.5 ; a = 0.1 ; M = 1e-5 #Valeurs de la génération des ICs
for index, nb in enumerate(nb_array):
    acc_0[index] = plummer_acc(nb, M, G, a) + NFW_acc(nb, G, rho_0, r_c)
    
#%% Computes the numerical acceleration
print("Computing the numerical acceleration...")
for index, nb in enumerate(nb_array):
    print("Start {}".format(index+1))
    acc[index] = np.linalg.norm(nb_array[index].TreeAccel(nb_array[index].pos, softening_length[index]), axis=1)
print("end")

#%%Make the plots
fig = plt.figure(1)
ax = plt.subplot() ; ax.cla()
alpha_tab=[1, 0.9, 0.5, 0.5, 1, 1]
marker_tab=["x", ".", "x", ".", "x", "." ]

for a_0, a, soft_length, alpha, marker in zip(acc_0, acc, softening_length, alpha_tab, marker_tab):
    ax.plot(a_0, a, linestyle="none", markevery=10, marker=marker, label="$\epsilon$ = {} kpc".format(soft_length),
            alpha=alpha)
    
# ax.plot(acc_0[0], acc_0[0], linestyle="none", marker=".", alpha=0.5, label="y=x")
ax.legend() ; ax.axis("equal")
ax.set_xlabel(r"$a_{\mathrm{th}} \, [\mathrm{kpc}/ (0.98 \, \mathrm{Gyr})^2]$")
ax.set_ylabel(r"$a_{\mathrm{num}} \, [\mathrm{kpc}/ (0.98 \, \mathrm{Gyr})^2]$")
fig.set_size_inches(5.5, 4)


output_location="../Report/figures/"
plt.tight_layout()
plt.savefig(output_location+"a_analytical_vs_a_numerical.pdf", format="pdf", bbox_inches='tight')
plt.close()
