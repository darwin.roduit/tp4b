#!/usr/bin/env python3
import argparse
import numpy as np

class store_as_list(argparse._StoreAction):
    def __call__(self, parser, namespace, values, option_string=None):
        values = list(values)
        return super().__call__(parser, namespace, values, option_string)

class store_as_array(argparse._StoreAction):
    def __call__(self, parser, namespace, values, option_string=None):
        values = np.array(values)
        return super().__call__(parser, namespace, values, option_string)

class ExplicitDefaultsHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    """Prints the default arguments when they are provided."""
    def _get_help_string(self, action):
        if action.default in (None, False):
            return action.help
        return super()._get_help_string(action)
    
class RawTextArgumentDefaultsHelpFormatter(ExplicitDefaultsHelpFormatter,
                                                                                       argparse.RawTextHelpFormatter):
    """Provides the defaults values and allows to format the text help"""
    pass