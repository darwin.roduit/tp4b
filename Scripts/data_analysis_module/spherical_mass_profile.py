 #!/usr/bin/env python3
 # -*- coding: utf-8 -*-
"""Documentation"""

import sys
import os
import numpy as np
import argparse

#Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from matplotlib import pyplot as plt
from matplotlib import cm 
from tqdm import tqdm

import Ptools as pt
from pNbody import Nbody
from pNbody import units
from pNbody import libgrid

from argparse_utils import RawTextArgumentDefaultsHelpFormatter
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, choose_n_filenames, get_snapshot_time

#%%
def f(r, rc): return np.log(r / rc + 1.)
def fm(r, rc): return rc * (np.exp(r) - 1.)

def integrated_mass_spherical_grid(nb, spherical_grid=None, **kwargs):
    #spherical_grid : libgrid.Spherical_1d_Grid object ; allows to define once 
    #the grid for multiple objects to speed up the computations
    
    #kwargs parameters----------------------------------------------------------------
    part_type=kwargs.get("part_type", None)
    r_max=kwargs.get("r_max", 50) ; n_bins=kwargs.get("n_bins", 32)
    do_potcenter= kwargs.get("do_potcenter", False)
    #--------------------------------------------------------------------------

    rc = 1 #parameter for the grid
        
    #Selects the type of particule
    nb2 = nb
    if part_type is not None:
        nb2 = nb.select(part_type) 
        
    if do_potcenter:
        nb2.potcenter()
        
    if spherical_grid is None :
        G = libgrid.Spherical_1d_Grid(
            rmin=0, rmax=r_max, nr=n_bins, g=lambda r:f(r, rc) , gm=lambda r:fm(r, rc))
    else:
        G = spherical_grid

    x = G.get_r()
    y = G.get_MassMap(nb2)
    y = np.add.accumulate(y)

    # output units
    out_units_x = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    out_units_y = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    
    fx = nb2.localsystem_of_units.convertionFactorTo(out_units_x.UnitLength)
    fy = nb2.localsystem_of_units.convertionFactorTo(out_units_y.UnitDensity)

    x = x * fx
    y = y * fy
    
    #Clean zero values
    x, y = pt.CleanVectorsForLogX(x, y)
    x, y = pt.CleanVectorsForLogY(x, y)
    
    return x, y

def spherical_mass_profile(files, n_profile, ftype, n_bins, do_potcenter, no_DM_half_light_radius_computations):    
    r_array_stars = np.zeros(n_profile+1, dtype=object) 
    r_array_DM = np.zeros(n_profile+1, dtype=object) 
    mass_array_stars = np.zeros(n_profile+1, dtype=object)
    mass_array_DM = np.zeros(n_profile+1, dtype=object)
    time_array_n_profile = np.zeros(n_profile+1)
    
    #Now, we will look at the mass profile at n_profile+1 different snapshots
    #NB: We also look at the profile for the snapshot 0000.
    #The maximal radius is adapted to the maximal position of the objects
    print("Computing the mass profiles for the stars and the dark matter...")
    for i,file in enumerate(tqdm(files)):
        nb = Nbody(file, ftype=ftype)
        
        #Max radius for the stars
        nb2 = nb.select("stars")
        r_max_data = np.max(nb2.rxyz())
        r_array_stars[i], mass_array_stars[i] = integrated_mass_spherical_grid(nb2, r_max=r_max_data, n_bins=n_bins,
                                                                               part_type=4, do_potcenter=do_potcenter) #Centers on the mass of the stars, useful if they decouple from DM

        #Max radius for the DM
        if not no_DM_half_light_radius_computations:
            nb2 = nb.select("halo")
            r_max_data = np.max(nb2.rxyz())
            r_array_DM[i], mass_array_DM[i] = integrated_mass_spherical_grid(nb2, r_max=r_max_data, n_bins=n_bins,
                                                                         part_type=1, do_potcenter=do_potcenter)

        time_array_n_profile[i] = get_snapshot_time(file)
    print("End of the mass profiles.")    
    return r_array_stars, r_array_DM, mass_array_stars, mass_array_DM, time_array_n_profile

def makePlot(output_location, r_array_stars, r_array_DM, mass_array_stars, mass_array_DM, time_array_n_profile, colours):    
    fig, ax = plt.subplots(nrows=1, ncols=2, num=1, figsize=(12, 4.5)) 
    ax[0].clear() ; ax[1].clear()
    
    #Plot for DM
    ax[0].title.set_text("Dark matter")
    for r,m,t,col in zip(r_array_DM, mass_array_DM, time_array_n_profile, colours):
        label = r"$t={:.2f}$ Gyr".format(t)
        ax[0].plot(r, m, color=col)
    ax[0].set_xscale("log") ; ax[0].set_yscale("log")
    ax[0].set_xlabel(r'$\rm{Radius}\,\left[ \rm{kpc} \right]$') 
    ax[0].set_ylabel(r'$\rm{Mass}\,\left[ M_{\odot} \right]$')
    
    #Plot for the stars
    ax[1].title.set_text("Stars")
    for r,m,t,col in zip(r_array_stars, mass_array_stars, time_array_n_profile, colours):
        label = r"$t={:.2f}$ Gyr".format(t)
        ax[1].plot(r, m, label=label, color=col)
    ax[1].set_xscale("log") ; ax[1].set_yscale("log")
    ax[1].set_xlabel(r'$\rm{Radius}\,\left[ \rm{kpc} \right]$') 
    ax[1].legend()
    
    plt.tight_layout()
    plt.savefig(output_location+"Mass_profile.png", format="png")
    plt.close()

#%%
def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter, 
                                     description="Analyses the spherical mass profiles of snapshots produced "
                                     "by simulating tidal striping of ultra faint dwarf galaxies.")
    
    parser.add_argument('input_location', help='Folder containing all the snapshot of one simulation.')
    parser.add_argument('output_location', help='Output folder to store the results.')
    
    parser.add_argument('--ftype', default="swift", type=str, 
                        help='Type of the simulation files, e.g. swift, gadget, etc.')
    
    parser.add_argument('--snap_basename', default="snapshot", type=str, 
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")
    
    parser.add_argument('--n_bins', default=64, type=int, 
                        help="Number of bins of the grid.")
    
    parser.add_argument('--n_profile', default=10, type=int, 
                        help="Number of snapshots to use.")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Centers the snapshot on the minimum of the potential ; False : Does nothing.")

    parser.add_argument('--no_DM_half_light_radius_computations', default=False, action="store_true",
                            help="False : Perform the computations of the DM half-light radius. This takes a lot of time."
                            "True : Do not do it ; it's quicker.")
        
    args = parser.parse_args()
    return args 

#%%
if __name__ == "__main__":
    args = parse_options()

    #Vérifier que l'on pourra utiliser les éléments du script sans argparse
    input_location = args.input_location #"/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/z_30_test"
    output_location = args.output_location #"/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/data_analysis"
    snap_basename = args.snap_basename
    n_profile = args.n_profile
    ftype = args.ftype
    n_bins = args.n_bins
    do_potcenter = args.do_potcenter
    no_DM_half_light_radius_computations = args.no_DM_half_light_radius_computations


    #----Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)

    #Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location=output_location+"/"

    colours = cm.rainbow(np.linspace(0, 1, n_profile+1)) #choses n colors in the colormap

    #---Performs the job
    n_snapshot = get_number_snapshot_directory(input_location, snap_basename) #number of snapshots in the snap folder
    files = choose_n_filenames(n_snapshot, n_profile, snap_basename, input_location)
    r_array_stars, r_array_DM, mass_array_stars, mass_array_DM, time_array_n_profile = spherical_mass_profile(files, n_profile, ftype, n_bins, do_potcenter,
                                                                                                              no_DM_half_light_radius_computations)

    #---Saves the data
    np.savez_compressed(output_location+"mass_profile_data", r_stars=r_array_stars, r_DM=r_array_DM, 
                        mass_stars=mass_array_stars, mass_DM=mass_array_DM, t=time_array_n_profile)
    
    #---Makes the plot
    makePlot(output_location, r_array_stars, r_array_DM, mass_array_stars, mass_array_DM, time_array_n_profile, colours)
