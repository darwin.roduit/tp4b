#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Documentation"""

from data_analysis_module.velocity_dispersion import velocity_dispersion_cylindrical_grid
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, get_all_snapshot_in_directory, get_snapshot_time
from data_analysis_module.half_mass_radius import compute_R_e, half_mass_radius
from argparse_utils import RawTextArgumentDefaultsHelpFormatter
from pNbody import Nbody
from tqdm import tqdm
from matplotlib import pyplot as plt
from astropy.constants import G as G_a
from astropy import units as u
import sys
import os
import argparse
import numpy as np

# Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))


# %%


def mass_half_light_wolf_relation(sigma_los, R_e, G):
    """Computes the half-light mass by means of Wolf relation.


    Parameters
    ----------
    sigma_los : float
        Average of the line of sight velocity dispersion. The average is a 
        luminosity-weighted average one.
    R_e : float
        2D projected half-light radius.
    G : float
        Newton gravitation constant.

    Returns
    -------
    M_half_light : float
        Half-light radius.

    """
    M_half_light = 4/G * sigma_los**2 * \
        R_e  # This is M(r=r_1/2), with r_1/2 the half light radius
    return M_half_light


def uncertainty_mass_half_light_wolf_relation(sigma_los, D_sigma_los, R_e, D_R_e, G):
    return 4/G * (sigma_los**2 * D_R_e + 2*sigma_los*D_sigma_los*R_e)


def wolf_relation(files, n_snapshot, ftype, n_stars_per_bin_1, n_stars_per_bin_2, n_los, mass_half_stars, do_potcenter):
    """Note: Only relevant for the stars, so no need of DM. """
    R_e_array_stars = np.zeros(n_snapshot)
    D_R_e_array_stars = np.zeros(n_snapshot)
    sigma_array_stars = np.zeros(n_snapshot)
    D_sigma_array_stars = np.zeros(n_snapshot)
    mass_dyn = np.zeros(n_snapshot)
    time_array = np.zeros(n_snapshot)

    time_array = np.zeros(n_snapshot)

    print("Computation of Wolf relation.")
    for i, file in enumerate(tqdm(files)):
        nb = Nbody(file, ftype=ftype)

        # Determines R_e
        nb2 = nb.select("stars")
        R_e_array_stars[i], D_R_e_array_stars[i] = compute_R_e(nb2, mass_half_stars, n_los=n_los,
                                                               n_part_per_bin_1=n_stars_per_bin_1, n_part_per_bin_2=n_stars_per_bin_2,
                                                               part_type=4, do_potcenter=do_potcenter)
        # Determines sigma
        # Calculer sigma dans le rayon effectif 2D, le R_e.
        n_part_per_bin_1_sigma = nb.npart_tot[4]  # We want one bin

        temp = velocity_dispersion_cylindrical_grid(nb2, n_los=n_los,
                                                    n_part_per_bin_1=n_part_per_bin_1_sigma, n_part_per_bin_2=n_part_per_bin_1_sigma,
                                                    part_type=4, r_max=R_e_array_stars[i], do_potcenter=do_potcenter)
        r_sigma, sigma_array_stars[i], D_sigma_array_stars[i] = temp[0][0], temp[1][0], temp[2][0]

        # Time determination
        time_array[i] = get_snapshot_time(file)

        # Mass_dyn determination (Attention, we need DM !!)
        nb2 = nb.selectc(nb.rxy() <= R_e_array_stars[i])
        mass_dyn[i] = nb2.get_mass_tot()

    # Finally computes Wolf relation
    # Units : kpc, 10^10 M_sun,
    unit_length = nb.UnitLength_in_cm[0]*u.cm
    unit_length = unit_length.to(u.kpc)
    unit_mass = nb.UnitMass_in_g[0]*u.g
    unit_mass = unit_mass.to(1e10*u.M_sun)
    unit_velocity = nb.UnitVelocity_in_cm_per_s*u.cm/u.s
    unit_velocity = unit_velocity.to(u.kpc/u.Gyr)
    unit_time = unit_length/unit_velocity
    G = G_a.to(unit_length**3 * unit_mass**(-1) * unit_time**(-2))

    # Units conversion
    sigma_array_stars = sigma_array_stars << u.km/u.s
    D_sigma_array_stars = D_sigma_array_stars << u.km/u.s
    sigma_array_stars = sigma_array_stars.to(unit_length/unit_time)
    D_sigma_array_stars = D_sigma_array_stars.to(unit_length/unit_time)

    R_e_array_stars = R_e_array_stars << unit_length
    D_R_e_array_stars = D_R_e_array_stars << unit_length

    # Finally computes the relation
    M_half_wolf = mass_half_light_wolf_relation(
        sigma_array_stars, R_e_array_stars, G)
    D_M_half_wolf = uncertainty_mass_half_light_wolf_relation(sigma_array_stars, D_sigma_array_stars,
                                                              R_e_array_stars, D_R_e_array_stars, G)

    M_half_wolf = M_half_wolf.to(u.M_sun).value
    D_M_half_wolf = D_M_half_wolf.to(u.M_sun).value

    # Convert to get same units
    mass_dyn = mass_dyn*1e10

    print("End of Wolf relation computations !")
    return M_half_wolf, mass_dyn, D_M_half_wolf, time_array


def makePlot(output_location, M_half_wolf, mass_dyn, D_M_half_wolf, time_array):
    plt.figure(6)
    ax = plt.subplot()
    ax.clear()
    ratio = M_half_wolf/mass_dyn
    uncertainty = D_M_half_wolf/mass_dyn
    ax.plot(time_array, ratio)
    ax.fill_between(time_array, ratio-uncertainty, ratio +
                    uncertainty, color="red", alpha=0.5)
    ax.set_xlabel(r'$t \,\left[ \rm{Gyr} \right]$')
    ax.set_ylabel(r'$M_{1/2}/M(R = R_e) $')
    ax.set_ylim(bottom=0)

    plt.tight_layout()
    plt.savefig(output_location+"mass_wolf_relation.png", format="png")
    plt.close()

# %%


def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter,
                                     description="Analyses the snapshots produced "
                                     "by simulating tidal striping of ultra faint dwarf galaxies.")

    parser.add_argument(
        'input_location', help='Folder containing all the snapshot of one simulation.')
    parser.add_argument('output_location',
                        help='Output folder to store the results.')

    parser.add_argument('--ftype', default="swift", type=str,
                        help='Type of the simulation files, e.g. swift, gadget, etc.')

    parser.add_argument('--snap_basename', default="snapshot", type=str,
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")

    parser.add_argument('--n_bins', default=64, type=int,
                        help="Number of bins of the grid")

    parser.add_argument('--r_max_DM', default=20, type=float,
                        help="Maximal radius of the grid for dark matter.")

    parser.add_argument('--r_max_stars', default=1, type=float,
                        help="Maximal radius of the grid for the stars.")

    parser.add_argument('--n_los', default=10, type=int,
                        help="Number of lines of sight.")

    parser.add_argument('--n_stars_per_bin_1', default=20, type=int,
                        help="Number of stars per bin (sub plots).")

    parser.add_argument('--n_stars_per_bin_2', default=40, type=int,
                        help="Number of stars per bin (total plot).")

    parser.add_argument('--n_DM_per_bin_1', default=250, type=int,
                        help="Number of DM particles per bin (sub plots).")

    parser.add_argument('--n_DM_per_bin_2', default=250, type=int,
                        help="Number of DM particles per bin (total plot).")

    parser.add_argument('--n_stars_per_bin_1_wolf', default=20, type=int,
                        help="Number of DM particles per bin (sub plot).")

    parser.add_argument('--n_stars_per_bin_2_wolf', default=40, type=int,
                        help="Number of DM particles per bin (total plot).")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Centers the snapshot on the minimum of the potential ; False : Does nothing.")

    args = parser.parse_args()
    return args


# %%
if __name__ == "__main__":
    args = parse_options()
    # "/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/z_30_test"
    input_location = args.input_location
    # "/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/data_analysis"
    output_location = args.output_location
    snap_basename = args.snap_basename
    ftype = args.ftype
    n_bins = args.n_bins
    r_max_stars = args.r_max_stars
    r_max_DM = args.r_max_DM
    n_los = args.n_los
    n_stars_per_bin_1 = args.n_stars_per_bin_1
    n_stars_per_bin_2 = args.n_stars_per_bin_2
    n_DM_per_bin_1 = args.n_DM_per_bin_1
    n_DM_per_bin_2 = args.n_DM_per_bin_2
    n_stars_per_bin_1_wolf = args.n_stars_per_bin_1_wolf
    n_stars_per_bin_2_wolf = args.n_stars_per_bin_2_wolf
    do_potcenter = args.do_potcenter
    no_DM_half_light_radius_computations = True  # We do not need DM in this scipt

    print("-------------------------------\nWelcome to Wolf relation analysis")

    # ----Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)

    # Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location = output_location+"/"

    # ---Performs the job
    # number of snapshots in the snap folder
    n_snapshot = get_number_snapshot_directory(input_location, snap_basename)
    files = get_all_snapshot_in_directory(input_location, snap_basename)

    mass_half_stars, mass_half_DM, r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time_array = half_mass_radius(files, n_snapshot, ftype, n_los, n_bins,
                                                                                                                               n_stars_per_bin_1, n_stars_per_bin_2, n_DM_per_bin_1, n_DM_per_bin_2, r_max_stars, r_max_DM,
                                                                                                                               do_potcenter, no_DM_half_light_radius_computations)
    M_half_wolf, mass_dyn, D_M_half_wolf, time_array = wolf_relation(files, n_snapshot, ftype, n_stars_per_bin_1_wolf,
                                                                     n_stars_per_bin_2_wolf, n_los, mass_half_stars, do_potcenter)

    # ---Saves the data
    np.savez_compressed(output_location+"wolf_relation", M_half_wolf=M_half_wolf,
                        M_dyn=mass_dyn, D_M_half_wolf=D_M_half_wolf, t=time_array)

    # ---Makes the plot
    makePlot(output_location, M_half_wolf, mass_dyn, D_M_half_wolf, time_array)
