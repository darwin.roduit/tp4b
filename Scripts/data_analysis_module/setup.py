from setuptools import Extension, setup
from Cython.Build import cythonize

extensions = [Extension("c_functions", ["c_functions.pyx"])]

setup(
    package_dir={'data_analysis_module': ''},
    ext_modules=cythonize(extensions),
)
