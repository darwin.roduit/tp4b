#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Analyses the velocity dispersion of snapshots produced by simulating tidal striping of ultra faint 
    dwarf galaxies. 
    Outputs :
        velocity_dispersion.npz : contains data about the velocity dispersion (sigma_z) for the stars and dark matter at different times
            - r_stars (dtype=object): radius of the stars
            - r_DM (dtype=object): radius of the dark matter
            - sigma_stars (dtype=object): velocity dispersion of the stars
            - D_sigma_stars (dtype=object): uncertainty on sigma_stars
            - sigma_DM (dtype=object): velocity dispersion of DM
            - D_sigma_DM (dtype=object): uncertainty on sigma_DM
"""

import sys
import os
import numpy as np
import copy
import argparse

#Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from matplotlib import pyplot as plt
from matplotlib import cm 
from tqdm import tqdm

import Gtools as gt
from pNbody import Nbody
from pNbody import units
from pNbody import libgrid

from argparse_utils import RawTextArgumentDefaultsHelpFormatter
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, choose_n_filenames, get_snapshot_time

#Bug fix This should fix the error: "RuntimeError: Invalid DISPLAY variable""
plt.switch_backend('agg')

# %% Functions
def velocity_dispersion_cylindrical_grid(nb, **kwargs):
    # kwargs parameters----------------------------------------------------------------
    part_type = kwargs.get("part_type", None)
    r_max = kwargs.get("r_max", None)
    n_los = kwargs.get("n_los", 3)
    # "number of particles per bin (sub plots)"
    n_part_per_bin_1 = kwargs.get("n_part_per_bin_1", int(1e3))
    # "number of particles per bin (total plot)"
    n_part_per_bin_2 = kwargs.get("n_part_per_bin_2", 30)
    # "mode used to define the bin radius in irregular grid (center, mean, uniform)"
    r_mode = kwargs.get("r_mode", "uniform")
    do_potcenter = kwargs.get("do_potcenter", True)
    # --------------------------------------------------------------------------

    r_max_saved = r_max

    # Selects the type of particules
    nb2 = nb  # copy.deepcopy(nb)
    if part_type is not None:
        nb2 = nb.select(part_type)

    if do_potcenter:
        nb2.potcenter()  # Centers at the min of the potential

    # output units
    out_units_x = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    out_units_y = units.UnitSystem(
        'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

    fx = nb2.localsystem_of_units.convertionFactorTo(
        out_units_x.UnitLength)
    fy = nb2.localsystem_of_units.convertionFactorTo(
        out_units_y.UnitVelocity)

    nb.pos = nb2.pos * fx
    nb.vel = nb2.vel * fy

    xs = np.zeros(0)
    ys = np.zeros(0)

    if n_los > 1:
        los = gt.getLOS(n_los, seed=0)
    else:
        los = np.array([0, 0, 1])

    for j in range(n_los):
        try:
            del nb2.Tree
        except AttributeError:
            # print("No nb.Tree attribute. Passing this step.")
            pass
        nbc = copy.deepcopy(nb2)  # copy the model

        # rotate
        if n_los > 1:
            nbc.align(axis=los[j])

        if do_potcenter:
            nbc.potcenter()

        r0 = nbc.rxy()  # compute the radius R
        v_z = nbc.Vz()

        # Keeps only the values at r0<r_max
        index = np.zeros(0)
        do_first_iteration = True
        r_max = r_max_saved

        # Iterates to take into account the fact that all the stars may move
        # out of r_max during the simulation
        while do_first_iteration or (len(index) == 0):
            do_first_iteration = False
            if r_max is not None:
                index = np.argwhere(r0 < r_max).flatten()
                r02 = r0[index]
                v_z2 = v_z[index]
            r_max *= 2.0
            # Stops increasing if r_max is bigger thant the size of the box
            if r_max >= nbc.boxsize[0]:
                r_max = nbc.boxsize[0]
                break

        # print("After while")
        r_max = r_max_saved
        
        # print("r_max = {}".format(r_max))
        # define the grid

        G = libgrid.CylindricalIrregular_1dr_Grid(r02, n_part_per_bin_1, rmode=r_mode)

        # compute the values
        rs = G.get_R()
        
        #Try to increase r_max if there is not enough points
        #Note: I should try to do the reverse : decrease r_max if it is indeed too big...
        #Alternative : Find the maximal size of the system and increase r_max based on that. Or choose
        # r_max based on that (e.g. take at least half of the points, if it's not enough, increase r_max to take 3/4 of the points, etc). 
        while (rs.size < 3):
            if r_max <= 5:
                r_max *= 2.0
            else: #Avoid to big changes...
                r_max += 1
                
            G = libgrid.CylindricalIrregular_1dr_Grid(r02, n_part_per_bin_1, rmode=r_mode)
            rs = G.get_R()
        
            # Stops increasing if r_max is bigger thant half the size of the box
            if r_max >= 0.5*nbc.boxsize[0]:
                r_max = 0.5*nbc.boxsize[0]
                break

        Means, Sigmas = G.get_MeanAndStd(v_z2)

        xs = np.concatenate((xs, rs))
        ys = np.concatenate((ys, Sigmas))

    if n_los > 1:
        G = libgrid.CylindricalIrregular_1dr_Grid(xs, n_part_per_bin_2)
        x = G.get_R()
        mean, std = G.get_MeanAndStd(ys)
        y = mean
    return x, y, std


def velocity_dispersion(files, n_profile, ftype, n_DM_per_bin_1, n_DM_per_bin_2, n_stars_per_bin_1,
                        n_stars_per_bin_2, n_los, r_max_DM, r_max_stars, do_potcenter, no_DM):
    r_array_stars = np.zeros(n_profile+1, dtype=object)
    r_array_DM = np.zeros(n_profile+1, dtype=object)
    sigma_array_stars = np.zeros(n_profile+1, dtype=object)
    sigma_array_DM = np.zeros(n_profile+1, dtype=object)
    D_sigma_array_stars = np.zeros(n_profile+1, dtype=object)
    D_sigma_array_DM = np.zeros(n_profile+1, dtype=object)
    time_array_n_profile = np.zeros(n_profile+1)

    print("Computation of the velocity dispersion within r_max_DM = {:.2e} and "
          "r_max_stars = {:.2e}.".format(r_max_DM, r_max_stars))
    for i, file in enumerate(tqdm(files)):
        nb = Nbody(file, ftype=ftype, ptypes=[4])
        r_array_stars[i], sigma_array_stars[i], D_sigma_array_stars[i] = velocity_dispersion_cylindrical_grid(nb, n_los=n_los,
                                                                                                              n_part_per_bin_1=n_stars_per_bin_1, n_part_per_bin_2=n_stars_per_bin_2,
                                                                                                              r_max=r_max_stars, do_potcenter=do_potcenter) #Centers on the mass of the stars, useful if they decouple from DM
        if not no_DM:
            nb = Nbody(file, ftype=ftype, ptypes=[1])   
            r_array_DM[i], sigma_array_DM[i], D_sigma_array_DM[i] = velocity_dispersion_cylindrical_grid(nb, n_los=n_los,
                                                                                                         n_part_per_bin_1=n_DM_per_bin_1, n_part_per_bin_2=n_DM_per_bin_2,
                                                                                                         r_max=r_max_DM, do_potcenter=do_potcenter)
        time_array_n_profile[i] = get_snapshot_time(file)
    print("End of the velocity dispersion !")
    return r_array_stars, r_array_DM, sigma_array_stars, sigma_array_DM, D_sigma_array_stars, D_sigma_array_DM, time_array_n_profile


def makePlot(output_location, r_array_stars, r_array_DM, sigma_array_stars, sigma_array_DM, time_array_n_profile, colours):
    fig, ax = plt.subplots(nrows=1, ncols=2, num=1, figsize=(12, 4.5))
    ax[0].clear()
    ax[1].clear()

    # Plot for DM
    ax[0].title.set_text("Dark matter")
    for r, sigma, t, col in zip(r_array_DM, sigma_array_DM, time_array_n_profile, colours):
        label = r"$t={:.2f}$ Gyr".format(t)
        ax[0].plot(r, sigma, color=col)
    ax[0].set_xlabel(r'$R\,\left[ \rm{kpc} \right]$')
    ax[0].set_ylabel(r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$')
    ax[0].set_ylim(bottom=0)

    # Plot for the stars
    ax[1].title.set_text("Stars")
    for r, sigma, t, col in zip(r_array_stars, sigma_array_stars, time_array_n_profile, colours):
        label = r"$t={:.2f}$ Gyr".format(t)
        ax[1].plot(r, sigma, label=label, color=col)
    ax[1].set_xlabel(r'$R\,\left[ \rm{kpc} \right]$')
    ax[1].legend()
    ax[1].set_ylim(bottom=0)

    plt.tight_layout()
    plt.savefig(output_location+"velocity_dispersion.png", format="png")
    plt.close()

# %% Parser


def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter,
                                     description="Analyses the velocity dispersion of snapshots produced "
                                     "by simulating tidal striping of ultra faint dwarf galaxies.",
                                     epilog="velocity_dispersion.npz : contains data about the velocity dispersion "
                                     "(sigma_z) for the stars and dark matter at different times\n"
                                     " - r_stars (dtype=object): radius of the stars\n"
                                     "- r_DM (dtype=object): radius of the dark matter\n"
                                     "- sigma_stars (dtype=object): velocity dispersion of the stars\n"
                                     "- D_sigma_stars (dtype=object): uncertainty on sigma_stars\n"
                                     "- sigma_DM (dtype=object): velocity dispersion of DM"
                                     "- D_sigma_DM (dtype=object): uncertainty on sigma_DM")

    parser.add_argument(
        'input_location', help='Folder containing all the snapshot of one simulation.')
    parser.add_argument('output_location',
                        help='Output folder to store the results.')

    parser.add_argument('--ftype', default="swift", type=str,
                        help='Type of the simulation files, e.g. swift, gadget, etc.')

    parser.add_argument('--snap_basename', default="snapshot", type=str,
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")

    parser.add_argument('--n_profile', default=10, type=int,
                        help="Number of snapshots.")
                        
    parser.add_argument('--files', default=None, type=str, nargs="*",
                        help="Snapshot to analyze.")

    parser.add_argument('--r_max_DM', default=20, type=float,
                        help="Maximal radius of the grid for dark matter.")

    parser.add_argument('--r_max_stars', default=1, type=float,
                        help="Maximal radius of the grid for the stars.")

    parser.add_argument('--n_los', default=10, type=int,
                        help="Number of lines of sight.")

    parser.add_argument('--n_stars_per_bin_1', default=20, type=int,
                        help="Number of stars per bin (sub plots).")

    parser.add_argument('--n_stars_per_bin_2', default=40, type=int,
                        help="Number of stars per bin (total plot).")

    parser.add_argument('--n_DM_per_bin_1', default=250, type=int,
                        help="Number of DM particles per bin (sub plots).")

    parser.add_argument('--n_DM_per_bin_2', default=250, type=int,
                        help="Number of DM particles per bin (total plot).")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Center the snapshot on the minimum of the potential ; False : Does nothing.")

    parser.add_argument('--no_DM', default=False, action="store_true",
                        help="False : Perform the computations of the DM. This takes a lot of time."
                        "True : Do not do it ; it's quicker.")

    args = parser.parse_args()
    return args


# %% Main
if __name__ == "__main__":
    args = parse_options()
    input_location = args.input_location
    output_location = args.output_location
    snap_basename = args.snap_basename
    ftype = args.ftype
    n_DM_per_bin_1 = args.n_DM_per_bin_1
    n_DM_per_bin_2 = args.n_DM_per_bin_2
    n_stars_per_bin_1 = args.n_stars_per_bin_1
    n_stars_per_bin_2 = args.n_stars_per_bin_2
    n_los = args.n_los
    r_max_DM = args.r_max_DM
    r_max_stars = args.r_max_stars
    n_profile = args.n_profile
    do_potcenter = args.do_potcenter
    no_DM = args.no_DM
    files = args.files

    # ----Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)

    # Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location = output_location+"/"

    # choses n colors in the colormap
    colours = cm.rainbow(np.linspace(0, 1, n_profile+1))

    # ---Performs the job
    # number of snapshots in the snap folder
    if files is None:
        n_snapshot = get_number_snapshot_directory(input_location, snap_basename)
        files = choose_n_filenames(
            n_snapshot, n_profile, snap_basename, input_location)
        print("Files to analyze: ", files)

    r_array_stars, r_array_DM, sigma_array_stars, sigma_array_DM, D_sigma_array_stars, D_sigma_array_DM, time_array_n_profile = velocity_dispersion(files, n_profile, ftype,
                                                                                                                                                    n_DM_per_bin_1, n_DM_per_bin_2, n_stars_per_bin_1, n_stars_per_bin_2, n_los, r_max_DM,
                                                                                                                                                    r_max_stars, do_potcenter, no_DM)

    # ---Saves the data
    np.savez_compressed(output_location+"velocity_dispersion", r_stars=r_array_stars,
                        r_DM=r_array_DM, sigma_stars=sigma_array_stars, sigma_DM=sigma_array_DM,
                        D_sigma_stars=D_sigma_array_stars, D_sigma_DM=D_sigma_array_DM,
                        t=time_array_n_profile)

    # ---Makes the plot
    makePlot(output_location, r_array_stars, r_array_DM,
             sigma_array_stars, sigma_array_DM, time_array_n_profile, colours)
