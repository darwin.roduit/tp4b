#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Documentation"""

import multiprocessing as mp
import Gtools as gt
from pNbody import libgrid
from pNbody import units
from pNbody import Nbody
from tqdm import tqdm
from matplotlib import pyplot as plt
import numpy as np
import copy
import argparse
import sys
import os


# Those two lines allows to run this script from whatever directory, ex : in
# TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))


from data_analysis_module.spherical_mass_profile import integrated_mass_spherical_grid
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, get_snapshot_time, get_all_snapshot_in_directory
from argparse_utils import RawTextArgumentDefaultsHelpFormatter

#Bug fix: RuntimeError: Invalid DISPLAY variable
plt.switch_backend('agg')

# %% Functions

def compute_R_e(nb, mass_half, **kwargs):
    """Determines the 2D projected half-light radius. """
    # kwargs parameters--------------------------------------------------------
    part_type = kwargs.get("part_type", None)
    n_los = kwargs.get("n_los", 3)
    # "number of particles per bin (sub plots)"
    n_part_per_bin_1 = kwargs.get("n_part_per_bin_1", int(1e3))
    # "number of particles per bin (total plot)"
    n_part_per_bin_2 = kwargs.get("n_part_per_bin_2", 30)
    # "mode used to define the bin radius in irregular grid (center, mean,
    # uniform)"
    r_mode = kwargs.get("r_mode", "uniform")
    do_potcenter = kwargs.get("do_potcenter", False)
    # --------------------------------------------------------------------------

    # Selects the type of particules
    nb2 = nb  # copy.deepcopy(nb)
    if part_type is not None:
        nb2 = nb.select(part_type)

    if do_potcenter:
        nb2.potcenter()  # Centers at the min of the potential

    # output units
    out_units_pos = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    out_units_mass = units.UnitSystem(
        'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

    f_pos = nb2.localsystem_of_units.convertionFactorTo(
        out_units_pos.UnitLength)
    f_mass = nb2.localsystem_of_units.convertionFactorTo(
        out_units_mass.UnitMass)

    nb2.pos = nb2.pos * f_pos
    nb2.mass = nb2.mass * f_mass
    R_s = np.zeros(0)
    M_s = np.zeros(0)

    if n_los > 1:
        los = gt.getLOS(n_los, seed=0)
    else:
        los = np.array([0, 0, 1])

    for j in range(len(los)):
        try:
            del nb2.Tree
        except AttributeError:
            # print("No nb.Tree attribute. Passing this step.")
            pass
        nbc = copy.deepcopy(nb2)  # copy the model

        # rotate
        if n_los > 1:
            nbc.align(axis=los[j])

        if do_potcenter:
            nbc.potcenter()

        r0 = nbc.rxy()  # compute the radius R

        # define the grid
        G = libgrid.CylindricalIrregular_1dr_Grid(
            r0, n_part_per_bin_1, rmode=r_mode)

        # compute the values
        r_s = G.get_R()
        mass = nbc.mass

        integrated_mass = G.get_IntegratedSum(mass)
        R_s = np.concatenate((R_s, r_s))
        M_s = np.concatenate((M_s, integrated_mass))
    if n_los > 1:
        G = libgrid.CylindricalIrregular_1dr_Grid(R_s, n_part_per_bin_2)

        # Finds the closest value to M_1/2 that is available in the grid
        diff_two_masses = np.max(np.diff(M_s))
        relative_tolerance = 100/diff_two_masses
        index = np.argwhere(np.isclose(
            M_s, mass_half, rtol=relative_tolerance)).flatten()

        while (index.size == 0):
            relative_tolerance *= 10
            index = np.argwhere(np.isclose(
                M_s, mass_half, rtol=relative_tolerance)).flatten()
        R_e_values = R_s[index]
        R_e = np.mean(R_e_values)
        D_R_e = np.std(R_e_values)
    return R_e, D_R_e


def do_job(file, ftype, mass_half_stars, mass_half_DM, n_los,
           n_stars_per_bin_1, n_stars_per_bin_2, n_DM_per_bin_1, n_DM_per_bin_2,
           do_potcenter, no_DM_half_light_radius_computations):
    nb = Nbody(file, ftype=ftype)
    
    time = get_snapshot_time(file)

    # For stars
    r_half_stars, Delta_r_half_stars = compute_R_e(nb,  mass_half_stars,
                                                   n_los=n_los,
                                                   n_part_per_bin_1=n_stars_per_bin_1,
                                                   n_part_per_bin_2=n_stars_per_bin_2,
                                                   part_type=4,
                                                   do_potcenter=do_potcenter, #Centers on the mass of the stars, useful if they decouple from DM
                                                   r_mode="uniform")
    # For DM
    if not no_DM_half_light_radius_computations:  # True by default, so does the computations
        r_half_DM, Delta_r_half_DM = compute_R_e(nb, mass_half_DM, n_los=n_los,
                                                 n_part_per_bin_1=n_DM_per_bin_1,
                                                 n_part_per_bin_2=n_DM_per_bin_2,
                                                 part_type=1,
                                                 do_potcenter=do_potcenter,
                                                 r_mode="uniform")
    else:
        r_half_DM = 0
        Delta_r_half_DM = 0
        
    return r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time


def half_mass_radius(files, n_snapshot, ftype, n_los, n_bins, n_stars_per_bin_1, n_stars_per_bin_2, n_DM_per_bin_1, n_DM_per_bin_2,
                     r_max_stars, r_max_DM, do_potcenter,
                     no_DM_half_light_radius_computations):
    """Compute the half-light radius."""
    nb_IC = Nbody(files[0], ftype=ftype)

    # Determines the half mass for the ICs
    # Stars
    r_stars, integrated_mass_stars = integrated_mass_spherical_grid(nb_IC, part_type=4, r_max=r_max_stars,
                                                                    n_bins=n_bins, do_potcenter=do_potcenter)
    mass_tot_stars = integrated_mass_stars[-1]  # M_tot is the last element
    mass_half_stars = 0.5*mass_tot_stars

    # Dark matter
    if not no_DM_half_light_radius_computations:
        r_DM, integrated_mass_DM = integrated_mass_spherical_grid(nb_IC, part_type=1, r_max=r_max_DM,
                                                              n_bins=n_bins, do_potcenter=do_potcenter)
        mass_tot_DM = integrated_mass_DM[-1]
        mass_half_DM = 0.5*mass_tot_DM
    else:
        mass_tot_DM = 0
        mass_half_DM = 0

    # Now does the actual job of finding r(M=M_1/2)
    r_half_stars = np.zeros(n_snapshot)
    r_half_DM = np.zeros(n_snapshot)
    Delta_r_half_stars = np.zeros(n_snapshot)
    Delta_r_half_DM = np.zeros(n_snapshot)
    time_array = np.zeros(n_snapshot)
    async_result_array = np.zeros(n_snapshot, dtype=object)

    print("Starting the evaluation of the radius of the half mass...")

    # Bug fix : maxtasksperchild=1 avoids memory problems (worker are killed when they finish
    #their work and new ones are spawned)
    with mp.Pool(maxtasksperchild=1) as pool:
        # Submit the jobs
        print("Submit the jobs")
        for i, file in enumerate(files):
            async_result_array[i] = pool.apply_async(do_job, args=(file, ftype,
                                                                   mass_half_stars,
                                                                   mass_half_DM, n_los,
                                                                   n_stars_per_bin_1,
                                                                   n_stars_per_bin_2,
                                                                   n_DM_per_bin_1,
                                                                   n_DM_per_bin_2,
                                                                   do_potcenter,
                                                                   no_DM_half_light_radius_computations),
                                                     callback=lambda x: print("One task done."))

        print("Waiting for the tasks to complete...")
        results = [ar.get() for ar in tqdm(async_result_array)]

    for i in range(len(files)):
        r_half_stars[i], Delta_r_half_stars[i], r_half_DM[i], Delta_r_half_DM[i], time_array[i] = results[i]

    print("Ending the evaluation of radius of the half mass !")
    return mass_half_stars, mass_half_DM, r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time_array


def makePlot(output_location, time_array, r_half_DM, Delta_r_half_DM,
             r_half_stars, Delta_r_half_stars):
    fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(12, 4.5))
    ax[0].clear()
    ax[1].clear()

    # DM
    ax[0].title.set_text("Dark matter")
    ax[0].plot(time_array, r_half_DM)
    ax[0].fill_between(time_array, r_half_DM-Delta_r_half_DM,
                       r_half_DM+Delta_r_half_DM, color="red", alpha=0.5)
    ax[0].set_xlabel(r'$t \,\left[ \rm{Gyr} \right]$')
    ax[0].set_ylabel(r'$R(M = M_{1/2}) \,\left[ \rm{kpc} \right]$')
    ax[0].set_ylim(bottom=0)

    # Stars
    ax[1].title.set_text("Stars")
    ax[1].plot(time_array, r_half_stars)
    ax[1].fill_between(time_array, r_half_stars-Delta_r_half_stars,
                       r_half_stars+Delta_r_half_stars, color="red", alpha=0.5)
    ax[1].set_xlabel(r'$t \,\left[ \rm{Gyr} \right]$')
    ax[1].set_ylim(bottom=0)

    plt.tight_layout()
    plt.savefig(output_location+"radius_for_half_mass.png", format="png")
    plt.close()

# %% Parse options


def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter,
                                     description="Analyses the half light mass"
                                     "of snapshots produced by simulating tidal"
                                     "striping of ultra faint dwarf galaxies.")

    parser.add_argument('input_location', help="Folder containing all the"
                        "snapshot of one simulation. Those snapshots must be centered on the center"
                        "of mass. ")
    parser.add_argument('output_location',
                        help='Output folder to store the results.')

    parser.add_argument('--ftype', default="swift", type=str,
                        help='Type of the simulation files, e.g. swift, gadget, etc.')

    parser.add_argument('--snap_basename', default="snapshot", type=str,
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")

    parser.add_argument('--n_bins', default=64, type=int,
                        help="Number of bins of the grid.")

    parser.add_argument('--n_los', default=10, type=int,
                        help="Number of lines of sight.")

    parser.add_argument('--n_stars_per_bin_1', default=20, type=int,
                        help="Number of stars per bin (sub plots).")

    parser.add_argument('--n_stars_per_bin_2', default=40, type=int,
                        help="Number of stars per bin (total plot).")

    parser.add_argument('--n_DM_per_bin_1', default=10000, type=int,
                        help="Number of DM particles per bin (sub plots).")

    parser.add_argument('--n_DM_per_bin_2', default=20000, type=int,
                        help="Number of DM particles per bin (total plot).")

    parser.add_argument('--r_max_DM', default=20, type=float,
                        help="Maximal radius of the grids for the DM.")

    parser.add_argument('--r_max_stars', default=1, type=float,
                        help="Maximal radius of the grid for the stars.")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Center the snapshot on the minimum of the potential ; False : Does nothing.")

    parser.add_argument('--no_DM_half_light_radius_computations', default=False, action="store_true",
                        help="False : Perform the computations of the DM half-light radius. This takes a lot of time."
                        "True : Do not do it ; it's quicker.")

    args = parser.parse_args()
    return args


# %% Main
if __name__ == "__main__":
    args = parse_options()
    # "/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/z_30_test"
    input_location = args.input_location
    # "/home/darwin/Bureau/TP4b/Scripts/test_data_analysis/data_analysis"
    output_location = args.output_location
    snap_basename = args.snap_basename
    ftype = args.ftype
    n_bins = args.n_bins
    r_max_stars = args.r_max_stars
    r_max_DM = args.r_max_DM
    n_los = args.n_los
    n_stars_per_bin_1 = args.n_stars_per_bin_1
    n_stars_per_bin_2 = args.n_stars_per_bin_2
    n_DM_per_bin_1 = args.n_DM_per_bin_1
    n_DM_per_bin_2 = args.n_DM_per_bin_2
    do_potcenter = args.do_potcenter
    no_DM_half_light_radius_computations = args.no_DM_half_light_radius_computations

    # ----Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)

    # Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location = output_location+"/"
        
    # Choose how new process are started 
    # Bug fix : OSError: [Errno 12] Cannot allocate memory (due to fork)
    mp.set_start_method("forkserver")

    # ---Performs the job
    # number of snapshots in the snap folder
    n_snapshot = get_number_snapshot_directory(input_location, snap_basename)
    files = get_all_snapshot_in_directory(input_location, snap_basename)
    mass_half_stars, mass_half_DM, r_half_stars, Delta_r_half_stars, r_half_DM, Delta_r_half_DM, time_array = half_mass_radius(files, n_snapshot, ftype, n_los, n_bins,
                                                                                                                               n_stars_per_bin_1, n_stars_per_bin_2, n_DM_per_bin_1, n_DM_per_bin_2, r_max_stars, r_max_DM,
                                                                                                                               do_potcenter, no_DM_half_light_radius_computations)

    # ---Saves the data
    np.savez_compressed(output_location+"radius_of_half_mass", mass_half_stars=mass_half_stars,
                        mass_half_DM=mass_half_DM, r_half_stars=r_half_stars, D_r_half_stars=Delta_r_half_stars,
                        r_half_DM=r_half_DM, D_r_half_DM=Delta_r_half_DM, t=time_array)

    # ---Makes the plot
    makePlot(output_location, time_array, r_half_DM,
             Delta_r_half_DM, r_half_stars, Delta_r_half_stars)
