# cython: nonecheck=False
# cython: boundscheck=False

import sys
import os

# Those two lines allows to run this script from whatever directory, ex : in
# TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

import cython
import copy
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm
from pNbody import Nbody
from pNbody import units
from pNbody import libgrid
from argparse_utils import RawTextArgumentDefaultsHelpFormatter
# from data_analysis_module.spherical_mass_profile import integrated_mass_spherical_grid
import Gtools as gt

# %% Functions
def compute_R_e(nb, double mass_half, int part_type, int n_los, int n_part_per_bin_1, int n_part_per_bin_2, r_mode, do_potcenter):
    """Determines the 2D projected half-light radius. """
    cdef double f_pos, f_mass,
    cdef double R_e = 0
    cdef double D_R_e = 0
    cdef relative_tolerance, diff_two_masses
    cdef int j, index_size

    # Selects the type of particules
    nb2 = nb  # copy.deepcopy(nb)
    if part_type is not None:
        nb2 = nb.select(part_type)

    if do_potcenter:
        nb2.potcenter()  # Centers at the min of the potential

    # output units
    out_units_pos = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    out_units_mass = units.UnitSystem(
        'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

    f_pos = nb2.localsystem_of_units.convertionFactorTo(
        out_units_pos.UnitLength)
    f_mass = nb2.localsystem_of_units.convertionFactorTo(
        out_units_mass.UnitMass)

    nb2.pos = nb2.pos * f_pos
    nb2.mass = nb2.mass * f_mass
    R_s = np.zeros(0)
    M_s = np.zeros(0)

    if n_los > 1:
        los = gt.getLOS(n_los, seed=0)
    else:
        los = np.array([0, 0, 1])

    for j in range(n_los):
        try:
            del nb2.Tree
        except AttributeError:
            # print("No nb.Tree attribute. Passing this step.")
            pass
        nbc = copy.deepcopy(nb2)  # copy the model

        # rotate
        if n_los > 1:
            nbc.align(axis=los[j, :])

        if do_potcenter:
            nbc.potcenter()

        r0 = nbc.rxy()  # compute the radius R

        # define the grid
        G = libgrid.CylindricalIrregular_1dr_Grid(
            r0, n_part_per_bin_1, rmode=r_mode)

        # compute the values
        r_s = G.get_R()
        mass = nbc.mass

        integrated_mass = G.get_IntegratedSum(mass)
        R_s = np.concatenate((R_s, r_s))
        M_s = np.concatenate((M_s, integrated_mass))
    if n_los > 1:
        G = libgrid.CylindricalIrregular_1dr_Grid(R_s, n_part_per_bin_2)

        # Finds the closest value to M_1/2 that is available in the grid
        diff_two_masses = np.max(np.diff(M_s))
        relative_tolerance = 100/diff_two_masses
        index = np.argwhere(np.isclose(
            M_s, mass_half, rtol=relative_tolerance)).flatten()

        index_size = index.size 
        while (index_size == 0):
            relative_tolerance *= 10
            index = np.argwhere(np.isclose(
                M_s, mass_half, rtol=relative_tolerance)).flatten()
            index_size = index.size
        R_e_values = R_s[index]
        R_e = np.mean(R_e_values)
        D_R_e = np.std(R_e_values)
    return R_e, D_R_e
