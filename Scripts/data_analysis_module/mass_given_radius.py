 #!/usr/bin/env python3
 # -*- coding: utf-8 -*-
"""Documentation"""
 
# print('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__,__name__,str(__package__)))

import sys
import os
import argparse

#Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from pNbody import Nbody
from pNbody import libgrid

from argparse_utils import RawTextArgumentDefaultsHelpFormatter
from data_analysis_module.snapshots_utils import get_number_snapshot_directory, get_all_snapshot_in_directory, get_snapshot_time
from data_analysis_module.spherical_mass_profile import integrated_mass_spherical_grid, f, fm

#%%
def find_r_star(integrated_mass, r_tilde, r, n_bins):
    """Finds the position of r_tilde (the fixed radius at which the mass is 
    evaluated) in the array."""
    
    #  print("r_tilde = {}".format(r_tilde))
    
    #Check that r is not empty
    if r.size==0: 
        print("WARNING: The radii array r of the system is empty (r={}). Choose either a bigger value for r_tilde, or a bigger value for r_max to get a non empty array. You may want to change the binning, too. \nThe snapshot may not be properly centered.".format(r))
        mass_star = 1e-10 #for log scale...
        Delta_m_star = 1e-10
        r_star_comp = 1e-10
        return mass_star, Delta_m_star, r_star_comp
    
    index = np.argwhere(np.isclose(r, r_tilde, 1/n_bins)).flatten()
        
    #When it does not find the value, take the last one in the array
    if index.size==0:        
        if np.all(r > r_tilde):
            print("Message : The radii r of the system are bigger than r_tilde={}".format(r_tilde))
            index=[0]
        elif np.all(r < r_tilde):
            index=[len(r)-1]
        else:
            raise RuntimeError("The radii are not smaller nor bigger than r_tilde={}. There is a problem somewhere...".format(r_tilde))
    
    #Take the first occurence
    index = index[0]
    
    #Find the error... Treat the different cases (boundaries or in the middle of the array)
    if index<len(r)-1 and index>0:
        Delta_m_star = np.max([np.abs(integrated_mass[index+1]-integrated_mass[index]), 
                                  np.abs(integrated_mass[index]-integrated_mass[index-1])])
    elif index==0:
        Delta_m_star=np.abs(r[index+1]-r[index])
    else:
        Delta_m_star=np.abs(r[index-1]-r[index])
            
    r_star_comp = r[index]
    mass_star = integrated_mass[index]
    
    return mass_star, Delta_m_star, r_star_comp

def mass_given_radius(files, n_snapshot, ftype, n_bins, r_tilde, r_max , do_potcenter, 
                                          no_DM_half_light_radius_computations):    
    #We will use the *same* grid, so we compute it once and for all
    rc = 1
    spherical_grid = libgrid.Spherical_1d_Grid(rmin=0, rmax=r_max, 
                                                nr=n_bins, g=lambda r:f(r, rc) , gm=lambda r:fm(r, rc))
    
    #Determines r_star_approx, i.e. the closest value to r_tilde available in the grid)
    nb_IC = Nbody(files[0], ftype=ftype)
    r, integrated_mass = integrated_mass_spherical_grid(nb_IC, spherical_grid=spherical_grid,
                                                        r_max=r_max, n_bins=n_bins, do_potcenter=do_potcenter)
    
    #Now compute the closest value to r_tilde
    #Notice that now we keep the grid fixed so that this value of r_tilde is the same 
    #for all files. Also notice that this value is the same for the stars and DM.
    index = np.argmin(np.abs(r - r_tilde))
    r_star_approx = r[index]
    
    #----For other files
    r_star_comp_stars = np.zeros(n_snapshot) ; r_star_comp_DM = np.zeros(n_snapshot)
    mass_star_stars = np.zeros(n_snapshot) ; mass_star_DM = np.zeros(n_snapshot)
    Delta_m_star_stars = np.zeros(n_snapshot) ; Delta_m_star_DM = np.zeros(n_snapshot)
    time_array = np.zeros(n_snapshot)
    
    print("Starting the evaluation of M(r={:.2f})".format(r_tilde))
    for i,file in enumerate(tqdm(files)):
        nb = Nbody(file, ftype=ftype)
        

        #Compute the integrated mass profile         
        r_stars, integrated_mass_stars = integrated_mass_spherical_grid(nb, spherical_grid=spherical_grid, 
                                    part_type=4, r_max=r_max, n_bins=n_bins, do_potcenter=do_potcenter)
        
        #Determine the intersection of the half mass with the curve
        #  print("r_stars = {}".format(r_stars))

        #  if r_stars.size==0:
            #  print("nb.rxyz() = {}".format(np.sort(nb.rxyz())))

        mass_star_stars[i], Delta_m_star_stars[i], r_star_comp_stars[i] = find_r_star(integrated_mass_stars, r_star_approx, r_stars, n_bins)
       
        if not no_DM_half_light_radius_computations:
            r_DM, integrated_mass_DM = integrated_mass_spherical_grid(nb, spherical_grid=spherical_grid, 
                                    part_type=1, r_max=r_max, n_bins=n_bins, do_potcenter=do_potcenter) 
            
            mass_star_DM[i], Delta_m_star_DM[i], r_star_comp_DM[i] = find_r_star(integrated_mass_DM, r_star_approx, r_DM, n_bins)
        
        time_array[i] = get_snapshot_time(file) #Computed above
    print("Ending the evaluation of  M(r={:.2f}) !".format(r_tilde))
    return r_star_approx, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM, time_array

def makePlot(output_location, r_tilde, time_array, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM):    
    fig, ax = plt.subplots(nrows=1, ncols=2, num=4, figsize=(12, 4.5)) 
    ax[0].clear() ; ax[1].clear()
    
    #For DM
    ax[0].title.set_text("Dark matter")
    ax[0].plot(time_array, mass_star_DM, label="DM")
    ax[0].fill_between(time_array, mass_star_DM-Delta_m_star_DM, mass_star_DM+Delta_m_star_DM, color="red", alpha=0.5)
    ax[0].set_xlabel(r'$t \,\left[ \rm{Gyr} \right]$')
    label_part = r"$M_(r={:.2f})".format(r_tilde)
    ax[0].set_ylabel(label_part + r'\,\left[ M_{\odot} \right]$')
    ax[0].set_yscale("log")

    #For Stars
    ax[1].title.set_text("Stars")
    ax[1].plot(time_array, mass_star_stars, label="Stars")
    ax[1].fill_between(time_array, mass_star_stars-Delta_m_star_stars, mass_star_stars+Delta_m_star_stars, color="red", alpha=0.5)
    ax[1].set_xlabel(r'$t \,\left[ \rm{Gyr} \right]$')
    ax[1].set_yscale("log")

    plt.tight_layout()
    plt.savefig(output_location+"mass_at_r={:.2f}.png".format(r_tilde), format="png")
    plt.close()

#%%
def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter, 
                                     description="Analyses the snapshots produced "
                                     "by simulating tidal striping of ultra faint dwarf galaxies.")
    
    parser.add_argument('input_location', help='Folder containing all the snapshot of one simulation.')
    parser.add_argument('output_location', help='Output folder to store the results.')

    parser.add_argument('--ftype', default="swift", type=str, 
                        help='Type of the simulation files, e.g. swift, gadget, etc.')
    
    parser.add_argument('--snap_basename', default="snapshot", type=str, 
                        help="Basename of the snapshots files, i.e the name before "
                        "the numbers. E.g. snapshot, snap, etc.")

    parser.add_argument('--n_bins', default=64, type=int, 
                        help="Number of bins of the grid.")

    parser.add_argument('--r_tilde', default=0.25, type=float, 
                        help="Fixed radius at which the evolution in time of the mass will be computed. Default: 0.5")

    parser.add_argument('--r_max', default=0.5, type=float, 
                        help="Maximal radius of the grid.")

    parser.add_argument('--do_potcenter', default=False, action="store_true",
                        help="True : Centers the snapshot on the minimum of the potential ; False : Does nothing.")
    
    parser.add_argument('--no_DM_half_light_radius_computations', default=False, action="store_true",
                        help="False : Perform the computations of the DM half-light radius. This takes a lot of time."
                        "True : Do not do it ; it's quicker.")
    
    args = parser.parse_args()
    return args 

#%%
if __name__ == "__main__":
    args = parse_options()
    input_location = args.input_location
    output_location = args.output_location
    snap_basename = args.snap_basename
    ftype = args.ftype
    n_bins = args.n_bins
    r_tilde = args.r_tilde
    r_max = args.r_max
    do_potcenter = args.do_potcenter
    no_DM_half_light_radius_computations = args.no_DM_half_light_radius_computations
    
    #----Creates the output directory
    if not (os.path.exists(output_location)):
        print("Output directory does not exist. It will be created.")
        os.makedirs(output_location)
    
    #Adds the "/" at the end if needed
    if (output_location[-1] != "/"):
        output_location=output_location+"/"
    
    #---Performs the job
    n_snapshot = get_number_snapshot_directory(input_location, snap_basename) #number of snapshots in the snap folder
    files = get_all_snapshot_in_directory(input_location, snap_basename)
    r_star_approx, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM, time_array = mass_given_radius(files, 
                                                                                                                      n_snapshot, ftype, n_bins, r_tilde, r_max , do_potcenter,
                                                                                                                      no_DM_half_light_radius_computations)
    
    #---Saves the data
    np.savez_compressed(output_location+"mass_at_given_radius", r_tilde=r_tilde, r_star_bin=r_star_approx, 
                        mass_star_stars=mass_star_stars, mass_star_DM=mass_star_DM, 
                        D_m_star_stars=Delta_m_star_stars, D_m_star_DM=Delta_m_star_DM,
                        t=time_array)
    
    #---Makes the plot
    makePlot(output_location, r_tilde, time_array, mass_star_stars, mass_star_DM, Delta_m_star_stars, Delta_m_star_DM)


    
