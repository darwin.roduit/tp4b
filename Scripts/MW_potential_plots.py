# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 19 10:58:42 2023

This file plots the potential MWPotential2014 from Bovy's paper.  It is also
used to determine the circular velocity of the particles for the example in
Swift,  as well as giving the values of the potential parameters for Swift.

@author: Darwin Roduit
"""

from scipy.optimize import curve_fit
from galpy.potential import plotRotcurve
from galpy.potential import MWPotential2014
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import ticker
from astropy import units as u
from astropy.constants import G as G_a

from pNbody.mass_models import MWPotential2014 as MW

# import MWPotential2014 as MW


plt.style.use(r"/home/darwin/Bureau/TP4b/Scripts/lab_style_figure.mplstyle")
matplotlib.use("qtagg")
# output_location="../Report/figures/"
output_location = ""

# %%Functions


# %%Retrieve the data

# Units : kpc, 10^10 M_sun,
unit_length = 1 * u.kpc
unit_mass = 1e10 * u.M_sun
unit_mass = unit_mass.to(unit_mass)
unit_time = 3.086e16 * u.s
unit_time = unit_time.to(unit_time)
unit_velocity = unit_length / unit_time

# Retrieve the parameters from the package
params = MW.MWPotential2014_parameters()

# Converts G to the right units
G = G_a.to(unit_length**3 * unit_mass ** (-1) * unit_time ** (-2))
print(G)

# NFW parameters
M_200 = params["M_200"] * unit_mass
c = params["c"]
rho_0 = params["rho_0"]
r_s = params["r_s"] * unit_length
r_200 = r_s * c
# M_200/(200*4/3*np.pi*r_200**3)
rho_c = params["rho_c"] * unit_mass / unit_length**3
H_0 = params["H_0"] / unit_time  # np.sqrt(8*np.pi*G/3 * rho_c)

print("c = {}".format(c))
print("M_200 = {}".format(M_200))
print("H_0 = {}".format(H_0))
print(H_0.to(u.km / u.s / u.Mpc))
print("rho_c = {}".format(rho_c))
print("r_200 = {}".format(r_200))
print("rho_0 = {:.2e}".format(rho_0))

# This is how swift computes things -- It's just a check that it is consistent
# rho_crit = 3*H_0**2/(8*np.pi*G)
# print(rho_crit)
# r_200_s = np.cbrt(3*M_200/(4*np.pi*200*rho_crit))
# print(r_200_s)

# Leave out the unit
r_s = r_s.value
G = G.value
# rho_0 = rho_0.value

# MN parameters
M_disk = params["M_disk"]
a = params["a"]
b = params["b"]

# PSC parameters
alpha = params["alpha"]
r_c = params["r_c"]
amplitude = params["amplitude"]
r_1 = params["r_1"]

# Potential contribution
f_1 = params["f_1"]
f_2 = params["f_2"]
f_3 = params["f_3"]

# %%CI pour l'exemple de swift

r_p1 = np.array([0, 0, 5])
R_p1 = np.sqrt(r_p1[0] ** 2 + r_p1[1] ** 2)
z_p1 = r_p1[2]
r_p2 = np.array([0, 5, 0])
R_p2 = np.sqrt(r_p2[0] ** 2 + r_p2[1] ** 2)
z_p2 = r_p2[2]
r_p3 = np.array([30, 0, 0])
R_p3 = np.sqrt(r_p3[0] ** 2 + r_p3[1] ** 2)
z_p3 = r_p3[2]
r_p4 = np.array([0, 8, 0])
R_p4 = np.sqrt(r_p4[0] ** 2 + r_p4[1] ** 2)
z_p4 = r_p4[2]


v_p1 = MW.Vcirc(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R_p1,
    z_p1, G
)
v_p2 = MW.Vcirc(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R_p2,
    z_p2, G
)
v_p3 = MW.Vcirc(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R_p3,
    z_p3, G
)
v_p4 = MW.Vcirc(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R_p4,
    z_p4, G
)

print("\n\nThe velocities are :")
print(v_p1)
print(v_p2)
print(v_p3)
print(v_p4)

# %%
# Concrètement je peux faire un fit pour que v_circ(R=8 kpc)=220 km/s pour
# trouver f_1, f_2 et f_3

courbe = plotRotcurve(MWPotential2014, Rrange=[1e-3, 30], grid=10000)[0]
plt.close()
v_circ = courbe.get_ydata() * 220
R = courbe.get_xdata() * 8


def velocity_1(x, f_1, f_2, f_3):
    return MW.Vcirc(
        rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, x,
        0, G
    )


def velocity_2(R, rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1,
               f_2, f_3):
    return MW.Vcirc(
        rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R,
        0, G
    )


bounds = (0, np.inf)
popt, pcov = curve_fit(velocity_1, R, v_circ, bounds=bounds)
err = np.sqrt(np.diag(pcov))
print(popt, err)

# bounds = ([0, 0, 0, 0, 0, -np.inf, 0, 0, 0, 0, 0, 0], np.inf)
# popt2, pcov2 = curve_fit(velocity_2, R, v_circ, bounds=bounds)
# err = np.sqrt(np.diag(pcov2))
# print(popt2, err)

y_fit = velocity_1(R, *popt)
# y_fit_2= velocity_2(R, *popt2)

# %%
fig, ax = plt.subplots(nrows=1, ncols=2, num=0, figsize=(11, 4))
ax[0].cla()
ax[1].cla()
# plt.cla()
ax[0].plot(R, v_circ, label=r"galpy")
ax[0].plot(R, y_fit, linestyle="-.", label="fit")
# ax[0].plot(R, y_fit_2, label="fit 2")
ax[0].set_xlabel("$R$ [kpc]")
ax[0].set_ylabel(r"$v_{\mathrm{circ}}$ [km/s]")
ax[0].legend()

ax[1].plot(R, 100 * np.abs(v_circ - y_fit) / v_circ, label="fit 1")
# ax[1].plot(R, 100*np.abs(v_circ-y_fit_2)/v_circ, label="fit 2")
ax[1].set_xlabel("$R$ [kpc]")
ax[1].set_ylabel(r"relative error [$\%$]")
# ax[1].legend()

plt.tight_layout()
plt.savefig(
    output_location + "MWPotential_2014_fit_parameters_f.pdf",
    format="pdf",
    bbox_inches="tight",
)
# plt.close()

# Clearly the first fit is way better (1e-13 % !)

# %%Plot
R = np.linspace(1e-1, 13, int(5e2))
z = np.linspace(-4, 4, int(5e2))
R, z = np.meshgrid(R, z)

Phi = MW.Potential(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R, z,
    G
)
rho = MW.Density(
    rho_0, r_s, M_disk, a, b, alpha, r_c, amplitude, r_1, f_1, f_2, f_3, R, z,
    G
)

fig = plt.figure(1)
fig.tight_layout()
ax1 = plt.subplot(121)
ax1.cla()
CS1 = ax1.contour(R, z, Phi, levels=15)
ax1.set_xlabel("$R$ [kpc]")
ax1.set_ylabel("$z$ [kpc]")
# fig.colorbar(CS1, label=r"$\Phi(r)$ [$\mathrm{kpc}^{2}/(3.086 \cdot 10^{16}
# \mathrm{s})$]", pad = 0.01, format='%.1e')
cb1 = fig.colorbar(
    CS1,
    label=r"$\Phi_{\mathrm{MW}}(R,z)$ [$\mathrm{kpc}^{2}/(0.98 \, \mathrm{Gyr})^2$]",
    pad=0.01,
    format="%.1e",
)
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True)
formatter.set_powerlimits((-1, 1))
cb1.ax.yaxis.set_major_formatter(formatter)

ax2 = plt.subplot(122)
ax2.cla()
CS2 = ax2.contour(R, z, rho, locator=ticker.LogLocator(base=10, subs=(1.0,)))
ax2.set_xlabel("$R$ [kpc]")  # ; ax2.set_ylabel("$z$ [kpc]")
fig.colorbar(
    CS2,
    label=r"$\rho_{\mathrm{MW}}(R,z)$ [$10^{10} M_{\odot} \, \mathrm{kpc}^{-3}$]",
    pad=0.01,
    format="%.0e",
)


fig.set_size_inches(11, 4)

plt.tight_layout()
plt.savefig(
    output_location + "MWPotential2014_potential_density_plot.pdf",
    format="pdf",
    bbox_inches="tight",
)
# plt.close()
