#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 09:58:03 2023

@author: darwin
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm , ticker
from PIL import Image

plt.style.use(r"/home/darwin/Bureau/TP4b/Scripts/lab_style_figure.mplstyle")

#%% Functions

def plot_data_profile(ax, title, r, mass, time, colours, xlabel, ylabel, xlog=False, ylog=False):
    ax.set_title(title, y=-0.25)
    for r,m,t,col in zip(r, mass, time, colours):
        label = r"$t \cong {:.2f}$ Gyr".format(t)
        ax.plot(r, m, color=col, label=label)
    if xlog:
        ax.set_xscale("log") 
    if ylog:
        ax.set_yscale("log")
    ax.set_xlabel(xlabel) 
    ax.set_ylabel(ylabel) 
    ax.legend()
    return ax

def plot_data_fill(ax, title, x, y, yerr, xlabel, ylabel, color_fil="red", xlog=False, ylog=False):
    ax.set_title(title, y=-0.25)
    ax.plot(x,  y)
    ax.fill_between(x, y-yerr, y+yerr, color=color_fil, alpha=0.5)
    if xlog:
        ax.set_xscale("log") 
    if ylog:
        ax.set_yscale("log")
    ax.set_xlabel(xlabel) 
    ax.set_ylabel(ylabel) 
    return ax

#%% Misc
output_location="/home/darwin/Bureau/TP4b/Report/figures/"
data_location = "/home/darwin/Bureau/TP4b/Simulations/For_report/"
data_to_load = [ r"IC=nfw_plummer_8_more_particles/data_analysis/", "z_2T_orbit/z=30/data_analysis/", 
                "z_2T_orbit/z=70/data_analysis/",  "r_theta_2T_orbit/r=70_theta=0.79/data_analysis/", 
                r"r_theta_2T_orbit/r=70_theta=1.57/data_analysis/",  r"Bootes_1/data_analysis/" ]

data_to_load_2 = [r"Hercules/data_analysis/", r"Tucana_2/data_analysis/"  ]

n_profile = 11
width = 11 ; height = 13.5

#%% Plots of the mass profiles------------------------------------------------------------------------------------------------
#Stars
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
colours = cm.rainbow(np.linspace(0, 1,n_profile)) #choses n colors in the colormap
title = ["a)", "b)", "c)", "d)", "e)", "f)"]
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "mass_profile_data.npz"
    data = np.load(file, allow_pickle=True)
    r_stars = data["r_stars"] ;  mass_stars = data["mass_stars"]  ; time = data["t"]
    plot_data_profile(ax[array_index], title[i], r_stars, mass_stars, time, colours, r'$\rm{r}\,\left[ \rm{kpc} \right]$', r'$\rm{M(r)}\,\left[ M_{\odot} \right]$', True, True)
    ax[array_index].set_xlim(right=1)

plt.tight_layout()
plt.savefig(output_location+"mass_profile_stars.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "mass_profile_data.npz"
    data = np.load(file, allow_pickle=True)
    r_DM= data["r_DM"] ;  mass_DM = data["mass_DM"]  ; time = data["t"]
    plot_data_profile(ax[array_index], title[i], r_DM, mass_DM, time, colours, r'$\rm{r}\,\left[ \rm{kpc} \right]$', r'$\rm{M(r)}\,\left[ M_{\odot} \right]$', True, True)

plt.tight_layout()
plt.savefig(output_location+"mass_profile_DM.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Now for Hercules and Tucana 2
#Stars
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "mass_profile_data.npz"
    data = np.load(file, allow_pickle=True)
    r_stars = data["r_stars"] ;  mass_stars = data["mass_stars"]  ; time = data["t"]
    plot_data_profile(ax[i], title[i], r_stars, mass_stars, time, colours, r'$\rm{r}\,\left[ \rm{kpc} \right]$', r'$\rm{M(r)}\,\left[ M_{\odot} \right]$', True, True)
    ax[i].set_xlim(right=1)

plt.tight_layout()
plt.savefig(output_location+"mass_profile_stars_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    mass_file = data_location + data_to_load_2[i] + "mass_profile_data.npz"
    data = np.load(file, allow_pickle=True)
    r_DM= data["r_DM"] ;  mass_DM = data["mass_DM"]  ; time = data["t"]
    plot_data_profile(ax[i], title[i], r_DM, mass_DM, time, colours, r'$\rm{r}\,\left[ \rm{kpc} \right]$', r'$\rm{M(r)}\,\left[ M_{\odot} \right]$', True, True)

plt.tight_layout()
plt.savefig(output_location+"mass_profile_DM_2.pdf", format="pdf", bbox_inches='tight')
plt.close()


#%% Plots of the half light radius--------------------------------------------------------------------------------------------
#Stars
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
title = ["a)", "b)", "c)", "d)", "e)", "f)"]
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "radius_of_half_mass.npz"
    data = np.load(file, allow_pickle=True)
    r_half_stars = data["r_half_stars"] ;  D_r_half_stars = data["D_r_half_stars"]  ; time = data["t"]
    plot_data_fill(ax[array_index], title[i], time, r_half_stars, D_r_half_stars, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$R(M = M_{1/2}) \,\left[ \rm{kpc} \right]$')
    ax[array_index].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"radius_of_half_mass_stars.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "radius_of_half_mass.npz"
    data = np.load(file, allow_pickle=True)
    r_half_DM = data["r_half_DM"] ;  D_r_half_DM = data["D_r_half_DM"]  ; time = data["t"]
    plot_data_fill(ax[array_index], title[i], time, r_half_DM, D_r_half_DM, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$R(M = M_{1/2}) \,\left[ \rm{kpc} \right]$')
    ax[array_index].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"radius_of_half_mass_DM.pdf", format="pdf", bbox_inches='tight')
plt.close()


#%%Now for Hercules and Tucana 2
#Stars
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "radius_of_half_mass.npz"
    data = np.load(file, allow_pickle=True)
    r_half_stars = data["r_half_stars"] ;  D_r_half_stars = data["D_r_half_stars"]  ; time = data["t"]
    plot_data_fill(ax[i], title[i], time, r_half_stars, D_r_half_stars, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$R(M = M_{1/2}) \,\left[ \rm{kpc} \right]$')
    ax[i].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"radius_of_half_mass_stars_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "radius_of_half_mass.npz"
    data = np.load(file, allow_pickle=True)
    r_half_DM = data["r_half_DM"] ;  D_r_half_DM = data["D_r_half_DM"]  ; time = data["t"]
    plot_data_fill(ax[i], title[i], time, r_half_DM, D_r_half_DM, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$R(M = M_{1/2}) \,\left[ \rm{kpc} \right]$')
    ax[i].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"radius_of_half_mass_DM_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Mass within radius r_tilda=0.5 kpc------------------------------------------------------------------------------------
#Stars
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
title = ["a)", "b)", "c)", "d)", "e)", "f)"]
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "mass_at_given_radius.npz"
    data = np.load(file, allow_pickle=True)
    mass_tilde_stars = data["mass_star_stars"] ;  D_m_tilde_stars = data["D_m_star_stars"]  ; time = data["t"]
    plot_data_fill(ax[array_index], title[i], time, mass_tilde_stars, D_m_tilde_stars, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_(r=0.5) \,\left[ M_{\odot} \right]$', ylog=False)
    ax[array_index].set_ylim(bottom=8.5e4)
    
    #Writes in scientific notation with 10^x on top of the plot
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True) 
    formatter.set_powerlimits((-1,1)) 
    ax[array_index].yaxis.set_major_formatter(formatter)

plt.tight_layout()
plt.savefig(output_location+"mass_within_0_5_kpc_stars.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "mass_at_given_radius.npz"
    data = np.load(file, allow_pickle=True)
    mass_tilde_DM = data["mass_star_DM"] ;  D_m_tilde_DM = data["D_m_star_DM"]  ; time = data["t"]
    plot_data_fill(ax[array_index], title[i], time, mass_tilde_DM, D_m_tilde_DM, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_(r=0.5) \,\left[ M_{\odot} \right]$', ylog=False)
    ax[array_index].set_ylim(bottom=1e7)

plt.tight_layout()
plt.savefig(output_location+"mass_within_0_5_kpc_DM.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Now for Hercules and Tucana 2
#Stars
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "mass_at_given_radius.npz"
    data = np.load(file, allow_pickle=True)
    mass_tilde_stars = data["mass_star_stars"] ;  D_m_tilde_stars = data["D_m_star_stars"]  ; time = data["t"]
    plot_data_fill(ax[i], title[i], time, mass_tilde_stars, D_m_tilde_stars, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_(r=0.5) \,\left[ M_{\odot} \right]$', ylog=False)
    ax[i].set_ylim(bottom=8.5e4)
    
    #Writes in scientific notation with 10^x on top of the plot
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True) 
    formatter.set_powerlimits((-1,1)) 
    ax[i].yaxis.set_major_formatter(formatter)

plt.tight_layout()
plt.savefig(output_location+"mass_within_0_5_kpc_stars_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "mass_at_given_radius.npz"
    data = np.load(file, allow_pickle=True)
    mass_tilde_DM = data["mass_star_DM"] ;  D_m_tilde_DM = data["D_m_star_DM"]  ; time = data["t"]
    plot_data_fill(ax[i], title[i], time, mass_tilde_DM, D_m_tilde_DM, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_(r=0.5) \,\left[ M_{\odot} \right]$', ylog=False)
    ax[i].set_ylim(bottom=1e7)

plt.tight_layout()
plt.savefig(output_location+"mass_within_0_5_kpc_DM_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%% Plots of the velocity dispersion----------------------------------------------------------------------------------------
#Stars
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
colours = cm.rainbow(np.linspace(0, 1,n_profile)) #choses n colors in the colormap
title = ["a)", "b)", "c)", "d)", "e)", "f)"]
x1 = [ 0, 0, 0, 0, 0, 0] ; x2 = [0.2, 0.2, 0.2, 0.2, 0.2, 0.2] ; y1 = [4, 4, 4, 4, 4, 4] ; y2 = [6.2, 6.2, 6.2, 6.2, 6.2, 6.2] 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "velocity_dispersion.npz"
    data = np.load(file, allow_pickle=True)
    r_stars = data["r_stars"] ;  sigma_stars = data["sigma_stars"]  ; time = data["t"]
    plot_data_profile(ax[array_index], title[i], r_stars, sigma_stars, time, colours, r'$R \,\left[ \rm{kpc} \right]$', r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$')
    
    # inset
    axins = ax[array_index].inset_axes([0.15, 0.1, 0.4, 0.4]) # X, Y, width, height
    plot_data_profile(axins, "", r_stars, sigma_stars, time, colours, "", "")
    axins.set_xlim(x1[i], x2[i]) ; axins.set_ylim(y1[i], y2[i])
    axins.legend("")    
    ax[array_index].indicate_inset_zoom(axins, edgecolor="black")
    # ax[array_index].set_xlim(right=1)

plt.tight_layout()
plt.savefig(output_location+"velocity_dispersion_stars.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
x1 = [ 0, 0, 0, 0, 0, 0] ; x2 = [2, 1, 4, 4, 4, 4] ; y1 = [5, 5, 5, 5, 5, 5] ; y2 = [10, 15, 15, 15, 15, 15] 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "velocity_dispersion.npz"
    data = np.load(file, allow_pickle=True)
    r_DM= data["r_DM"] ;  sigma_DM = data["sigma_DM"]  ; time = data["t"]
    plot_data_profile(ax[array_index], title[i], r_DM, sigma_DM, time, colours, r'$R \,\left[ \rm{kpc} \right]$', r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$')
    
    # inset
    axins = ax[array_index].inset_axes([0.09, 0.52, 0.4, 0.4]) # X, Y, width, height
    plot_data_profile(axins, "", r_DM, sigma_DM, time, colours, "", "")
    axins.set_xlim(x1[i], x2[i]) ; axins.set_ylim(y1[i], y2[i])
    axins.legend("")    
    ax[array_index].indicate_inset_zoom(axins, edgecolor="black")
    # ax[array_index].set_xlim(right=10)
    
plt.tight_layout()
plt.savefig(output_location+"velocity_dispersion_DM.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Now for Hercules and Tucana 2
#Stars
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
x1 = [ 0, 0] ; x2 = [0.2, 0.2] ; y1 = [4, 4] ; y2 = [6.2, 6.2] 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "velocity_dispersion.npz"
    data = np.load(file, allow_pickle=True)
    r_stars = data["r_stars"] ;  sigma_stars = data["sigma_stars"]  ; time = data["t"]
    plot_data_profile(ax[i], title[i], r_stars, sigma_stars, time, colours, r'$R \,\left[ \rm{kpc} \right]$', r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$')
    
    # inset
    axins = ax[i].inset_axes([0.15, 0.1, 0.4, 0.4]) # X, Y, width, height
    plot_data_profile(axins, "", r_stars, sigma_stars, time, colours, "", "")
    axins.set_xlim(x1[i], x2[i]) ; axins.set_ylim(y1[i], y2[i])
    axins.legend("")    
    ax[i].indicate_inset_zoom(axins, edgecolor="black")

plt.tight_layout()
plt.savefig(output_location+"velocity_dispersion_stars_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

# DM
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
x1 = [ 0, 0] ; x2 = [4,4] ; y1 = [5, 5] ; y2 = [15, 15] 
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "velocity_dispersion.npz"
    data = np.load(file, allow_pickle=True)
    r_DM= data["r_DM"] ;  sigma_DM = data["sigma_DM"]  ; time = data["t"]
    plot_data_profile(ax[i], title[i], r_DM, sigma_DM, time, colours, r'$ R \,\left[ \rm{kpc} \right]$', r'$\sigma_z\,\left[ \rm{km}/\rm{s} \right]$')
    ax[i].legend(loc="upper right")

    # inset
    axins = ax[i].inset_axes([0.09, 0.52, 0.4, 0.4]) # X, Y, width, height
    plot_data_profile(axins, "", r_DM, sigma_DM, time, colours, "", "")
    axins.set_xlim(x1[i], x2[i]) ; axins.set_ylim(y1[i], y2[i])
    axins.legend("")    
    ax[i].indicate_inset_zoom(axins, edgecolor="black")
    # ax[array_index].set_xlim(right=10)
    
plt.tight_layout()
plt.savefig(output_location+"velocity_dispersion_DM_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%% Plots of the Wolf's relation
#Stars
fig, ax = plt.subplots(nrows=1, ncols=2, num=2, figsize=(width, height/3)) 
colours = cm.rainbow(np.linspace(0, 1,n_profile)) #choses n colors in the colormap
title = ["a)", "b)", "c)", "d)", "e)", "f)"]
for i in range(len(data_to_load_2)):
    file = data_location + data_to_load_2[i] + "wolf_relation.npz"
    data = np.load(file, allow_pickle=True)
    M_half_wolf = data["M_half_wolf"] ;  D_M_half_wolf = data["D_M_half_wolf"]  ;  M_dyn = data["M_dyn"] ; time = data["t"]
    ratio = M_half_wolf/M_dyn ; uncertainty = D_M_half_wolf/M_dyn
    plot_data_fill(ax[i], title[i], time, ratio, uncertainty, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_{1/2}/M(R = R_e) $')
    ax[i].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"wolf_relation_2.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Now for Hercules and Tucana 2
#Stars
fig, ax = plt.subplots(nrows=3, ncols=2, num=1, figsize=(width, height)) 
for i in range(len(data_to_load)):
    array_index = np.unravel_index(i, (3, 2)) #converts 1D index to 2D index    
    file = data_location + data_to_load[i] + "wolf_relation.npz"
    data = np.load(file, allow_pickle=True)
    M_half_wolf = data["M_half_wolf"] ;  D_M_half_wolf = data["D_M_half_wolf"]  ;  M_dyn = data["M_dyn"] ; time = data["t"]
    ratio = M_half_wolf/M_dyn ; uncertainty = D_M_half_wolf/M_dyn
    plot_data_fill(ax[array_index], title[i], time, ratio, uncertainty, r'$t \,\left[ 0.98 \, \rm{Gyr} \right]$', r'$M_{1/2}/M(R = R_e) $')
    ax[array_index].set_ylim(bottom=0)

plt.tight_layout()
plt.savefig(output_location+"wolf_relation.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%Images of the simulations
fig, ax = plt.subplots(nrows=6, ncols=4, num=1, figsize=(width, width/4*6)) 
fig.subplots_adjust(wspace=0, hspace=0.05)
imgs_numbers = [125, 250, 375, 500]
data_to_load = [ r"IC=nfw_plummer_8_more_particles/pngs_xz/", "z_2T_orbit/z=30/pngs_xz/", 
                "z_2T_orbit/z=70/pngs_xz/",  "r_theta_2T_orbit/r=70_theta=0.79/pngs_xz/", 
                r"r_theta_2T_orbit/r=70_theta=1.57/pngs_xy/",  r"Bootes_1/pngs_xz/" ]

#Pour les simulations avec angles, prendre un autre point de vue que xz
for i in range(len(data_to_load)):
    for j in range(len(imgs_numbers)):
        picture = data_location + data_to_load [i]+ "0{}_0000-#all-000000.png".format(imgs_numbers[j])
        img = np.asarray(Image.open(picture))
        ax[i, j].imshow(img)
        ax[i, j].axis("off")
        
fig.tight_layout()
plt.savefig(output_location+"simulation_images.pdf", format="pdf", bbox_inches='tight')
plt.close()

#%%For Hercules and Tucana 2
data_to_load_2 = [ r"Hercules/pngs_xz/" , r"Tucana_2/pngs_yz/" ]
fig, ax = plt.subplots(nrows=2, ncols=4, num=2, figsize=(width, width/4*2)) 
fig.subplots_adjust(wspace=0, hspace=0.05)

for i in range(len(data_to_load_2)):
    for j in range(len(imgs_numbers)):
        picture = data_location + data_to_load_2 [i]+ "0{}_0000-#all-000000.png".format(imgs_numbers[j])
        img = np.asarray(Image.open(picture))
        ax[i, j].imshow(img)
        ax[i, j].axis("off")
        
fig.tight_layout()
plt.savefig(output_location+"simulation_images_2.pdf", format="pdf", bbox_inches='tight')
plt.close()













