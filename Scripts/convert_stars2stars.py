#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 09:47:42 2023

@author: darwin
"""

import os
from tqdm import tqdm

import numpy as np
from pNbody import Nbody

#%%
def get_all_snapshot_in_directory(directory, snap_basename):
    """
    Finds all snapshot in the give directory. 

    Parameters
    ----------
    directory : str
        Path to the directory containing the snaphots.
    snap_basename : str
        Basename of the snapshots.
        Examples: snap, snapshot

    Returns
    -------
    files : list of str
        List of all snapshots in the directory.

    """
    if (directory[-1] != "/"):
        directory=directory+"/"
    all_dir_files = os.listdir(directory) #gets all files in the directory
    files = [file for file in all_dir_files if file.endswith(".hdf5")] #finds all .hdf5 files
    files = [directory+file for file in files if file.startswith(snap_basename)] #keeps only the files corresponding to snapshots
    files.sort()
    return files

#%%
#Here the ids are ordered st the stars are the 800 last particles
nb_init = Nbody("Tucana_2_launched_same_ID.hdf5")
stars_id = nb_init.num[-801:-1]
files = get_all_snapshot_in_directory("snap_corrected", "snapshot")

for file in tqdm(files):
    nb = Nbody(file)
        
    #Search the stars indices
    orig_indices = nb.num.argsort() #argsort does not change the order of num    
    indices = orig_indices[np.searchsorted(nb.num[orig_indices], stars_id)]
    
    #Give the stars the correct type number
    nb.tpe[indices] = 4*np.ones(indices.shape)
    nb.npart[1] -= 800 #Subtract the number of halo particles
    nb.npart[4] = 800 #Set the number of stars
        
    #Write the data
    nb.write()
