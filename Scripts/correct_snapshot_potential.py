#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 22 20:02:29 2023

@author: Darwin Roduit

This module centers the simulation either on the potential minimum or to the density maximum. 
If the potcenter method is chosen, corrects the potential of Swift simulations by computing the 
potential with pNbody Tree code and centers the simulation on the minimum of this potential. 

Beware that it may take time. 
"""

import multiprocessing as mp
import os
import argparse
import numpy as np
from tqdm import tqdm
from pNbody import Nbody

from argparse_utils import RawTextArgumentDefaultsHelpFormatter, store_as_list

#%%Functions

def write_one_new_snap(file, out_file, epsilon, method, n_bins, n_iter, overwrite, select_particle):
    if not os.path.exists(out_file) or overwrite: 
        if not os.path.exists(file):
            print("Error: File {} does not exist.".format(file))
        
        if select_particle == "halo":
            nb = Nbody(file, ptypes=[1])
        elif select_particle == "stars":
            nb = Nbody(file, ptypes=[4])
        else:
            nb = Nbody(file)
            
        if method=="potcenter":
            try:
                del nb.pot
                nb.potcenter(eps=epsilon)
            except:
                nb.potcenter(eps=epsilon)
        else: # method == "histocenter"
            r_max = 2.0*np.max(nb.rxyz())
            for i in range(n_iter): #Essaie d'affiner le centrage par itération
                nb.histocenter(rbox=r_max, nb=n_bins)
                r_max = r_max/2.0
        nb.rename(out_file)
        nb.write()

def write_new_snap(files, out_files, epsilon, method, n_bins, n_iter, overwrite, select_particle, do_parallel):
    """
    This function uses nb.potcenter() to generate snapshots with the potential 
    computed by pNbody.

    Parameters
    ----------
    files : list of files
        Snapshots that must be corrected.
    out_files : list of files
        Output files into which the new snapshots are saved.
    epsilon : float
        Smoothing_length.
    overwrite : bool
        True: overwrites existing files ; False: does not overwrite existing files.
    select_particle: str
        Select a particle type if not None

    Returns
    -------
    None.

    """
    
    if not do_parallel:
        for file,out_file in zip(tqdm(files), out_files):
            write_one_new_snap(file, out_file, epsilon, method, n_bins, n_iter, overwrite, select_particle)
    else:
        async_result_array = np.zeros(len(files), dtype=object)        
        # Bug fix : maxtasksperchild=1 avoids memory problems (worker are killed when they finish
        #their work and new ones are spawned)
        with mp.Pool(maxtasksperchild=1) as pool:
            #Submit the jobs
            print("Sumbit the jobs")
            for i in tqdm(range(len(files))):
                file = files[i] ; out_file = out_files[i]
                async_result_array[i] = pool.apply_async(write_one_new_snap, args=(file, out_file, epsilon, method, n_bins, n_iter, overwrite, select_particle), callback=lambda x: print("One task done."))
            print("Waiting for the tasks to complete...")
            results = [ar.get() for ar in tqdm(async_result_array)]
    return 


#%%
#Creates a parser object
def parse_options():
    parser = argparse.ArgumentParser(formatter_class=RawTextArgumentDefaultsHelpFormatter, 
                                     description="Centers the snapshots on the " 
            "minimum of the potential computed by pNbody and saves the output.", 
            epilog = "Example\n----------------\n \n"
            'An example with folder is:\n"python3 ./correct_snapshot_potential.py '
            '--folder="/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/" '
            '--snap_basename="snapshot" --out_folder="/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap_corrected"'
            '\n \nAn example with files is: \npython3 ./correct_snapshot_potential.py '
            '--files "/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0000.hdf5" '
            '"/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0001.hdf5"'
            '"/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0002.hdf5" '
            '--out_files "/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0000_corrected.hdf5" '
            '"/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0001_corrected.hdf5" '
            '"/home/darwin/Bureau/TP4b/Simulations/z_2T_orbit/z=290/snap/snapshot_0002_corrected.hdf5"')
    
    #Either give a list of files
    parser.add_argument('--files', action=store_as_list, default=None, type=str,
                        nargs="*", help="Snapshots that must be corrected.")
    parser.add_argument('--out_files', action=store_as_list, default=None, type=str,
                        nargs="*", help="Output files into which the new snapshots are saved.")

    #Or a folder and the basename
    parser.add_argument('--folder', action="store", default=None, type=str,
                        help="Folder containing snapshots that must be corrected.")
    parser.add_argument('--out_folder', action="store", default=None, type=str,
                        help="Output folder into which the new snapshots are saved.")
    parser.add_argument('--snap_basename', action="store", default="snapshot", 
                        help="Base name of the snapshots.")

    #Method to center
    parser.add_argument('--method', default="potcenter", type=str, choices=["potcenter", "histocenter"],
                        help='Method to center the particles. Advice : If you select the stars, use potencter. \n')
    parser.add_argument('--n_bins', default=500, type=int, help="Number of bins to use for the "
                        "histocenter method. ")
    parser.add_argument('--n_iter', default=10, type=int, help="Number of iteration for convergence of "
                        "histocenter method. ")
    parser.add_argument('--epsilon', default=0.025, type=float, 
                        help='Gravitational softening length for potcenter method.')
    
    #Other options
    parser.add_argument('--overwrite', default=False, action="store_true",
                        help='True: overwrites existing files ; False: does not overwrite existing files.')
    parser.add_argument('--do_parallel', default=False, action="store_true",
                        help='True: do computations in parallel ; False: Do not compuations in parallel.')
    parser.add_argument('--select_particle', default=None, type=str, choices=[None, "stars", "halo"], 
                                            help="Select a particle type")
    

    args = parser.parse_args()
    return args

#Does the job
def correct_snapshot(args):
    print("---------------------------------------\nStarting correcting the potential of the snapshots...\n")
    files = []
    out_files = [] 
    if args.files is not None:
        files = args.files
        out_files = args.out_files
        # write_new_snap(args.files, args.out_files, args.epsilon, args.overwrite, args.n_bins, args.n_iter, args.overwrite)
    else: 
        #Creates the output directory
        if not (os.path.exists(args.out_folder)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(args.out_folder)
        
        #Adds the "/" at the end of the folder path if it is absent
        if args.out_folder[-1] != "/":
            args.out_folder=args.out_folder+"/"
            
        if args.folder[-1] != "/":
            args.folder=args.folder+"/"
            
        #Get all snapshots in the given directory
        all_dir_files = os.listdir(args.folder) #gets all files in the directory
        files = [file for file in all_dir_files if file.endswith(".hdf5")] #finds all .hdf5 files
        files = [file for file in files if file.startswith(args.snap_basename)] #keeps only the files corresponding to snapshots
        files.sort() #sorts the snapshots to process them in the ordre
        
        #Adds the path to the files and out_files
        out_files = list(files)
        
        for i,file in enumerate(files):
            files[i] = args.folder + file
            out_files[i] = args.out_folder + file

        # write_new_snap(files, out_files, args.epsilon, args.method, args.n_bins, args.n_iter, args.overwrite, args.select_particle)
    
    #Now write the files    
    write_new_snap(files, out_files, args.epsilon, args.method, args.n_bins, args.n_iter, args.overwrite, args.select_particle, args.do_parallel)
    print("End----\n")
    
if __name__ == '__main__':
    args = parse_options()
    mp.set_start_method("forkserver")
    correct_snapshot(args)
