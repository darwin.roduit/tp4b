#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 21:33:06 2023

@author: darwin
"""

import numpy as np
from astropy import units as u
import astropy.coordinates as coord
# import gala.potential as gp
# import gala.dynamics as gd
# from gala.dynamics import mockstream as ms
# import gala.coordinates as gc
# from gala.dynamics.nbody import DirectNBody
# from gala.units import galactic, UnitSystem
_ = coord.galactocentric_frame_defaults.set("v4.0")
   

def compute_distance(distance_modulus):
    return 10**(distance_modulus/5.0 + 1)


#Gaia (ICRS) coordinates                                     
ra_particles = 143.88670 #alpha
dec_particles = -36.767330 #beta
distance_modulus = 20.60 
dist_particles = compute_distance(distance_modulus)*u.pc 
dist_particles = dist_particles.to(u.kpc).value
v_los = 290 ; v_particles = v_los 
pmra_particles = -0.10 #mu_alpha
pmdec_particles = 0.09 #mu_delta


ics = coord.SkyCoord(ra=ra_particles*u.degree, dec=dec_particles*u.degree,
                          distance=dist_particles*u.kpc,
                          pm_ra_cosdec=pmra_particles*u.mas/u.yr,
                          pm_dec=pmdec_particles*u.mas/u.yr,
                          radial_velocity=v_particles*u.km/u.s)
ics = ics.transform_to(coord.Galactocentric)
position = ics.data.get_xyz()
velocity = ics.velocity.get_d_xyz()
print(position)
print(velocity)


#%%
print("----------------")
distance_modulus = np.array([21.31,  18.13, 20.81, 20.33, 16.39, 19.26, 16.88, 
                             19.53, 16.67, 22.80, 19.8, 20.72, 20.52, 18.70, 
                             20.68, 19.50, 19.46, 20.89, 17.20, 20.10, 21.65, 
                             22.15, 21.68, 20.94, 21.25, 23.06, 21.66, 23.06, 
                             19.6, 20.30, 18.30, 21.31, 17.50, 19.81, 19.2, 19.62, 
                             16.80, 17.68, 19.64, 17.27, 18.8, 16.8, 18.36, 18.7, 
                             19.94, 17.70, 19.41, 19.80, 17.90, 24.28, 24.40, 23.36, 
                             24.40, 24.85, 25.14, 25.61, 25.57, 25.77, 24.8, 25.8])
dist_particles = compute_distance(distance_modulus)*u.pc 
dist_particles = dist_particles.to(u.kpc).value
dist_particles = np.sort(dist_particles)
print(dist_particles)