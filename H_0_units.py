#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 21:39:11 2023

@author: darwin
"""

import numpy as np
from astropy import units as u
from astropy.constants import G as G_a

unit_length = 1*u.kpc
unit_mass = 1e10*u.M_sun ; unit_mass = unit_mass.to(unit_mass)
unit_velocity = 1e5*u.cm/u.s
unit_time = unit_length.to(u.cm)/unit_velocity
unit_time_2 =  (1e3*unit_length).to(u.cm)/unit_velocity
print(unit_time)
print(unit_time_2)

G = G_a.to(unit_length**3 * unit_mass**(-1) * unit_time**(-2))

#NFW parameters
r_s = 16*unit_length ; M_200 = 147.4103154277408*unit_mass  ;  r_200 = 157.1744550043975*unit_length ; c = r_200/r_s    
log_c200_term = np.log(1 + c) - c / (1. + c)
rho_0 = M_200 / (4 * np.pi * r_s**3 * log_c200_term) ; rho_0 = rho_0.to(unit_mass/unit_length**3)    
rho_c = M_200/(200*4/3*np.pi*r_200**3)
H_0 = np.sqrt(8*np.pi*G/3 * rho_c).to(1/unit_time)

print(H_0.to(u.km/u.s/u.kpc))
print(H_0.to(1/unit_time_2))
print(H_0.to(u.km/u.s/u.Mpc))
