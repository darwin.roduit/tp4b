% !TeX encoding = UTF-8
% !TeX program = xelatex
% !TeX spellcheck = en_US
% !TeX root = report_tp4b.tex

\documentclass[tradiabstract]{styles/aa}
%\documentclass{styles/aa}
\usepackage{styles/preambule_general}
\usepackage{styles/labos_styles}

\usepackage{graphicx}
\usepackage{txfonts}
\usepackage{lmodern} %Change la fonte (faut l'ajouter à chaque fois parce que je ne sais guère pourquoi sinon il ne charge pas...)

\begin{document}
   \title{$N$-body simulations of the tidal stripping of ultra-faint dwarf galaxies}
%   \subtitle{Large scale structure of the universe in absence of baryonic matter}
   \author{D. Roduit}
   \institute{Laboratory of Astrophysics, EPFL}
   \date{\today}

   
   
	\abstract{%Context :
		Observations of Milky-Way (MW) dwarf galaxies show that some of them may have been affected by tidal stripping. This is a consequence of the gravitational interaction between the dwarf and the MW. The interaction can lead to deformation or even disruption of these satellites. Since they are not in equilibrium, the Virial theorem cannot be used anymore. Thus,$N$-body simulations must be run. Because of the aforementioned effects, tidal stripping plays an important role in the interpretation of future galaxy surveys. Indeed, the study of such systems provides information on their dark matter halos, which then allows to put constraints on cosmological models. 
		%Aims :
		The purpose of this work is to investigate the interaction of a spheroidal dwarf galaxy with the MW and the effects of this interaction on the dwarf galaxy. We are also interested in the fraction of galaxies that will survive and those that will suffer complete destruction. 
%		Methods : 
		To this end, we rely on the idealized case in which a single dwarf galaxy evolves in the static potential of the MW. The dwarf galaxy is modeled by a NFW dark matter halo and a plummer distribution for the stars. We rely on $N$-body simulations with the \texttt{swift} code with gravitational forces only. First, circular orbits are considered and then, realistic cases with Bootes $1$, Hercules and Tucana $2$ are simulated. 
%		Resuts : 
		It has been found that the NFW profile is higly resistant to tides, always leaving a remnant in the simulations that were carried out. As a consequence, the stars undergo a small expansion, but no disruption. 
%		Conclusions : 
		Simulations with different initial conditions for dwarf galaxies must be run to determine the initial conditions that will eventually disrupt the galaxy. }

	\keywords{dwarf galaxy, UFD, cold dark matter, $N$-body simulation, collisionless simulation, dark matter, swift, tidal stripping}

	\maketitle
	
%
%-------------------------------------------------------------------
\section{Introduction}

Ultra faint dwarf galaxies have been discovered a century ago by \citet{shapley_two_1938}. In the following decades, more dwarf galaxies have been discovered \citep{Reaves_1956}. They were classified in different categories such as dwarf spheroidal galaxies (DSph) or dwarf ellipticals (DEl) \citep{van_den_bergh_1959}. Recent surveys, for example the Sloan Digital Sky Survey \citep{Martin_2008a}, have allowed to find more dwarfs of the Milky Way (MW) and more fainter ones, which are now called ultra-faint dwarf galaxies (UFDs).  \\
In the last two decades, dwarf galaxies properties have be probed. It has been showed that such galaxies are dark matter dominated \citep{Kleyna_2005, Martin_2007}. Moreover, simulations performed in the Lambda cold dark matter ($\Lambda$CDM) cosmological model, which is a widely accepted framework for actual cosmology (see e.g. the thorough discussion in \citet{Peebles_LCDM}), illustrates that dark matter structures organize in halos, sub halos and filaments. Those halos and sub halos can have a wide range of masses, but they all follow the same mass profile : the NFW profile. \\
Many observations have identified effects of tidal stripping on dwarf galaxies (e.g. \citet{Sand_2009, Longeard_2022}). This is a consequence of the interactions of the dwarf with the Milky Way. The gravitational interaction of such system may give rise to disruption, elongation, deformation and changes in velocity dispersion of the stellar component of the dwarf and its dark matter halo. In such context, the Virial theorem cannot be used anymore to study the properties of the dark matter. Thus, $N$-body simulations must be run. \\
Moreover, due to the aforementioned effects, the process of tidal stripping plays an important role in the interpretation of future galaxy surveys and spectroscopic surveys of particular UFDs. The study of such system permits to gain knowledge on their halos, which can be used to determine the right cosmological model by constraining dark matter models. \\
However, the process of tidal stripping is complex. It has been studied in cosmological simulations (e.g. \citet{Sawala_2016, Sawala_2017,Garrison_Kimmel_2017}), but also in more idealized cases in which one considers only a dwarf galaxy interacting with its host galaxy (e. g. \citet{errani_2020, errani2023, van_den_Bosch_2017}). 
In this work, we rely on the idealized situation in which a single $N$-body dwarf galaxy evolves in the static potential of the host galaxy, the Milky Way. We want to investigate how the dwarf galaxy is affected by the interactions with the MW. Particularly, we are interested in which extent and under which conditions the stars are perturbed by the tides. We are also interested in the fraction of galaxies that will survive and those that will suffer complete destruction. In the latter case, we want to determine whether they end up in galactic streams or not. \\
We start in section \ref{subsec:IC} with a description of the hypotheses and initial conditions for the dwarf galaxies. In section \ref{subsec:MW_model}, we detail the potential that models the MW. Then, section \ref{subsec:n_body_code} presents \texttt{swift}, the code used to perform the simulations ; we also detail the choice of the softening length for our collisionless simulations. Section \ref{subsec:simulations} explains the types of simulations that were run. Section \ref{sec:data_analysis} presents the observables that were studied on the previous simulations. Finally, section \ref{sec:results_discussion} contains our results and section \ref{sec:conclusion} summarizes our findings and gives some future directions. 

\section{Hypotheses and numerical approach}\label{sec:hypotheses_num_approach}

	\subsection{Hypotheses and initial conditions}\label{subsec:IC}
	
	In this paper, we model dwarf spheroid galaxies with two components in equilibrium: stars and a dark matter halo, where only gravitational forces are taken into account. Moreover, we assume the system to be ergodic and collisionless. So, stars follow a plummer model whose potential is:
%	
		\begin{equation}
			\Phi(r) = - \frac{G M}{\sqrt{r^2 + b_p^2}} \; , \label{eq:plummer_pot}
		\end{equation}
%		
	where $r$ is the radial distance in spherical coordinates, $G$ is the gravitational constant, M is the total mass of the stellar part of the dwarf galaxy and $b$ is plummer scale length. Whereas the halo is modeled by a NFW density profile, given by: 
%	
		\begin{equation}
			\rho(r) = \cfrac{\rho_0}{\cfrac{r}{r_s} \left( 1 + \cfrac{r}{r_s} \right)^2} \; , \label{eq:NFW_profile}
		\end{equation}
%		
	where $\rho_0$ is the scale density and $r_s$ is the scale radius. \\
	Numerically, stars and dark matter are modeled by particles of the same mass. Therefore, one needs to randomly sample the positions and velocities of those particles to generate the dwarf galaxy. This is accomplished by a numerical implementation of Eddington's formula given by eq. (4.46b) p. 289 in \citep{binney_galactic_2008} that determines the distribution function. Then, particles can be sampled from the latter. The implementation is based on \citet{errani_2020}.
%	and \url{https://github.com/rerrani/nbopy}. 
	
	In this work, all simulations were carried with the same dwarf galaxy at equilibrium. The parameters are $\rho_0 = 0.007 \cdot 10^{10}$ \unit{\Msun \per \kpc}, $r_s = 0.5$ \unit{\kpc}, $M = 10^5$ \unit{\Msun}, $b_p = 0.1$ \unit{\kpc}. 
	The particles have an individual mass of $m_{\text{part}} = 125$ \unit{\Msun} and the total number of them is $N = \num{2751687}$.

	\subsection{Milky Way model} \label{subsec:MW_model}
	
	The Milky Way is modeled by \citet{bovy2015} \texttt{MWPotential2014} potential. This potential is composed by a bulge modeled by a power spherical law with cut-off, a Miyamoto-Nagai (MN) disk and a NFW dark matter halo. It can be described in cylindrical coordinates $(R, \, \theta, \, z)$ by the following: 
%	
		\begin{equation}
			\Phi_{\text{MW}}(R, z) = f_h \Phi_{\text{NFW}} + f_d \Phi_{\text{MN}} + f_b \Phi_{\text{bulge}} \;, \label{eq:MW_pot}
		\end{equation}
		
	where $f_h$, $f_d$ and $f_b$ are three coefficients that adjust the strength of each individual component. $\Phi_{\text{NFW}}$ is the potential derived from equation \eqref{eq:NFW_profile}. The Miyamoto-Nagai potential is:
	
%
		\begin{equation}
			\Phi_{\text{MN}}(R, z) = - \frac{G M_{\text{disk}}}{\sqrt{R^2 + \left(a + \sqrt{z^2 + b^2} \right)^2}} \; ,
		\end{equation}
%		
	with $M_{\text{disk}}$ the mass of the disk, $a$ and $b$ two parameters of the model. The bulge potential is modeled by the density: 
%	
		\begin{equation}
			\rho_{\text{bulge}}(r) = A \qty(\frac{r_1}{r})^\alpha \exp(-\frac{r^2}{r_c^2}) \; .
		\end{equation}	 
%		
	In the density, $A$ is an amplitude, $r_1$ a reference radius for amplitude, $\alpha$ is the inner power and $r_c$ is the cut-off radius. 
	Since the NFW and the Miyamoto-Nagai are well-known potentials, their properties can be found easily, e.g. in \citet{binney_galactic_2008}. For the power spherical density with cut-off, here are provided the mass inside a radius $r$ and the potential:
%	
		\begin{align}
			M(r) &= 2 \pi A r_1^\alpha r_c^{3 - \alpha} \, \gamma \left( \frac{3 - \alpha}{2}, \, \frac{r^2}{r_c^2} \right) \; , \\
			\Phi(r) &= - \frac{G M(r)}{r} - 2 \pi G A r_1^\alpha r_c^{2 - \alpha} \, \Gamma \left( \frac{2 - \alpha}{2}, \, \frac{r^2}{r_c^2} \right) \; ,
		\end{align}
%
	where $\gamma(s, \, x)$ is the lower incomplete gamma function and $\Gamma(s, \, x)$ is the upper incomplete gamma function. All other properties can be deduced from those equations. 
	
	\begin{figure*}
	%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/MWPotential_2014_fit_parameters_f}
		\caption{Least square fit on \texttt{galpy}'s \texttt{MWPotential2014} rotation curve (left) to obtain the values of the coefficients $f_h$, $f_d$ and $f_b$ of equation \eqref{eq:MW_pot}. The relative error is provided in the right figure.}
		\label{fig:MWPotential_2014_fit_parameters_f}
	\end{figure*}
	
	\begin{figure*}
		\hspace{-0.5cm}
		\includegraphics[scale=0.7]{figures/MWPotential2014_potential_density_plot}
		\caption{Illustration of \texttt{MWPotential2014} potential (left) and density (right). }
		\label{fig:MWPotential2014_potential_density_plot}
	\end{figure*}
	
	One thing to notice is that \texttt{galpy} normalizes the potential in its own way. As a result, to find the parameters $f_h$, $f_d$ and $f_b$, a least square fit has been made on the rotation curve at $z=0$, which is shown on the left figure \ref{fig:MWPotential_2014_fit_parameters_f} with the relative error on the right. For completeness, all parameters of the model have been summarized in table \ref{tab:MWPotential2014_parameters}, as well as some additional properties of the NFW halo. The potential and the density are presented on figure \ref{fig:MWPotential2014_potential_density_plot}.
	
	\begin{table}[]
		\centering
		\begin{tabular}{@{}ll@{}}
			\toprule
			Parameters &  \\ \midrule
			$\alpha$ & $1.80$ \\
			$r_c$ $\unite{\kpc}$ & $1.90$ \\
			$A$ & $1.00$ \\
			$r_1$ $\unite{\kpc}$ & $1.00$  \\ 
			      &  \\ 
 			$M_{\text{disk}}$ $\unite[10^{10} \;]{\Msun}$ & $6.80$ \\
			$a$ $\unite{\kpc}$ & $3.00$ \\
			$b$ $\unite{\kpc}$ & $0.28$ \\
			         &  \\
			$r_s$ $\unite{\kpc}$ & $16.00$ \\
			$\rho_0$ $\unite[10^{10} \;]{\Msun \per \kpc}$ & $1.94 \cdot 10^{-3}$ \\
			$r_{200}$ $\unite{\kpc}$ & $157.17$ \\
			$M_{200}$ $\unite[10^{10} \;]{\Msun}$ & $147.41$ \\
			$c$ & $9.8$ \\
			$\rho_c$ $\unite[10^{10} \;]{\Msun \per \kpc}$ & $4.53 \cdot 10^{-8}$  \\
			$H$ $\unite{(\unitTime)^{-1}}$ & $0.128$ \\
			         &  \\ 
			$f_b$     & $0.02$ \\
			$f_d$       & $1.00$ \\
			$f_h$       & $0.44$ \\ \bottomrule
		\end{tabular}
		\caption{Parameters and properties of \texttt{MWPotential2014}. }
		\label{tab:MWPotential2014_parameters}
	\end{table}
	
	\begin{figure}
		\hspace{-0.5cm}
		\includegraphics[scale=0.7]{figures/a_analytical_vs_a_numerical}
		\caption{Comparison or the numerical acceleration $a_{\text{num}}$ with the theoretical acceleration $a_{\text{th}}$ from equation \eqref{eq:acc_theoretical} in simulations with different softening length $\varepsilonup$.}
		\label{fig:a_analytical_vs_a_numerical}
	\end{figure}
	
	\subsection{$N$-body code} \label{subsec:n_body_code}
	
	We use \texttt{swift} \citep{swift_code} to perform the $N$-body simulations. This code allows to use an external potential in which the particles are evolving. Therefore, \texttt{MWPotential2014} has been implemented so that the particles can evolve in it. 
	
	Since the system is assumed collisionless, before running simulations one must determine the softening length $\varepsilon$ that softens the gravity force when two particles are in close contact. In order to do this, five low resolution simulations (eight times less particles) with $\varepsilon \in \left\lbrace  0.0125, \, 0.02, \, 0.025, \, 0.05, \, 0.1 \right\rbrace $ \unit{\kpc} have been carried out without the external potential. To determine the best softening length, one compares the numerical acceleration norm $a_{\text{num}}$ with the theoretical acceleration norm $a_{\text{th}}$. The latter is the sum of the accelerations due to the plummer model of the stars and the NFW dark matter halo:
%		
		\begin{equation}
			a_{\text{th}} = \frac{G M r}{\qty(r^2 + b^2)^{3/2}} + 4 \pi G \rho_0 r_s^2 \abs{\cfrac{1}{r\qty(1 + \frac{r}{r_s}) } - \frac{r_s}{r^2} \log(1 + \frac{r}{r_s})} \,. \label{eq:acc_theoretical}
		\end{equation}
% 
	The resulting plot is presented on figure \ref{fig:a_analytical_vs_a_numerical}. As one can see, the cases with $\varepsilon = \left\lbrace 0.05, \, 0.1 \right\rbrace$ \unit{\kpc} provide a high softening, which breaks the linear regime  between the two accelerations around $a_{\text{th}} = 0.010$ \unit{\kpc \per (\unitTime) \squared}.  The simulation with $\varepsilon = 0.0125$ \unit{\kpc} breaks the linear regime the farthest but it was slow : it took  more than two hours to complete whereas the two simulations with $\varepsilon = \left\lbrace 0.02, \, 0.025 \right\rbrace$ \unit{\kpc} completed within one hour and a half. These two simulations break the linear regime almost at the same time, so one could choose one of them. For this work, $\varepsilon = 0.025$ \unit{\kpc} was chosen. Hence, for the high resolution simulations, $\varepsilon = 0.0125$ \unit{\kpc} was used.

	\subsection{Simulations}\label{subsec:simulations}
	
	In this work, only circular orbits were considered. Nevertheless, two orbital parameters were varied and analyzed : the distance from the center of the Milky Way and the inclination ($\theta$ in spherical coordinates) with respect to the $xz$-plane. All of those simulations are performed over a time range of two orbital periods.\\
	For the first type of simulations, the dwarf galaxies were placed at $(x, \, y,\, z) = (0, \, 0 ,\, z)$ and $z$ took the values $z \in \left\lbrace 30, \, 70, \, 110, \, 130 \right\rbrace$. Those simulations correspond to $\theta = \pi / 2$, i.e. the dwarf galaxy evolves in a plane perpendicular to the galactic plane.  \\
	For the second simulations, the radial distance $r$ was fixed to $r=70$ and two angles were chosen, $\theta \in \left\lbrace \pi/4 , \, \pi \right\rbrace$. The case $\theta = \pi$ corresponds to dwarf galaxies evolving in the galactic plane. \\
	Finally, in order to study more realistic cases, simulations with some Milky Way dwarf galaxies were done. We proceeded as follow : (1) the proper motion of Bootes $1$, Hercules and Tucana 2 were taken from \citep{battaglia2022}; (2) their orbits were integrated backward in \texttt{swift}, with one particle each, to obtain the positions and velocities at their respective apocenter; (3) the ICs given in \ref{subsec:IC} were used to launch these dwarfs in orbit of the Milky Way. In this case, the backward integration was performed over $6.5 \cdot$ \unit{\unitTime} to obtain the apocenter of Tucana 2 and Hercules. For Bootes $1$, the first apocenter was chosen. Then, the simulations were done during the same amount of time. The orbits of those dwarfs are illustrated on figure \ref{fig:gaia_orbits}. For those simulations, the apocenter and the pericenter found are given in table \ref{tab:apo_peri_centers_dg}.
	
	\begin{table}[]
		\centering
		\begin{tabular}{@{}llll@{}}
			\toprule
			& Bootes $1$ & Hercules & Tucana $2$ \\ \midrule
			Apocentre $\unite{\kpc}$ & $101.56$ & $414.99$ & $415.81$   \\
			Pericentre $\unite{\kpc}$ & $42.56$ & $67.98$ & $53.77$ \\ \bottomrule
		\end{tabular}
		\caption{Apocentre and pericenter of Bootes $1$, Hercules and Tucana $2$ found by backward integration of the proper motion from \citep{battaglia2022}. }
		\label{tab:apo_peri_centers_dg}
	\end{table}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.6]{figures/gaia_orbits}
		\caption{Orbits of the backward integration of Bootes $1$, Hercules and Tucana $2$. The dwarf galaxies are then launched from their apocentre and evolve for $6.5$ \unit{\unitTime}.}
		\label{fig:gaia_orbits}
	\end{figure*}
	
	
\section{Data analysis}\label{sec:data_analysis}

		First of all, for all the data analysis of this work, \texttt{pNbody}\footnote{\url{https://obswww.unige.ch/~revaz/pNbody/}} was used. The latter allows to manipulate and analyze large $N$-body simulations. Then, to properly analyze the data, one goes into the frame of the dwarf galaxy. This is done by centering on the minimum of the potential. To compute the potential due only to the interaction between the particles, without the external potential, a tree code is used. However, this tree code may fail if two particles are too close to each other. In this case, the simulations are centered on the maximum of the density. The latter is performed by binning the particles into a box. To provide better accuracy, the centering procedure is iterative with a box twice smaller at each iteration. In this work, ten iterations were used. 
		
		To study the tidal stripping of dwarf galaxies, one starts by determining the spherical mass profile for the stars and the dark matter halo at different times of the simulation. To better illustrate the evolution, eleven snapshots, including the snapshot at $t=0$ \unit{\unitTime}, were plotted. \\
		The second observable studied is the $2$D projected half-light radius $R_e$. To estimate it, the procedure was the following:
%		
		\begin{enumerate}
			\item Compute the half mass $M_{1/2}$ of the system for the first snapshot. Since the mass is conserved, it will be constant all long. 
			\item For each snapshot, find the radius $r$ at which at which $M(r) = M_{1/2}$. Repeat this step for multiple random lines of sight. Take $R_e$ to be the average value.
		\end{enumerate}
%	
		This has been done for the stars and dark matter as well. \\
		The third observable is the mass $\tilde{M} \equiv M(r = \tilde{r})$ within a chosen radius $\tilde{r} = 0.5$ \unit{\kpc}. This time, this has been performed in $3$D, so no averaging over random lines of sight were needed. The value of $\tilde{r}$ is small enough to study if the stars go in or out this radius. \\
		The fourth observable is the velocity dispersion. It has also been averaged over multiple random lines of sight. Eleven snapshots were chosen, the same ones as for the spherical mass profile.  \\
		Finally, the last observable is the the mass enclosed within the $3$D deprojected half-light radius $r_{1/2}$ using a relation that is named Wolf's equation as it is showed in \citep{wolf2010}. Wolf's equation reads:
%		
			\begin{equation}
				M_{1/2} = 4 G^{-1} \expval{\sigma_{\text{los}}^2} R_e \; , \label{eq:wolf_equation}
			\end{equation}
%	
		where $\sigma_{\text{los}}$ is the line of sight velocity dispersion, the brackets mean that we shall take the luminosity-weighted average. The half-light mass computed within $R_e$ with Wolf relation is compared with the actual mass of the dwarf galaxy within $R_e$, i.e. $M(R = R_e)$.\\
		Note that for all the previous observables, the stars and the dark matter were treated separately. However, to compute $M_{1/2}$ with Wolf's relation, only the stellar component of the dwarf galaxies is used. 
	
\section{Results and discussion} \label{sec:results_discussion}
	
	To begin with, the results of one type of data for all simulations, except for Hercules and Tucana $2$ and the simulations with $z=130, 170$, are presented together on the same figures to facilitate the comparisons. The cases of Hercules and Tucana $2$ present some peculiarity, which will be discussed later in \ref{subsec:hercules_tucana}. That is justified to separate them from the rest. For the simulations with $z=130, 170$, they present similar results as for $z=30, 70$, with less evolution as it will be explained below. \\
	The mass profiles are presented on figure \ref{fig:mass_profile_stars} for the stars and on figure \ref{fig:mass_profile_DM} for the DM. The results for the half-light radius are given on figures \ref{fig:radius_of_half_mass_stars} and \ref{fig:radius_of_half_mass_DM} ; the results for the mass within $\tilde{r} = 0.5$ \unit{kpc} are given on figure \ref{fig:mass_within_0_5_kpc_stars} and \ref{fig:mass_within_0_5_kpc_DM}. Then, the velocity dispersion for the stars and the halo are show on figure \ref{fig:velocity_dispersion_stars} and \ref{fig:velocity_dispersion_DM}. Finally, the results for Wolf's equation are presented on figure \ref{fig:wolf_relation}. We also provide images of the simulations to show how it is affected by the interaction with the external potential. They are shown on figure \ref{fig:simulation_images}. \\
	
	At first, let us comment the isolated case. The figures clearly show that the dwarf galaxy does not evolve : the mass profile remains almost the same, the half-light radius does not increase nor decrease significantly but fluctuates randomly. Similarly, the velocity dispersion curves do not change drastically and Wolf's equation also remains constant. Therefore, it can be stated that the isolated case is in equilibrium. \\
	
	Now, the other simulations can be analyzed. On figures \ref{fig:mass_profile_stars} and \ref{fig:radius_of_half_mass_stars}, one clearly sees that the stars are slightly expanding, which translates by a diminishing mass within $M(\tilde{r})$ as visible on figure \ref{fig:mass_within_0_5_kpc_stars} and by an increasing Wolf's equation on figure \ref{fig:wolf_relation}. Notice that this equation describes well our system since the ratio $M_{1/2}/M(R = R_e)$ is close to $1$. \\
	The simulation with $z=30$ is the one that has its stars changing the most. Since it is the closest simulation, this was expected. However, this change is rather small. The stars do not tear up and the dwarf does not get destroyed. Well, as one can see on the figures and more specifically on figure \ref{fig:simulation_images}, the dark matter halo undergo a violent change in shape : it gets filamented, but there still exists a spherical core. This is confirmed on the figure of the dark matter profile \ref{fig:mass_profile_DM}. The core resists the tidal stripping of the Milky Way even in this extreme simulation where the halo, large as far as $r=30$ \unit{\kpc}, reaches the center of the Milky Way. This is confirmed by the plots of the velocity dispersion of the halo, which remains similar to its core. \\
	For the simulations with either $z=70$ or $r=70$ and two different angles, the above analysis applies, except that this time the halo does not reach the center of the Milky Way. Since the dwarf is farther in those simulations, the effect is smaller, which was expected. The conclusion is that the farther the dwarf with circular orbit, the lower the effect of the tidal stripping. The simulations confirm this. Moreover, the angle at which the dwarf orbits the Milky Way does seem to affect much the tidal stripping, since the plots are similar to each others. Indeed, the potential used is almost spherical at high distances. The left figure in \ref{fig:MWPotential2014_potential_density_plot} illustrates this fact. \\
	Another point towards the low effect of the tidal stripping on the dwarf galaxies is their velocity dispersion. For the stellar part, the velocity dispersion changes slightly as the stars evolve and expand slowly. None of the simulations with circular orbits show drastic changes in the velocity dispersion. Indeed, the latter evolves, it fluctuates a little, but never changes its form. It stays similar to the isolated case. This means that the stars keep following the plummer distribution. Of course, the dark matter velocity dispersion tells a different story. It does change, but mainly for the outer part of the halo, the one that gets deformed. The core part evolves but remains similar as the simulations evolve in time. Of course, one should define what is the core part. The mass profiles made it clear that the order of magnitude is $1$ \unit{\kpc}. Farther away the dark matter gets elongated and further deformed. The velocity dispersion profile confirms the order of magnitude. Table \ref{tab:dg_core_estimation}, shows estimations of the core part of the dwarf galaxy that survives. The estimation is made by eyesight to give a rough estimate. A rigorous approach should be used to precisely determine the size of the core. 
	
	\begin{table}[]
		\centering
		\begin{tabular}{@{}lc@{}}
			\toprule
			Simulation & Core size $\unite{\kpc}$ \\ \midrule
			$z=30$ \unit{\kpc} & $0.5$ \\
			$z=70$ \unit{\kpc} & $2$ \\
			$r=70$ \unit{\kpc}, $\theta=\pi/4$ & $2$ \\
			$r=70$ \unit{\kpc}, $\theta=\pi$ & $2$ \\
			Bootes $1$& $1$  \\ \bottomrule
		\end{tabular}
		\caption{Estimation of the core size of the dwarf galaxies for the different simulations. The isolated case is not given since it does not evolve in the external potential. }
		\label{tab:dg_core_estimation}
	\end{table}
	
	
	Finally, even Bootes $1$ presents only a small increase of its half-light radius. But the case of Bootes $1$ is different since this dwarf galaxy follows an elliptical orbit and thus undergo a gradient of the tidal stripping force. This gradient can be seen on figures \ref{fig:radius_of_half_mass_DM} and \ref{fig:mass_within_0_5_kpc_DM} for the dark matter : at around $3$ and $5.5 \cdot$ \unit{\unitTime} the half-light radius reaches a local maximum, while at $1$, $3$ and $5.5 \cdot$ \unit{\unitTime} the mass enclosed within $\tilde{r}$ starts decreasing faster than at other times. The analysis of the circular orbits simulations remains valid for Bootes $1$. 
	
	Those results points that the NFW profile for dark matter halo is highly resistant to tidal stripping. The outer parts of the halo gets destroyed but there remains a core that acts as a protection to the stars, which lie in the latter, preventing them to change much in shape. Of course, the stars evolve and tend to slowly expand outwards, not necessarily spherically. But this effect is small over the timescale of a few giga years. Therefore, one should not expect observing dwarf galaxies with big tidal stripping effects. The presence of a remnant core for NFW halo has also been observed by \cite{errani2023}, but they had a isothermal Milky Way potential. 
	Nevertheless, this conclusion is based on one type of initial conditions for the dwarf galaxies, given in section \ref{subsec:IC}. More initial conditions should be tested to confirm or not this conclusion and to provide types of initial conditions that may provide more visible tidal stripping effect. 
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/mass_profile_stars}
		\caption{Mass profiles of the \emph{stars} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:mass_profile_stars}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/mass_profile_DM}
		\caption{Mass profiles of the \emph{dark matter halo} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:mass_profile_DM}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/radius_of_half_mass_stars}
		\caption{$2$D radius of the half mass for the \emph{stars} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:radius_of_half_mass_stars}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/radius_of_half_mass_DM}
		\caption{$2$D radius of the half mass for the \emph{dark matter} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:radius_of_half_mass_DM}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/mass_within_0_5_kpc_stars}
		\caption{Mass enclosed within a radius $\tilde{r} = 0.5$ \unit{\kpc} for the \emph{stars} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:mass_within_0_5_kpc_stars}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/mass_within_0_5_kpc_DM}
		\caption{Mass enclosed within a radius $\tilde{r} = 0.5$ \unit{\kpc} for the \emph{dark matter} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:mass_within_0_5_kpc_DM}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/velocity_dispersion_stars}
		\caption{Velocity dispersion for the \emph{stars} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:velocity_dispersion_stars}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/velocity_dispersion_DM}
		\caption{Velocity dispersion for the \emph{halo} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:velocity_dispersion_DM}
	\end{figure*}
	
	\begin{figure*}
		%		\centering
		\hspace{-0.7cm}
		\includegraphics[scale=0.7]{figures/wolf_relation}
		\caption{Evolution of Wolf's equation \eqref{eq:wolf_equation} for a) Isolated case, b) Simulation with $z=30$, c) Simulation with $z=70$, d) Simulation with $r=70$, $\theta = \pi/4$, e) Simulation with $r=70$, $\theta = \pi$, f) Bootes $1$.}
		\label{fig:wolf_relation}
	\end{figure*}
	
	\begin{figure*}
		\centering
		%		\hspace\\{-0.8cm}
		\includegraphics[scale=0.56]{figures/simulation_images}
		\caption{From left to right : Evolution of the simulation at each fourth of the simulations. From top to bottom, the simulations are : Isolated case ; Simulation with $z=30$ ; Simulation with $z=70$ ; Simulation with $r=70$, $\theta = \pi/4$ ; Simulation with $r=70$, $\theta = \pi$ ; Bootes $1$.}
		\label{fig:simulation_images}
	\end{figure*}
	
	\subsection{Hercules and Tucana 2}\label{subsec:hercules_tucana}
	
	The results for Hercules and Tucana $2$ are presented on figures \ref{fig:mass_profile_stars_2} and \ref{fig:mass_profile_DM_2} for the mass profiles, \ref{fig:radius_of_half_mass_stars_2} and \ref{fig:radius_of_half_mass_DM_2} for the half-light radius, \ref{fig:mass_within_0_5_kpc_stars_2} and \ref{fig:mass_within_0_5_kpc_DM_2} for the mass within $\tilde{r} = 0.5$ \unit{kpc}, \ref{fig:velocity_dispersion_stars_2} and \ref{fig:velocity_dispersion_DM_2} for the velocity dispersion and Wolf's relation is given on figure \ref{fig:wolf_relation_2}. The images of the simulations are shown on figure \ref{fig:simulation_images_2}. \\
	Those two simulations are separated from the other because the simulations show that the stars start contracting. This effect is not small and is visible on figure \ref{fig:radius_of_half_mass_stars_2}. This effect was not expected. But this is not all, the dark matter halo does not contract at all, as shown on figure \ref{fig:radius_of_half_mass_DM_2}, which is counter-intuitive. It's as if some mysterious force acts on the stars and not on the dark matter. Indeed, those two simulations may reveal some problems either in the initial conditions of the dwarf galaxies, in which case maybe a different seed for the same initial conditions may not give this contraction. Or there is a problem with \texttt{swift}. \\
	There is not any physical explanation yet. Therefore, we treat those simulations as problematic and they are being investigated to determine the cause. 
	
	%Images pour Hercules et Tucana 2
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/mass_profile_stars_2}
		\caption{Mass profiles of the \emph{stars} for a) Hercules, b) Tucana $2$.}
		\label{fig:mass_profile_stars_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{0.2cm}
		\includegraphics[scale=0.65]{figures/mass_profile_DM_2}
		\caption{Mass profiles of the \emph{dark matter halo} for a) Hercules, b) Tucana $2$.}
		\label{fig:mass_profile_DM_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/radius_of_half_mass_stars_2}
		\caption{$2$D radius of the half mass for the \emph{stars} for a) Hercules, b) Tucana $2$.}
		\label{fig:radius_of_half_mass_stars_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/radius_of_half_mass_DM_2}
		\caption{$2$D radius of the half mass for the \emph{dark matter} for  a) Hercules, b) Tucana $2$.}
		\label{fig:radius_of_half_mass_DM_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/mass_within_0_5_kpc_stars_2}
		\caption{Mass enclosed within a radius $\tilde{r} = 0.5$ \unit{\kpc} for the \emph{stars} for a) Hercules, b) Tucana $2$.}
		\label{fig:mass_within_0_5_kpc_stars_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/mass_within_0_5_kpc_DM_2}
		\caption{Mass enclosed within a radius $\tilde{r} = 0.5$ \unit{\kpc} for the \emph{stars} for a) Hercules, b) Tucana $2$.}
		\label{fig:mass_within_0_5_kpc_DM_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/velocity_dispersion_stars_2}
		\caption{Velocity dispersion for the \emph{stars} for a) Hercules, b) Tucana $2$.}
		\label{fig:velocity_dispersion_stars_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/velocity_dispersion_DM_2}
		\caption{Velocity dispersion for the \emph{halo} for a) Hercules, b) Tucana $2$.}
		\label{fig:velocity_dispersion_DM_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
%		\hspace{-0.7cm}
		\includegraphics[scale=0.65]{figures/wolf_relation_2}
		\caption{Evolution of Wolf's equation \eqref{eq:wolf_equation} for a) Hercules, b) Tucana $2$.}
		\label{fig:wolf_relation_2}
	\end{figure*}
	
	\begin{figure*}
		\centering
		%		\hspace\\{-0.8cm}
		\includegraphics[scale=0.6]{figures/simulation_images_2}
		\caption{From left to right : Evolution of the simulation at each fourth of the simulations. Top : Hercules ; Bottom : Tucana $2$.}
		\label{fig:simulation_images_2}
	\end{figure*}

\section{Conclusion} \label{sec:conclusion}

To conclude, the present work used \texttt{swift} to study the evolution of the tidal stripping of dwarf galaxies modeled by a NFW profile for the dark matter halo and a plummer model for the stars. The dwarfs were evolving in \texttt{MWPotential2014}, a Milky Way like potential from \cite{bovy2015}. Different simulations were performed:
%
	\begin{itemize}
		\item Circular orbits with a dwarf galaxy placed at $(0, \, 0, \, z)$ and $z$ was varied ; 
		\item Circular orbits with the dwarf placed $r=70$ \unit{\kpc} and at two different angles $\theta = \pi/4$ and $\theta = \pi$ ;
		\item Realistic orbits for Bootes $1$, Hercules and Tucana $2$ with their apocenter  found by backward integration from their proper motions.
	\end{itemize}
%	
We found that the NFW profile was highly resistant to tides in such a way that in our simulations, there was always a remnant, the core part of the profile. Also, the stars tend to expand outwards, but this effect is small so that in real observations one should not expect to find a big effect. \\
This work also simulated Hercules and Tucana $2$. Both have a contraction of the stellar part of the dwarf. This effect does not happen to the dark matter halo. Since this is counter-intuitive and physically hard to explain, one concluded that those simulations may have encountered some problems either in the initial conditions or with \texttt{swift}. The problems are under investigation. \\
Finally, the present work was limited to one type of initial conditions and one set of values for the parameters. However, to deepen the understanding of tidal stripping of dwarf galaxies, different initial conditions must be used. On the one hand, one should vary the parameters of the plummer and NFW model to determine their effects on the evolution of the dwarf galaxies. On the other hand, different models can be used, particularly for the dark matter halo. One direction can be to use the same profile as \cite{errani2023}, who used a \textquote{cored} NFW density profile that has a central constant density core. The more complex Milky Way potential of the present work may exhibit different results.





\bibliographystyle{aa}
\bibliography{bibliography}

\end{document}